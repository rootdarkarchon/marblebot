﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Preconditions
{
    public class RequireMentionedUserCardsAttribute : PreconditionAttribute
    {
        private readonly CardType? cardType;
        private readonly int minCards;

        public RequireMentionedUserCardsAttribute(int minCards = 1, CardType cardType = CardType.Undefined)
        {
            this.minCards = minCards;
            this.cardType = cardType;
        }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            bool success = true;
            List<UserCard> ownedCards;
            ulong userId = context.Message.MentionedUserIds.First();

            using (var db = new DatabaseContext())
            {
                if (cardType != CardType.Undefined)
                {
                    ownedCards = db.GetCardsForUser(userId).Where(c => c.BaseCard.Type == cardType.Value).ToList();
                }
                else
                {
                    ownedCards = db.GetCardsForUser(userId).ToList();
                }
            }

            success = ownedCards.Count() >= minCards;

            if (success)
            {
                return Task.FromResult(PreconditionResult.FromSuccess());
            }
            else
            {
                var type = cardType == CardType.Undefined ? "" : " " + cardType.ToString();
                return Task.FromResult(PreconditionResult.FromError($"The user you mentioned requires at least {minCards}{type} cards to run this command."));
            }
        }
    }
}