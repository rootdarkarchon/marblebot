﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Preconditions
{
    public class RequireModeratorAttribute : PreconditionAttribute
    {
        public RequireModeratorAttribute()
        {
        }

        public async override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            User user;
            using (var db = new DatabaseContext())
            {
                user = (db.Users as IQueryable<User>).SingleOrDefault(e => e.DiscordId == context.Message.Author.Id && (e.IsModerator || e.IsOwner));
            }

            bool success = false;
            if (user == null)
            {
                var owner = await context.Guild.GetOwnerAsync();
                success = owner.Id == context.Message.Author.Id;
            }

            if (user != null || success)
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("This command requires moderator rights.");
            }
        }
    }
}