﻿using Discord.Commands;
using System;
using System.Threading.Tasks;

namespace MarbleBot.Preconditions
{
    public class RequireUserInNoTransactionAttribute : PreconditionAttribute
    {
        public RequireUserInNoTransactionAttribute()
        {
        }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            bool success = true;
            var handler = services.GetService(typeof(CommandHandler)) as CommandHandler;
            if (handler.UsersBlockedFromTransactions.Contains(context.Message.Author.Id))
            {
                success = false;
            }

            if (success)
            {
                return Task.FromResult(PreconditionResult.FromSuccess());
            }
            else
            {
                return Task.FromResult(PreconditionResult.FromError($"{context.Message.Author.Mention} please clear up your open transactions (upgrades, trades) and try again."));
            }
        }
    }
}