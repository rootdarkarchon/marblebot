﻿using Discord.Commands;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Preconditions
{
    public class RequireMentionedUserInNoTransactionAttribute : PreconditionAttribute
    {
        public RequireMentionedUserInNoTransactionAttribute()
        {
        }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            var handler = services.GetService(typeof(CommandHandler)) as CommandHandler;
            bool success = true;
            if (handler.UsersBlockedFromTransactions.Contains(context.Message.MentionedUserIds.First()))
            {
                success = false;
            }

            if (success)
            {
                return Task.FromResult(PreconditionResult.FromSuccess());
            }
            else
            {
                return Task.FromResult(PreconditionResult.FromError($"The user you want to trade with is currently locked from transactions. Try again later."));
            }
        }
    }
}