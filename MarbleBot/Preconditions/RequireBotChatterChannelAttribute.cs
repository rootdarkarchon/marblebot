﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Preconditions
{
    public class RequireBotChatterChannelAttribute : PreconditionAttribute
    {
        public RequireBotChatterChannelAttribute()
        {
        }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            bool success = false;
            using (var db = new DatabaseContext())
            {
                success = (db.Channels as IQueryable<Channel>).Where(c => c.ChannelType == Utilities.ChannelType.BotChatter).Any(c => c.ChannelId == context.Channel.Id);
            }

            if (success)
            {
                return Task.FromResult(PreconditionResult.FromSuccess());
            }
            else
            {
                return Task.FromResult(PreconditionResult.FromError("This command needs to run in the bot chat channel."));
            }
        }
    }
}