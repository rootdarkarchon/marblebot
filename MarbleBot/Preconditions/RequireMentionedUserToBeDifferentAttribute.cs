﻿using Discord.Commands;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Preconditions
{
    public class RequireMentionedUserToBeDifferentAttribute : PreconditionAttribute
    {
        public RequireMentionedUserToBeDifferentAttribute()
        {
        }

        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            bool success = context.Message.MentionedUserIds.First() != context.Message.Author.Id;

            if (success)
            {
                return Task.FromResult(PreconditionResult.FromSuccess());
            }
            else
            {
                return Task.FromResult(PreconditionResult.FromError($"You cannot run this command with yourself."));
            }
        }
    }
}