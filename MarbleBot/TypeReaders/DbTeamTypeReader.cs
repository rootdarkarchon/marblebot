﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.TypeReaders
{
    public class DbTeamTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            Team output = null;

            using (var db = new DatabaseContext())
            {
                if (int.TryParse(input, out var teamID))
                {
                    output = (db.Teams as IQueryable<Team>).SingleOrDefault(c => c.Id == teamID);
                }
                else
                {
                    output = (db.Teams as IQueryable<Team>).SingleOrDefault(c => c.Name.ToLower() == input.ToLower());
                }
            }

            if (output != null)
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(output));
            }

            return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Input not a valid Team"));
        }
    }
}