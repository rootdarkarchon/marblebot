﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.TypeReaders
{
    public class DbUserTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            ulong mentionedId = 0;

            User output = null;

            if (context.Message.MentionedUserIds.Count > 0)
            {
                mentionedId = context.Message.MentionedUserIds.First();
            }

            using (var db = new DatabaseContext())
            {
                if (mentionedId == 0)
                {
                    if (ulong.TryParse(input, out var userId))
                    {
                        output = db.Users.Include(u => u.OwnedCards).SingleOrDefault(c => c.DiscordId == userId);
                    }
                }
                else
                {
                    output = db.Users.Include(u => u.OwnedCards).SingleOrDefault(c => c.DiscordId == mentionedId);
                }
            }

            if (output != null)
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(output));
            }

            return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "UserId not found in database"));
        }
    }
}