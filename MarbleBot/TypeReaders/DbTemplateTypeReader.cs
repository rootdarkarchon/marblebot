﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.TypeReaders
{
    public class DbTemplateTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            Template output = null;

            using (var db = new DatabaseContext())
            {
                if (int.TryParse(input, out var templateId))
                {
                    output = (db.Templates as IQueryable<Template>).Include(t => t.TemplateFields).SingleOrDefault(c => c.Id == templateId);
                }
                else
                {
                    output = (db.Templates as IQueryable<Template>).Include(t => t.TemplateFields).SingleOrDefault(c => c.Name.ToLower() == input.ToLower());
                }
            }

            if (output != null)
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(output));
            }

            return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Input not a valid Template"));
        }
    }
}