﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.TypeReaders
{
    public class DbSettingTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            Setting output = null;

            using (var db = new DatabaseContext())
            {
                output = db.Settings.SingleOrDefault(c => c.Name.ToLower() == input.ToLower());
            }

            if (output != null)
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(output));
            }

            return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Input not a valid Setting"));
        }
    }
}