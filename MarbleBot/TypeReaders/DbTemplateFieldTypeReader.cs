﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.TypeReaders
{
    public class DbTemplateFieldTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            TemplateField output = null;

            using (var db = new DatabaseContext())
            {
                if (int.TryParse(input, out int templateId))
                {
                    output = (db.TemplateFields as IQueryable<TemplateField>).Include(t => t.AssociatedTemplate).SingleOrDefault(c => c.Id == templateId);
                }
            }

            if (output != null)
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(output));
            }

            return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Input not a valid TemplateField"));
        }
    }
}