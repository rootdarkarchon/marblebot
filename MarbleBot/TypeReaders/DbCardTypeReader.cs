﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.TypeReaders
{
    public class DbCardTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            Card output = null;

            using (var db = new DatabaseContext())
            {
                if (int.TryParse(input, out int cardID))
                {
                    output = (db.Cards as IQueryable<Card>).Include(c => c.Template).Include(c => c.Template.TemplateFields).Include(c => c.Team).SingleOrDefault(c => c.Id == cardID);
                }
                else
                {
                    output = (db.Cards as IQueryable<Card>).Include(c => c.Template).Include(c => c.Template.TemplateFields).Include(c => c.Team).SingleOrDefault(c => c.Name.ToLower() == input.ToLower());
                }
            }

            if (output != null)
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(output));
            }

            return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Input not a valid Card"));
        }
    }

    public class UpgradePropertiesArrayTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            List<UpgradeProperty> properties = new List<UpgradeProperty>();

            var unparsedProperties = input.Split(new char[] { ' ' });

            bool success = true;

            foreach (var prop in unparsedProperties)
            {
                if (Enum.TryParse(prop, out UpgradeProperty value))
                {
                    properties.Add(value);
                }
                else
                {
                    success = false;
                    break;
                }
            }

            if (success)
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(properties.ToArray()));
            }
            else
            {
                return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Input not valid UpgradeProperties"));
            }
        }
    }
}