﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.TypeReaders
{
    public class DbUserCardTypeReader : TypeReader
    {
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            UserCard output = null;

            using (var db = new DatabaseContext())
            {
                if (int.TryParse(input, out var userId))
                {
                    output = db.UserCards.Include(u => u.BaseCard).Include(u => u.BaseCard.Template).Include(u => u.BaseCard.Template.TemplateFields).Include(u => u.BaseCard.Team).Include(u => u.Owner).SingleOrDefault(c => c.Id == userId);
                }
            }

            if (output != null)
            {
                return Task.FromResult(TypeReaderResult.FromSuccess(output));
            }

            return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "UserCardId not found in database"));
        }
    }
}