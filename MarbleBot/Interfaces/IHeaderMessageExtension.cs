﻿namespace MarbleBot.Interfaces
{
    public interface IHeaderMessageExtension : IReactiveDeckAddon
    {
        string HeaderMessage { get; }
    }
}