﻿using Discord;
using MarbleBot.Utilities.Event;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MarbleBot.Interfaces
{
    public interface IFooterReactionAddon<T> : IFooterMessageDeckAddon, IWaitableExtension
    {
        IEnumerable<IEmote> FooterReactionEmotes { get; }

        T Result { get; }

        Task AddReactionToFooter(object sender, ReactionAddedEventArgs messageEvent);

        void ClearResult();

        Task HandleFooterReactionAdd(object sender, ReactionEventArgs reaction);

        Task HandleFooterReactionRemove(object sender, ReactionEventArgs reaction);
    }
}