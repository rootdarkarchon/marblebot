﻿using Discord;
using Discord.WebSocket;
using MarbleBot.Utilities.Event;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MarbleBot.Interfaces
{
    public interface ICardsReactionDeckAddon : IReactiveDeckAddon
    {
        IEnumerable<IEmote> DeckReactionEmotes { get; }
        SocketUser Self { get; }

        Task HandleDeckReactionAdd(object sender, ReactionEventArgs reaction);

        Task ManageReactionsToDeck(object sender, AddToDeckEventArgs e);
    }
}