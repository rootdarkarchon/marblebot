﻿using Discord;
using Discord.WebSocket;
using MarbleBot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Interfaces
{
    public abstract class ReactableBase : IReactable
    {
        protected List<SelfReaction> SelfReactions;

        private readonly List<SocketReaction> ReactionsDuringInitialize;

        private bool handlingReactions;

        private bool initialized;

        protected ReactableBase(IUser initiator, UserTransaction transaction, CommandHandler handler)
        {
            Initiator = initiator;
            Transaction = transaction;
            Handler = handler;
            ReactableMessages = new List<IUserMessage>();
            SelfReactions = new List<SelfReaction>();
            ReactionsDuringInitialize = new List<SocketReaction>();
        }

        public IMessageChannel Channel => Transaction.Channel;
        public CommandHandler Handler { get; private set; }
        public IUser Initiator { get; private set; }

        public List<IUserMessage> ReactableMessages { get; private set; }
        public IUser Self => Transaction.Self;

        public UserTransaction Transaction { get; private set; }
        public List<IUser> TransactionUsers => Transaction.Participants;
        protected abstract bool AdditionalCompletion { get; }
        protected abstract bool ConfirmResult { get; set; }

        protected bool MessageReacted { get; set; }

        public async Task AddReactions(IUserMessage message, params IEmote[] emotes)
        {
            foreach (var emote in emotes)
            {
                SelfReactions.Add(new SelfReaction(message.Id, Self.Id, emote));
                await message.AddReactionAsync(emote);
            }
        }

        public IUserMessage GetMessageById(ulong id)
        {
            if (id != 0)
                return ReactableMessages.Single(u => u.Id == id);
            else
                return null;
        }

        public async Task ReactAdd(SocketReaction reaction)
        {
            UpdateReactableMessages(reaction.Message.Value);
            SelfReaction selfReaction = SelfReactions.FirstOrDefault(r => r.MessageId == reaction.MessageId && r.Emote.Name == reaction.Emote.Name && r.UserId == reaction.UserId);
            if (selfReaction != null)
            {
                SelfReactions.Remove(selfReaction);
                return;
            }
            if (!initialized)
            {
                ReactionsDuringInitialize.Add(reaction);
                return;
            }
            await WaitUntilUnblocked(reaction);
            using (Channel.EnterTypingState())
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                Transaction.LastInteraction = DateTime.Now;
                await ReactAddImplementation(reaction);
            }
            handlingReactions = false;
        }

        public async Task ReactRemove(SocketReaction reaction)
        {
            UpdateReactableMessages(reaction.Message.Value);
            SelfReaction selfReaction = SelfReactions.FirstOrDefault(r => r.MessageId == reaction.MessageId && r.Emote.Name == reaction.Emote.Name && r.UserId == reaction.UserId);
            if (selfReaction != null)
            {
                SelfReactions.Remove(selfReaction);
                return;
            }
            if (!initialized)
            {
                await Channel.SendMessageAsync("> **Wait until the bot has finished constructing your message.**");
                return;
            }
            await WaitUntilUnblocked(reaction);
            using (Channel.EnterTypingState())
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                Transaction.LastInteraction = DateTime.Now;
                await ReactRemoveImplementation(reaction);
            }
            handlingReactions = false;
        }

        public async Task RemoveReactions(IUserMessage message, IUser user, params IEmote[] emotes)
        {
            foreach (var emote in emotes)
            {
                SelfReactions.Add(new SelfReaction(message.Id, user.Id, emote));
                await message.RemoveReactionsAsync(user, emotes);
            }
        }

        public async Task<bool> WaitUntilCompletion()
        {
            while (!AdditionalCompletion && !MessageReacted && !Transaction.Complete)
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5));
            }

            await Delete();

            if (!ConfirmResult)
            {
                Transaction.Cancel();
            }

            return ConfirmResult;
        }

        public bool WasReactedTo(ulong userid, ulong messageid)
        {
            return (userid == Initiator.Id || userid == Self.Id)
                && (ReactableMessages.Any(r => r.Id == messageid));
        }

        protected async Task Execute(Func<List<IUserMessage>, Task> Action)
        {
            if (!Handler.Reactables.Contains(this))
            {
                Handler.Reactables.Add(this);
            }

            ReactableMessages = new List<IUserMessage>();
            Transaction.LastInteraction = DateTime.Now;
            initialized = false;
            await Task.Run(async () =>
            {
                try
                {
                    using (Channel.EnterTypingState())
                    {
                        await Task.Delay(TimeSpan.FromSeconds(1));
                        await Action(ReactableMessages);
                        await FinishInitialization();
                    }
                }
                catch (Exception ex)
                {
                    await CommandHandler.Log(new LogMessage(LogSeverity.Critical, this.ToString(), "Error during execute", ex));
                    if (Transaction.ParentTransaction != null)
                    {
                        await Transaction.ParentTransaction.Channel.SendMessageAsync("Critical Error occurred during processing: " + ex.ToString());
                    }
                    else
                    {
                        await Channel.SendMessageAsync("Critical Error occurred during processing: " + ex.ToString());
                    }
                }
            });
        }

        protected abstract Task ReactAddImplementation(SocketReaction reaction);

        protected abstract Task ReactRemoveImplementation(SocketReaction reaction);

        private async Task Delete()
        {
            Handler.Reactables.Remove(this);

            List<IUserMessage> messagesToDelete = new List<IUserMessage>(ReactableMessages);
            messagesToDelete.Reverse();
            foreach (var msg in messagesToDelete)
            {
                try
                {
                    await msg.DeleteAsync();
                }
                catch (Exception ex)
                {
                    await CommandHandler.Log(new LogMessage(LogSeverity.Error, this.ToString(), "Could not delete message " + msg.Id, ex));
                }
            }
        }

        private async Task FinishInitialization()
        {
            if (ReactionsDuringInitialize.Any())
            {
                await Channel.SendMessageAsync("> **Wait until the message has finished constructing. Your reactions have been removed. You will have to re-add your reactions.**");
            }
            while (ReactionsDuringInitialize.Any())
            {
                var react = ReactionsDuringInitialize.First();
                await react.Message.Value.RemoveReactionAsync(react.Emote, react.User.Value);
                ReactionsDuringInitialize.Remove(react);
            }

            Transaction.LastInteraction = DateTime.Now;
            initialized = true;
        }

        private void UpdateReactableMessages(IUserMessage message)
        {
            ReactableMessages.Remove(ReactableMessages.FirstOrDefault(f => f.Id == message.Id));
            ReactableMessages.Add(message);
        }

        private async Task WaitUntilUnblocked(SocketReaction reaction)
        {
            if (handlingReactions)
            {
                await Channel.SendMessageAsync("> **The bot is currently processing your reaction. Don't react so quickly.**");
                await CommandHandler.Log(new LogMessage(LogSeverity.Info, GetType().Name.ToString(), $"User {reaction.User.Value.Username} reacted too quickly."));
                while (handlingReactions)
                {
                    await Task.Delay(TimeSpan.FromSeconds(0.5));
                }
                handlingReactions = true;
            }
            else
            {
                handlingReactions = true;
            }
        }
    }
}