﻿using MarbleBot.Reactables;

namespace MarbleBot.Interfaces
{
    public interface IReactiveDeckAddon
    {
        DeckReactable Deck { get; set; }
    }
}