﻿namespace MarbleBot.Interfaces
{
    public interface IWaitableExtension : IReactiveDeckAddon
    {
        bool ConfirmResult { get; }
        bool WaitForExtension { get; }
    }
}