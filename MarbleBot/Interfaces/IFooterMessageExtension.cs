﻿namespace MarbleBot.Interfaces
{
    public interface IFooterMessageDeckAddon : IReactiveDeckAddon
    {
        string FooterMessage { get; }
    }
}