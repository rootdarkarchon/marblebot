﻿using Discord;
using Discord.WebSocket;
using MarbleBot.Utilities;
using System.Threading.Tasks;

namespace MarbleBot.Interfaces
{
    public interface IReactable
    {
        IMessageChannel Channel { get; }
        CommandHandler Handler { get; }
        IUser Initiator { get; }

        IUser Self { get; }

        UserTransaction Transaction { get; }

        Task ReactAdd(SocketReaction reaction);

        Task ReactRemove(SocketReaction reaction);

        Task<bool> WaitUntilCompletion();

        bool WasReactedTo(ulong userid, ulong messageid);
    }
}