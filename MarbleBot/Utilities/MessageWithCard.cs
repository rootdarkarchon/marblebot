﻿using Discord;
using MarbleBot.Models;
using System;

namespace MarbleBot.Utilities
{
    public class MessageWithCard
    {
        public MessageWithCard(Func<IUserMessage> message, UserCard card)
        {
            GetMsgFunc = message;
            Card = card;
        }

        public UserCard Card { get; set; }
        public IUserMessage Message => GetMsgFunc.Invoke();
        private Func<IUserMessage> GetMsgFunc { get; set; }
    }
}