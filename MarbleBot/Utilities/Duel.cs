﻿using DataStructures.RandomSelector;
using Discord;
using MarbleBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class Duel
    {
        public UserCard Winner;
#pragma warning disable IDE0052 // Remove unread private members
#pragma warning disable IDE0044 // Add readonly modifier
#pragma warning disable CA1823
        private static List<string> LastUsed_Text_HighEffortRoll_Failed = new List<string>();
        private static List<string> LastUsed_Text_HighEffortRoll_FailedInitialWonEffort = new List<string>();
        private static List<string> LastUsed_Text_HighEffortRoll_Won = new List<string>();
        private static List<string> LastUsed_Text_InitialRoll_BaseStat0 = new List<string>();
        private static List<string> LastUsed_Text_InitialRoll_ResultInRange = new List<string>();
        private static List<string> LastUsed_Text_InitialRoll_ResultInRangeBelowHalf = new List<string>();
        private static List<string> LastUsed_Text_InitialRoll_ResultOverBaseStat = new List<string>();
        private static List<string> LastUsed_Text_InitialRoll_RollIsBaseStat = new List<string>();
        private static List<string> LastUsed_Text_LowEffort_BaseStat0 = new List<string>();
        private static List<string> LastUsed_Text_LowEffort_Other = new List<string>();
        private static List<string> LastUsed_Text_MediumEffortRoll_Failed = new List<string>();
        private static List<string> LastUsed_Text_MediumEffortRoll_FailedInitialWonEffort = new List<string>();
        private static List<string> LastUsed_Text_MediumEffortRoll_Won = new List<string>();
        private static List<string> LastUsed_Text_Won_ByBaseStat = new List<string>();
        private static List<string> LastUsed_Text_Won_ByEffort = new List<string>();
        private static List<string> LastUsed_Text_Won_ByLuck = new List<string>();
        private static List<string> LastUsed_Text_Won_ByResult = new List<string>();
#pragma warning restore IDE0052 // Remove unread private members
#pragma warning restore IDE0044 // Add readonly modifier
#pragma warning restore CA1823
        private readonly UserCard card1;
        private readonly Effort card1Effort;
        private readonly UserCard card2;
        private readonly Effort card2Effort;
        private readonly CardProperty challenge;
        private readonly ITextChannel channel;
        // Initial Roll

        // Failed effort roll → coach demanded too much
        private readonly List<string> Text_HighEffortRoll_Failed = new List<string>
{
"The coach told **{0}** to do their best and **{0}** tried! But it was not enough, the marble hurt itself by pushing too far.",
    "And now **{0}** is caught! They’re stuck and it looks like they won’t be able to win the *{1}* challenge unless a miracle happens.",
    "Oh no! **{0}** jumped off the course and didn’t finish the *{1}* challenge.",
    "And it all comes undone for **{0}** as they stumble through the rest of the *{1}* challenge due to too high demands of the coach!",
    "You’re in too deep, and the concept of you winning in *{1}* is all but naught. Better luck next time, **{0}**!",
    "Looks like someone could not handle all the pressure! There's always a next time, **{0}**!",
    "And **{0}** slipped! Oh boy, that will be a spicy post-race interview!",
    "Looks like we’re going to have to call in the manager for **{0}**’s team...their coach is not preparing them well enough in *{1}* and might be fired!",
    "The head coach of **{0}** must be in shame right now. They’re going to need to re-evaluate their strategy for future challenges in *{1}*.",
};

        // High Effort Roll → all coaching related
        // Failed initial roll & won effort roll → they did not roll success in initial but do a small comeback thanks to the encouragement, does not mean winning
        private readonly List<string> Text_HighEffortRoll_FailedInitialWonEffort = new List<string>
{
"Even when failing the *{1}* challenge **{0}** gave their best. Seems like the coach's pep talk pushed it beyond its limits! But will it be enough?",
    "The JMRC is reviewing the timing and scoring down to thousandths in the *{1}* challenge, and this finish is just good enough for **{0}**! That was a close one!",
    "**{0}** sped up at the very end of the *{1}* challenge to clock a decent time after all.",
    "**{0}** did manage to make up some momentum after all! We will see how that turns out.",
    "Would you look at that cheeky comeback by **{0}**? Not bad at all!",
    "**{0}** must be in really high spirits, because nothing is stopping them from pushing forward in this challenge!",
};

        // Won initial roll & won effort roll → they did roll a decent to good number in initial and passed the effort roll, does not mean winning
        private readonly List<string> Text_HighEffortRoll_Won = new List<string>
{
"**{0}** showed up today in top form! Seems like the coach's pep talk did wonders! They put in their highest effort. Will it be rewarded?",
    "And there is no question that **{0}** has dominated in the *{1}* challenge. Well done!",
    "There's no doubt why the coach put their faith in **{0}** as they gave their best in the *{1}* challenge.",
    "**{0}**'s time in the *{1}* challenge is faster than even their personal best!",
    "Right out of the gate **{0}** is moving quicker than ever before! Could they set a record-breaking time?",
    "Is it history in the making that we are witnessing here? Absolutely splendid run for **{0}** as they do their absolute best, with even luck being on their side today!",
    "What a performance by **{0}** in the *{1}* challenge! Could they set a new Marble League record??",
    "An effort by **{0}** worthy of a gold medal! We’ll have to wait for official confirmation from the JMRC…",
    "**{0}** seems to be well-trained for this event! Maybe Tide should take a page from this coach’s handbook.",
    "The crowd is going wild, the coach is beaming, and **{0}** is rolling towards what looks to be a banner finish in this event!",
};

        // Base Stat of this card is 0 → automatic failure of roll, does not mean failure in challenge
        private readonly List<string> Text_InitialRoll_BaseStat0 = new List<string>
{
    "**{0}** had a really tough start in the *{1}* category, having no skill in it whatsoever.",
    "Without any prior experience **{0}** rolls into the challenge. Were they the right choice?",
    "Interesting! The coach decided to let **{0}** compete for *{1}*. Will they roll it home, despite having no experience? We shall see.",
    "**{0}** shambles across the course like a sozzled ball bearing! Not a good score to put up in the standings.",
    "What is **{0}** doing? That's not how you are supposed to roll in this challenge!",
    "Oh no! **{0}** is rolling in the wrong direction! How is that even possible?",
    "The starting gate has lifted, but **{0}** isn’t moving! This isn’t a good start for them in the *{1}* challenge.",
};

        // Rolled result was in range and above half of the base stat → success, does not mean winning, "good result"
        private readonly List<string> Text_InitialRoll_ResultInRange = new List<string>
{
"A good result, **{0}** has successfully completed the *{1}* challenge!",
    "**{0}** delivered a solid, albeit not peak performance in this trial. Depending on how the other marble fares, it could be a solid head start.",
    "**{0}** is just gonna get there in this challenge. Good for them!",
    "Solid showing from **{0}**! Will it be enough to win the *{1}* challenge?",
"Hey, that's not too shabby from **{0}**!",
"It looks like that may be enough...a good showing of *{1}* by **{0}** in this challenge, but we’ll have to wait for official confirmation in the standings.",
};

        // Rolled result was in range and below half of the base stat → success, does not mean winning, "underperforming result"
        private readonly List<string> Text_InitialRoll_ResultInRangeBelowHalf = new List<string>
{
    "While **{0}** has successfully completed the *{1}* challenge, their time could have been better.",
    "**{0}** delivered a subpar performance in this trial. It could be enough if the other marble slips up.",
    "Today is not **{0}**’s day; they finished, but below even their own expectations.",
    "A below average showing from **{0}**! It might not enough to win the *{1}* challenge.",
    "A disappointing performance **{0}** compared to their rolls in the past!",
};

        // Rolled result was over base stat → failure of roll, does not mean failure in challenge
        private readonly List<string> Text_InitialRoll_ResultOverBaseStat = new List<string>
{
    "Overexertion takes its toll and **{0}** has big trouble in the *{1}* challenge. Did they push too far?",
    "**{0}** pushed a bit too far and got off the track during the challenge. The fans can't believe their eyes!",
    "**{0}** didn’t quite get there in the *{1}* challenge today. Maybe this just wasn’t their moment.",
    "All that training in *{1}* just fell apart for **{0}**. Looks like they’ll need a different approach to training for next time.",
    "Life is all about balance. Looks like **{0}** really needs to practice more of that in the future.",
    "Trouble for **{0}** this time as their performance was decisively below average.",
"**{0}**, welcome to wisdom 101 - today's lesson: The paths of greed and vanity will always lead to downfall…",
"Looks like **{0}**’s ego got in the way of their performance. We don’t want to see that…",
};

        // Rolled result was the base stat → great success, does not mean winning
        private readonly List<string> Text_InitialRoll_RollIsBaseStat = new List<string>
{
"**{0}** ran the *{1}* challenge to the best of their ability!",
    "**{0}** came through the *{1}* trial with flying colors. They gave their best!",
    "Rarely you see performance as great as by **{0}** as they pass the *{1}* challenge!",
    "From start to finish, a great success for **{0}** in this challenge! The fans are certainly satisfied.",
    "What a phenomenal performance by **{0}**! They did their absolute best in the *{1}* challenge!",
    "**{0}** was really flirting with the edges of this challenge, but they pulled off a really good result. Let’s see how that shakes out…",
};

        // Medium Effort Roll → all coaching related

        // Base stat of this card is 0 → potentially bad choice to do low effort when base stat is 0
        private readonly List<string> Text_LowEffort_BaseStat0 = new List<string>
{
"The coach told **{0}** to not overexert themselves in the *{1}* challenge and play it safe. Was this a really good decision?",
    "Did a poor choice by the coach ruin **{0}**s chances to win in this trial?",
    "A real head-scratcher from the coaching end here as **{0}** keeps it steady and unimpressive.",
    "Nothing to lose, and nothing to gain. What was **{0}**'s coach even thinking?",
    "So apparently **{0}**'s coach decided to play it safe, but was that really the best thing to do here?",
    "The coach is playing the long game for **{0}**, but that might not work out well in this challenge…",
};

        // Low Effort Roll
        // Otherwise → potentially good choice
        private readonly List<string> Text_LowEffort_Other = new List<string>
{
"The coach told **{0}** to not overwork themselves in the *{1}* challenge and play it safe. Maybe this was the right choice.",
    "A good call by the coach to not incentivize overexertion in this category. It's very well possible that the performance of **{0}** was already good enough.",
    "**{0}** keeps a steady spin and maybe that's enough. The coach knows what they’re doing.",
    "Did the coach tell **{0}** to keep it safe in the *{1}* challenge? Sounds like what anyone would say under these circumstances.",
    "Silence on the coaching end before **{0}** took part in the *{1}* challenge. Looks like they know what **{0}** can do.",
    "**{0}**'s coach knows darn well what they're doing, and it shows!",
    "Sometimes less is more, and in this event, it’s certainly working. Good execution by **{0}** and good advising by their coach!",
    "Slow and steady wins the race, and there’s a chance that it might also win this challenge for **{0}**.",
};

        // Failed effort roll → coach demanded too much
        private readonly List<string> Text_MediumEffortRoll_Failed = new List<string>
{
"The coach encouraged **{0}** to perform really well. Sadly, **{0}** could not handle the pressure and was not at their best in the last seconds of the trial.",
    "Neck and neck: and **{0}** isn’t going to hold on. No! It looks like they’re out of the *{1}* challenge by several lengths.",
    "**{0}** lost too much ground and they could not finish the *{1}* challenge to the best of their ability. The fans don’t look too happy.",
"All that jostling did not pan out well for **{0}** in this challenge. Maybe focus on rolling to the finish next time?",
"**{0}**'s coach does not look happy at all. Did they demand too much in this challenge?",
};

        // Failed initial & won effort roll → they did not roll success in initial but do a small comeback thanks to the encouragement, does not mean winning
        private readonly List<string> Text_MediumEffortRoll_FailedInitialWonEffort = new List<string>
{
"Despite *{1}* challenge being really hard on **{0}**, they gave all they could hot off the coach’s speech. Will it be enough for them?",
    "The coach didn't look too confident at the start, but they can breathe a sigh of relief as **{0}** at least crosses the finish.",
    "And we have a comeback! Not even **{0}** themselves expected that!",
    "The fans really helped **{0}** to not give up the *{1}* challenge just yet!",
    "And it looks like **{0}** managed to cross the finish line after a really poor start! All eyes on the Jumbotron.",
};

        // Won initial & won effort roll →  they did roll a decent to good number in initial and passed the effort roll, does not mean winning
        private readonly List<string> Text_MediumEffortRoll_Won = new List<string>
{
"In a surprise to everyone, **{0}** performed above their expectations! The coach did well to encourage them here. But is this enough to beat the other contestant?",
    "The coach's last words for **{0}** clearly boosted them. The time they set in the *{1}* challenge was really good after all.",
    "The coach will be pleasantly surprised that **{0}** scored a very good time in the *{1}* challenge thanks to their encouragement.",
    "**{0}** is starting to separate from the opponent in the *{1}* challenge! Looks like this marble has the confidence to take this round.",
    "That's a happy smile on the coach’s face, as **{0}** finishes the *{1}* challenge with a great showing!",
    "It looks like **{0}** is going to hold their *{1}* together in this challenge. Will this be enough against the other marble?",
    "The fans seem satisfied enough with **{0}** in the *{1}* challenge. There’s even a little crowd chant stirring for them!",
};

        // Winning Texts

        // Card1 base stat is better than card2 base stat → roll result is equal, effort is equal, base stats differ
        private readonly List<string> Text_Won_ByBaseStat = new List<string>
{
    "**{0}**s training in this category gave them a slight edge over **{2}**!",
    "And once again it becomes apparent how important the off-season training is, as **{0}** beats **{2}** in this category!",
    "It looks like training could have helped **{2}** after all. **{0}** takes the lead and wins!",
    "A well-formed marble is as important as well-formed stats and look at **{0}** go! Their training really helped them keep the lead in this match.",
    "Just as we get close to the finish of this trial **{2}** really tries to give it all, but it looks like **{0}** comes out of nowhere and with superior strategy wins the match!",
};

        // Card1 effort is better than card2 effort → roll result is equal, card1 used higher effort
        private readonly List<string> Text_Won_ByEffort = new List<string>
{
"The effort **{0}** put into this challenge was barely enough to beat **{2}**!",
    "The coach's pep talk really helped **{0}** to get in the lead in this challenge!",
    "**{0}** fends off a strong fight from **{2}** to win the *{1}* challenge!",
    "**{2}** may have seemed stronger earlier, but that last push by **{0}** scored them the win!",
    "**{0}** takes the win in the *{1}* challenge! Some real guts shown by the winner there near the end.",
};

        // Random result → roll result is equal, effort is equal, base stat is equal, random choice
        private readonly List<string> Text_Won_ByLuck = new List<string>
{
    "By sheer luck **{0}** snags the win for this category!",
"LOOK AT **{0}**!! Look at **{0}**, they have come out of nowhere and they have won the battle for this category!",
"This neck-and-neck race ends with **{0}** edging over **{2}** to take the *{1}* challenge!",
"What's that? **{0}** breaks apart from **{2}** near the end to win it all!",
"What a finish! Just a few bad rolls from **{2}** was enough for **{0}** to come first in the *{1}* challenge.",
};

        // Card1 result is better than card2 result → card 1 wins by roll result
        private readonly List<string> Text_Won_ByResult = new List<string>
{
"It looks like **{0}** had absolutely no problem with this challenge, surpassing **{2}**!",
    "An excellent roll by **{0}** beating **{2}** by a landslide in the *{1}* challenge!",
    "Wow. **{2}** wasn’t in the same league as **{0}** in the *{1}* challenge; they didn’t even come close to **{0}** as they clinched the win!",
    "This could be one of the biggest margins of victory that **{0}** has ever had. Congratulations!",
    "**{2}** lost control in the opening seconds as **{0}** takes the *{1}* challenge without a sweat.",
};

        private double card1Result;
        private double card2Result;

        public Duel(UserCard card1, Effort card1Effort, UserCard card2, Effort card2Effort, CardProperty property, ITextChannel channel)
        {
            this.card1 = card1;
            this.card2 = card2;
            this.card1Effort = card1Effort;
            this.card2Effort = card2Effort;
            this.challenge = property;
            this.channel = channel;
        }

        public async Task Execute(int categoryCount)
        {
            await channel.SendMessageAsync("> The " + GetCategoryCountString(categoryCount) + " category our competitors will partake in is **" + challenge + "**!");

            await Task.Delay(TimeSpan.FromSeconds(5));

            Random rnd = new Random();
            var card1stat = card1.GetTotalProperty(challenge);
            var card2stat = card2.GetTotalProperty(challenge);
            var card1upgrade = card1.GetUpgradeProperty(challenge);
            var card2upgrade = card2.GetUpgradeProperty(challenge);

            var card1Max = card1stat + (card1upgrade >= 3 ? 2 : 3);
            var card2Max = card2stat + (card2upgrade >= 3 ? 2 : 3);

            int card1UpgradeBonus = card1upgrade > 0 ? 1 : 0;
            int card2UpgradeBonus = card2upgrade > 0 ? 1 : 0;

            GenerateWeights(card1stat, card1Max, card1UpgradeBonus, out var numbersCard1, out var weightsCard1);
            GenerateWeights(card2stat, card2Max, card2UpgradeBonus, out var numbersCard2, out var weightsCard2);

            DynamicRandomSelector<int> selector1 = new DynamicRandomSelector<int>(numbersCard1, weightsCard1);
            DynamicRandomSelector<int> selector2 = new DynamicRandomSelector<int>(numbersCard2, weightsCard2);

            var card1roll1 = selector1.SelectRandomItem();
            var card2roll1 = selector2.SelectRandomItem();

            //var card1roll1 = rnd.Next(card1UpgradeBonus, card1Max);
            //var card2roll1 = rnd.Next(card2UpgradeBonus, card2Max);

            var card1Result1 = card1roll1 > card1stat ? 0 : card1roll1;
            var card2Result1 = card2roll1 > card2stat ? 0 : card2roll1;

            var card1roll2 = rnd.Next(0, 100);
            var card2roll2 = rnd.Next(0, 100);

            card1Result = DetermineEffortResult(card1roll2, card1Result1, card1Effort);
            card2Result = DetermineEffortResult(card2roll2, card2Result1, card2Effort);

            await CommandHandler.Log(new LogMessage(LogSeverity.Verbose, "Duel", "C1S " + card1stat + " C1M " + card1Max));
            await CommandHandler.Log(new LogMessage(LogSeverity.Verbose, "Duel", "C2S " + card2stat + " C2M " + card2Max));
            await CommandHandler.Log(new LogMessage(LogSeverity.Verbose, "Duel", "C1Roll1 " + card1roll1 + " C2Roll1 " + card2roll1));
            await CommandHandler.Log(new LogMessage(LogSeverity.Verbose, "Duel", "C1Roll2 " + card1roll2 + " C2Roll2 " + card2roll2));
            await CommandHandler.Log(new LogMessage(LogSeverity.Verbose, "Duel", "C1R1 " + card1Result1 + " C2R1 " + card2Result1));
            await CommandHandler.Log(new LogMessage(LogSeverity.Verbose, "Duel", "C1R2 " + card1Result + " C2R2 " + card2Result));

            await SendInitialRollMessage(card1, card1stat, card1roll1, card1Result1, card2);

            await Task.Delay(TimeSpan.FromSeconds(7.5));

            await SendInitialRollMessage(card2, card2stat, card2roll1, card2Result1, card1);

            await Task.Delay(TimeSpan.FromSeconds(7.5));

            await SendEffortRollMessage(card1, card1Result1, card1Result, card1Effort, card2);

            await Task.Delay(TimeSpan.FromSeconds(7.5));

            await SendEffortRollMessage(card2, card2Result1, card2Result, card2Effort, card1);

            await Task.Delay(TimeSpan.FromSeconds(7.5));

            await DetermineWinner(rnd, card1stat, card2stat, card1Result, card2Result);

            await Task.Delay(TimeSpan.FromSeconds(7.5));
        }

        public Embed GetResultEmbed()
        {
            EmbedBuilder builder = new EmbedBuilder
            {
                Title = "Category " + challenge + " winner: " + Winner.ToString() + "!",

                ImageUrl = Winner.GetEmbedImageUrl().ToString()
            };

            return builder.Build();
        }

        private static double DetermineEffortResult(int roll, double result, Effort effort)
        {
            if (roll > (int)effort && result == 0)
            {
                result = effort switch
                {
                    Effort.HighEffort => 2,
                    Effort.MediumEffort => 1,
                    Effort.LowEffort => 0,
                    _ => 0,
                };
            }
            else if (roll > (int)effort)
            {
                double effortMultiplier = effort switch
                {
                    Effort.HighEffort => 2,
                    Effort.MediumEffort => 1.5,
                    Effort.LowEffort => 1,
                    _ => 1
                };
                result *= effortMultiplier;
            }
            else
            {
                double effortMultiplier = effort switch
                {
                    Effort.HighEffort => 0.25,
                    Effort.MediumEffort => 0.5,
                    Effort.LowEffort => 1,
                    _ => 1,
                };
                result *= effortMultiplier;
            }

            return result;
        }

        private static void GenerateWeights(int card2stat, int card2Max, int card2UpgradeBonus, out int[] numbersCard2, out float[] weightsCard2)
        {
            numbersCard2 = new int[card2Max - card2UpgradeBonus];
            weightsCard2 = new float[card2Max - card2UpgradeBonus];
            for (int i = 0; i < card2Max - card2UpgradeBonus; i++)
            {
                var number = i + card2UpgradeBonus;
                if (number == 0 || number > card2stat)
                {
                    weightsCard2[i] = 5;
                }
                else if (number <= (int)(Math.Ceiling(card2stat / 2.0)))
                {
                    weightsCard2[i] = 10f;
                }
                else
                {
                    weightsCard2[i] = 15;
                }
                numbersCard2[i] = number;
            }
        }

        private static string GetCategoryCountString(int count)
        {
            return count switch
            {
                0 => "first",
                1 => "second",
                2 => "third",
                3 => "fourth",
                4 => "final",
                _ => "unknown"
            };
        }

        private async Task DetermineWinner(Random rnd, int card1stat, int card2stat, double card1Result2, double card2Result2)
        {
            string message;
            if (card1Result2 > card2Result2)
            {
                message = GetText(nameof(Text_Won_ByResult), card1, challenge, card2);
                Winner = card1;
            }
            else if (card2Result2 > card1Result2)
            {
                message = GetText(nameof(Text_Won_ByResult), card2, challenge, card1);
                Winner = card2;
            }
            else
            {
                if (card1stat > card2stat)
                {
                    message = GetText(nameof(Text_Won_ByBaseStat), card1, challenge, card2);
                    Winner = card1;
                }
                else if (card2stat > card1stat)
                {
                    message = GetText(nameof(Text_Won_ByBaseStat), card2, challenge, card1);
                    Winner = card2;
                }
                else
                {
                    if (card1Effort > card2Effort)
                    {
                        message = GetText(nameof(Text_Won_ByEffort), card1, challenge, card2);
                        Winner = card1;
                    }
                    else if (card2Effort > card1Effort)
                    {
                        message = GetText(nameof(Text_Won_ByEffort), card2, challenge, card1);
                        Winner = card2;
                    }
                    else
                    {
                        UserCard loser;
                        if (rnd.Next(100) % 2 == 0)
                        {
                            Winner = card1;
                            loser = card2;
                        }
                        else
                        {
                            Winner = card2;
                            loser = card1;
                        }
                        message = GetText(nameof(Text_Won_ByLuck), Winner, challenge, loser);
                    }
                }
            }

            await channel.SendMessageAsync(message);
        }

        private string GetText(string textName, UserCard card0, CardProperty challenge, UserCard card1)
        {
            var field = typeof(Duel).GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Single(f => f.Name == textName);
            var fieldList = (List<string>)field.GetValue(this);
            var lastUsedField = typeof(Duel).GetFields(BindingFlags.NonPublic | BindingFlags.Static).Single(f => f.Name == "LastUsed_" + textName);

            var lastUsedMessages = (List<string>)lastUsedField.GetValue(null);

            var lastUsedText = fieldList.RemoveAll(c => lastUsedMessages.Contains(c));
            var newText = fieldList.PickRandomAndRemove();

            if (lastUsedMessages.Count == 2)
            {
                lastUsedMessages.Remove(lastUsedMessages.First());
            }

            lastUsedMessages.Add(newText);

            lastUsedField.SetValue(null, lastUsedMessages);
            if (card1 != null)
            {
                return "> " + string.Format(newText, card0.BaseCard.Name, challenge, card1?.BaseCard.Name);
            }
            else
            {
                return "> " + string.Format(newText, card0.BaseCard.Name, challenge);
            }
        }

        private async Task SendEffortRollMessage(UserCard card, double result, double finalresult, Effort effort, UserCard card2)
        {
            switch (effort)
            {
                case Effort.LowEffort: await SendLowEffortRollMessage(card, finalresult, card2); break;
                case Effort.MediumEffort: await SendMediumEffortRollMessage(card, result, finalresult, card2); break;
                case Effort.HighEffort: await SendHighEffortRollMessage(card, result, finalresult, card2); break;
            }
        }

        private async Task SendHighEffortRollMessage(UserCard card, double result, double finalresult, UserCard card2)
        {
            string message;
            if (result > finalresult || finalresult == 0)
            {
                message = GetText(nameof(Text_HighEffortRoll_Failed), card, challenge, card2);
            }
            else if (result == 0 && finalresult > 0)
            {
                message = GetText(nameof(Text_HighEffortRoll_FailedInitialWonEffort), card, challenge, card2);
            }
            else
            {
                message = GetText(nameof(Text_HighEffortRoll_Won), card, challenge, card2);
            }

            await channel.SendMessageAsync(message);
        }

        private async Task SendInitialRollMessage(UserCard card, double stat, double roll, double result, UserCard card2)
        {
            string message = "";
            if (stat == 0)
            {
                message = GetText(nameof(Text_InitialRoll_BaseStat0), card, challenge, card2);
            }
            else if (result == 0)
            {
                message = GetText(nameof(Text_InitialRoll_ResultOverBaseStat), card, challenge, card2);
            }
            else if (Math.Abs(stat - roll) < 0.001)
            {
                message = GetText(nameof(Text_InitialRoll_RollIsBaseStat), card, challenge, card2);
            }
            else if (roll == result && roll < stat / 2)
            {
                message = GetText(nameof(Text_InitialRoll_ResultInRangeBelowHalf), card, challenge, card2);
            }
            else if (roll == result)
            {
                message = GetText(nameof(Text_InitialRoll_ResultInRange), card, challenge, card2);
            }

            await channel.SendMessageAsync(message);
        }

        private async Task SendLowEffortRollMessage(UserCard card, double finalresult, UserCard card2)
        {
            string message;
            if (finalresult == 0)
            {
                message = GetText(nameof(Text_LowEffort_BaseStat0), card, challenge, card2);
            }
            else
            {
                message = GetText(nameof(Text_LowEffort_Other), card, challenge, card2);
            }

            await channel.SendMessageAsync(message);
        }

        private async Task SendMediumEffortRollMessage(UserCard card, double result, double finalresult, UserCard card2)
        {
            string message;
            if (result > finalresult || finalresult == 0)
            {
                message = GetText(nameof(Text_MediumEffortRoll_Failed), card, challenge, card2);
            }
            else if (result == 0 && finalresult > 0)
            {
                message = GetText(nameof(Text_MediumEffortRoll_FailedInitialWonEffort), card, challenge, card2);
            }
            else
            {
                message = GetText(nameof(Text_MediumEffortRoll_Won), card, challenge, card2);
            }

            await channel.SendMessageAsync(message);
        }
    }
}