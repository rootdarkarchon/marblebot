﻿using Discord.WebSocket;
using System;

namespace MarbleBot.Utilities.Event
{
    public class ReactionEventArgs : EventArgs
    {
        public MessageWithCard MessageWithCard;
        public SocketReaction Reaction;
    }
}