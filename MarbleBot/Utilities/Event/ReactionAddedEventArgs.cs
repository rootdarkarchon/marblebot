﻿using Discord;
using MarbleBot.Models;
using System;

namespace MarbleBot.Utilities.Event
{
    public class ReactionAddedEventArgs : EventArgs
    {
        public UserCard Card;
        public IUserMessage Message;
    }
}