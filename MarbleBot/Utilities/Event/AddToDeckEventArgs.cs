﻿using System;
using System.Collections.Generic;

namespace MarbleBot.Utilities.Event
{
    public class AddToDeckEventArgs : EventArgs
    {
        public List<MessageWithCard> Messages;
    }
}