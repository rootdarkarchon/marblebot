﻿using DataStructures.RandomSelector;
using MarbleBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarbleBot.Utilities
{
    public static class EnumerableExtension
    {
        public static double GetDoubleValue(this IEnumerable<Setting> source, string key)
        {
            return source.First(e => e.Name == key).DoubleValue;
        }

        public static int GetIntValue(this IEnumerable<Setting> source, string key)
        {
            return source.First(e => e.Name == key).IntValue;
        }

        public static string GetStringValue(this IEnumerable<Setting> source, string key)
        {
            return source.First(e => e.Name == key).Value;
        }

        public static ulong GetULongValue(this IEnumerable<Setting> source, string key)
        {
            return source.First(e => e.Name == key).ULongValue;
        }

        public static Card PickRandom(this IEnumerable<Card> source)
        {
            var list = source.ToList();
            DynamicRandomSelector<Card> selector = new DynamicRandomSelector<Card>(list, list.Select(u => (float)u.Quantity).ToList());
            return selector.SelectRandomItem();
        }

        public static string PickRandomAndRemove(this List<string> source)
        {
            Random rnd = new Random();
            var text = source[rnd.Next(source.Count)];
            source.Remove(text);
            return text;
        }
    }
}