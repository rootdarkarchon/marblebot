﻿using Discord;
using System;

namespace MarbleBot.Utilities
{
    public class UserEffortMessage
    {
        private readonly Func<IUserMessage> msgFunc;

        public UserEffortMessage(Func<IUserMessage> msg, CardProperty property, IEmote emote)
        {
            msgFunc = msg;
            Property = property;
            Emote = emote;
        }

        public IEmote Emote { get; set; }
        public IUserMessage Message => msgFunc.Invoke();
        public CardProperty Property { get; }
    }
}