﻿namespace MarbleBot.Utilities
{
    public enum CardProperty
    {
        Rarity,
        Name,
        Strength,
        Endurance,
        Power,
        Value,
        Description,
        Balance,
        Teamwork,
        ImagePath,
        Quantity,
        Type
    }

    public enum CardRarity
    {
        Undefined = 999,
        Common = 0,
        Uncommon = 1,
        Rare = 2,
        Epic = 3
    }

    public enum CardType
    {
        Normal = 0,
        Legendary = 1,
        Undefined = 999
    }

    public enum ChannelType
    {
        Admininistrative,
        Earning,
        BotChatter,
        Transaction
    }

    public enum Effort
    {
        LowEffort = 0,
        MediumEffort = 33,
        HighEffort = 66
    }

    public enum TemplateFieldProperty
    {
        Text,
        X,
        Y,
        Width,
        Height,
        FontSize,
        Color,
        FontName,
        Alignment,
        LineAlignment,
        DisplayWhenZero,
        FontStyle,
        ScaleMode
    }

    public enum UpgradeProperty
    {
        Strength,
        Endurance,
        Power,
        Balance,
        Teamwork
    }
}