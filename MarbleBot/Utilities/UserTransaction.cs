﻿using Discord;
using Discord.Rest;
using MarbleBot.Database;
using MarbleBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public sealed class UserTransaction : IDisposable
    {
        public bool AllowEarning = false;
        public ITextChannel Channel;
        private readonly CancellationTokenSource cts = new CancellationTokenSource();

        private readonly CommandHandler handler;
        private readonly ITextChannel originalChannel;
        private int ChannelTimeout = 10;
        private DateTime lastTransaction;
        private double timeout;

        public UserTransaction(CommandHandler handler, IUser self, ITextChannel originalChannel, bool isBlocking, bool allowEarning, params IUser[] participants)
        {
            Self = self;
            Participants = participants.ToList();
            LastInteraction = DateTime.Now;
            IsBlocking = isBlocking;
            AllowEarning = allowEarning;
            Complete = false;
            if (IsBlocking)
            {
                handler.UsersBlockedFromTransactions.AddRange(participants.Select(p => p.Id));
            }

            this.handler = handler;
            this.originalChannel = originalChannel;
        }

        public bool Complete { get; private set; }
        public bool IsBlocking { get; set; }

        public DateTime LastInteraction
        {
            get
            {
                if (ParentTransaction == null)
                {
                    return lastTransaction;
                }
                else
                {
                    return ParentTransaction.LastInteraction;
                }
            }
            set
            {
                if (ParentTransaction == null)
                {
                    lastTransaction = value;
                }
                else
                {
                    ParentTransaction.LastInteraction = value;
                }
            }
        }

        public Action OnFinished { get; set; }
        public UserTransaction ParentTransaction { get; set; }
        public List<IUser> Participants { get; }
        public IUser Self { get; private set; }

        public void Cancel(int timeout = 10)
        {
            ChannelTimeout = timeout;
            cts.Cancel();
        }

        public void Dispose()
        {
            cts.Cancel();
            cts.Dispose();
        }

        public async Task<ITextChannel> Start(string channelName, double timeout, IGuild guild, string startmessage)
        {
            ulong categoryId = 0;
            this.timeout = timeout;
            using (var db = new DatabaseContext())
            {
                categoryId = db.Settings.GetULongValue(Setting.BotChannelsCategoryId);
            }

            // create new channel for the bot and the user
            Channel = await guild.CreateTextChannelAsync(channelName, prop =>
            {
                prop.CategoryId = handler.BotChannelCategoryId;
                prop.Position = 1000;
            }).ConfigureAwait(false);

            await Channel.AddPermissionOverwriteAsync(handler.Client.CurrentUser, new OverwritePermissions(viewChannel: PermValue.Allow));

            foreach (var participant in Participants)
            {
                await Channel.AddPermissionOverwriteAsync(participant, new OverwritePermissions(viewChannel: PermValue.Allow));
            }

            await Channel.AddPermissionOverwriteAsync(guild.EveryoneRole, new OverwritePermissions(viewChannel: PermValue.Deny));

            string message = startmessage;

            startmessage += Environment.NewLine + $"This channel will be deleted after the transaction is complete *or after {timeout} minutes of inactivity.*" + Environment.NewLine +
                $"> For all you do please **wait until your reaction clears before you react again**, this can take some time due to Discord rate limits. It is possible that the bot does not react to your choice. Wait a bit, remove the reaction and react again.";

            if (IsBlocking)
            {
                if (Participants.Count == 1)
                {
                    startmessage += Environment.NewLine + $"**! ! ! You have been blocked from all transactions (earning, trading, upgrading) as long as this transaction is running ! ! !**";
                }
                else
                {
                    startmessage += Environment.NewLine + $"**! ! ! Users {string.Join(", ", Participants.Select(p => p.Username))} have been blocked from all transactions (earning, trading, upgrading) as long as this transaction is running ! ! !**";
                }
            }

            if (AllowEarning)
            {
                startmessage += Environment.NewLine + "> This channel allows earning of cards.";
            }

            _ = await Channel.SendMessageAsync(startmessage);

            _ = RunUntilTimeoutOrCancel(cts.Token);

            using (var db = new DatabaseContext())
            {
                db.Add(new Channel() { ChannelId = Channel.Id, ChannelType = Utilities.ChannelType.Transaction });
                if (AllowEarning)
                {
                    db.Add(new Channel() { ChannelId = Channel.Id, ChannelType = Utilities.ChannelType.Earning });
                }
                await db.SaveChangesAsync();
            }

            handler.UpdateChannels();

            return Channel;
        }

        private async Task RunUntilTimeoutOrCancel(CancellationToken token)
        {
            while ((DateTime.Now - LastInteraction).TotalMinutes < timeout)
            {
                try
                {
                    await Task.Delay(TimeSpan.FromSeconds(5), token);
                }
                catch { }
                if (token.IsCancellationRequested)
                    break;
            }

            if (IsBlocking)
            {
                handler.UsersBlockedFromTransactions.RemoveAll(u => Participants.Select(p => p.Id).Contains(u));
            }

            await Channel.SendMessageAsync($"This channel will now close in {ChannelTimeout} seconds. Head back to " + originalChannel.Mention);

            await Task.Delay(TimeSpan.FromSeconds(ChannelTimeout));

            OnFinished?.Invoke();

            try
            {
                using (var db = new DatabaseContext())
                {
                    var channel = (db.Channels as IQueryable<Channel>).Where(c => c.ChannelId == Channel.Id);
                    db.RemoveRange(channel);
                    await db.SaveChangesAsync();
                }

                handler.UpdateChannels();

                await (Channel as RestTextChannel).DeleteAsync();
            }
            catch (Exception ex)
            {
                await CommandHandler.Log(new LogMessage(LogSeverity.Error, "UserTransaction", "Failed to remove channel", ex));
            }

            Complete = true;
        }
    }
}