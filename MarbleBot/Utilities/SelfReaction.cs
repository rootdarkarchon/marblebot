﻿using Discord;

namespace MarbleBot.Utilities
{
    public class SelfReaction
    {
        public IEmote Emote;

        public ulong MessageId;

        public ulong UserId;

        public SelfReaction(ulong messageId, ulong userId, IEmote emote)
        {
            MessageId = messageId;
            Emote = emote;
            UserId = userId;
        }
    }
}