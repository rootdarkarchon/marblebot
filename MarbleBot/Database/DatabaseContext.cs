﻿using MarbleBot.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace MarbleBot.Database
{
    public class DatabaseContext : DbContext
    {
        private static bool _created = false;

        public DatabaseContext()
        {
            if (!_created)
            {
                _created = true;
#if DEBUG
                Database.EnsureDeleted();
                Database.EnsureCreated();
#endif
            }

            this.ChangeTracker.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Card>().Property(t => t.Name).HasColumnType("TEXT COLLATE NOCASE");
            builder.Entity<Team>().Property(t => t.Name).HasColumnType("TEXT COLLATE NOCASE");
            builder.Entity<Template>().Property(t => t.Name).HasColumnType("TEXT COLLATE NOCASE");

            builder.Entity<Card>()
                .HasIndex(u => u.Name)
                .IsUnique();
            builder.Entity<Team>()
                .HasIndex(u => u.Name)
                .IsUnique();
            builder.Entity<Template>()
                .HasIndex(u => u.Name)
                .IsUnique();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlite(@"Data Source=MarbleBot.db");
        }

        public List<UserCard> GetCardsForUser(ulong userId)
        {
            return UserCards
                    .Include(u => u.BaseCard)
                    .Include(u => u.BaseCard.Template)
                    .Include(u => u.BaseCard.Template.TemplateFields)
                    .Include(u => u.BaseCard.Team)
                    .Include(u => u.Owner)
                    .Where(u => u.Owner.DiscordId == userId)
                    .ToList();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Channel> Channels { get; set; }
        public DbSet<UserCard> UserCards { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<TemplateField> TemplateFields { get; set; }
        public DbSet<Team> Teams { get; set; }
    }
}