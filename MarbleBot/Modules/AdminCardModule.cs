﻿using DataStructures.RandomSelector;
using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using MarbleBot.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    [Group("card")]
    [RequireAdminChannel]
    [RequireModerator]
    public class AdminCardModule : ModuleBase<SocketCommandContext>
    {
        public AdminCardModule(CommandHandler handler)
        {
            Handler = handler;
        }

        public CommandHandler Handler { get; }

        [Command("add")]
        public async Task AddCard(string name, Team team, Template template, string imagePath, int quantity,
            int value = 1, CardRarity rarity = CardRarity.Common, CardType type = CardType.Normal, string description = "",
            int strength = 0, int endurance = 0, int power = 0, int balance = 0, int teamwork = 0)
        {
            Discord.Attachment attachment;
            Card c = new Card()
            {
                Name = name,
                Team = team,
                Template = template,
                ImagePath = imagePath,
                Quantity = quantity,
                Value = value,
                Rarity = rarity,
                Type = type,
                Strength = strength,
                Endurance = endurance,
                Power = power,
                Balance = balance,
                Teamwork = teamwork,
                Description = description
            };

            if (Context.Message.Attachments.Any())
            {
                attachment = Context.Message.Attachments.First();
                WebRequest req = WebRequest.Create(new Uri(attachment.Url));
                var stream = req.GetResponse().GetResponseStream();
                byte[] bytes = default;

                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    bytes = ms.ToArray();
                }

                File.WriteAllBytes(c.FullImagePath, bytes);
            }

            if (!File.Exists(c.FullImagePath))
            {
                await Context.Channel.SendMessageAsync($"Image \"{c.FullImagePath}\" not found. Correct the path or attach an image to this command.");
                return;
            }

            using (var db = new DatabaseContext())
            {
                db.Attach(c.Team);
                db.Attach(c.Template);
                db.Add(c);

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    await Context.Channel.SendMessageAsync(e.InnerException.Message);
                    return;
                }
            }

            await Context.Channel.SendMessageAsync(c.DetailString());
            Handler.UpdateCards();
        }

        [Command("delete")]
        [Preconditions.RequireBotOwner]
        public async Task DeleteCard(Card card)
        {
            List<UserCard> usercards;
            using (var db = new DatabaseContext())
            {
                usercards = db.UserCards.Include(c => c.BaseCard).Include(c => c.Owner).Where(c => c.BaseCard == card).ToList();
                db.Cards.Remove(card);
                db.UserCards.RemoveRange(usercards);
                await db.SaveChangesAsync();
            }

            Handler.UpdateCards();
            Handler.UpdateUsers();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Deleted card {card.Name}");
            foreach (var ucard in usercards.GroupBy(c => c.Owner))
            {
                sb.AppendLine($"Deleted {ucard.Count()} cards from {ucard.Key.DiscordId}");
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("display")]
        public async Task DisplayCard(Card card)
        {
            var userCard = new UserCard(card)
            {
                Id = DateTime.Now.Millisecond
            };
            await Context.Channel.SendMessageAsync(embed: userCard.GetEmbed());
        }

        [Command("get")]
        public async Task GetCard(Card card)
        {
            await Context.Channel.SendMessageAsync(card.DetailString());
        }

        [Command("list")]
        public async Task GetCardList(CardRarity cardRarity = CardRarity.Undefined)
        {
            List<Card> cards;
            using (var db = new DatabaseContext())
            {
                if (cardRarity == CardRarity.Undefined)
                {
                    cards = (db.Cards as IQueryable<Card>).Include(c => c.Template).Include(c => c.Team).ToList();
                }
                else
                {
                    cards = (db.Cards as IQueryable<Card>).Include(c => c.Template).Include(c => c.Team).Where(c => c.Rarity == cardRarity).ToList();
                }
            }

            StringBuilder sb = new StringBuilder();
            int stringLenth = 0;
            foreach (var card in cards)
            {
                string append = card.DetailString();
                if (stringLenth + append.Length + Environment.NewLine.Length > 2000)
                {
                    await Context.Channel.SendMessageAsync(sb.ToString());
                    sb = new StringBuilder();
                    stringLenth = 0;
                }
                stringLenth += append.Length + Environment.NewLine.Length;
                sb.AppendLine(append);
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Group("quantity")]
        public class AdminCardQuantityModule : ModuleBase<SocketCommandContext>
        {
            [Command("status")]
            public async Task Status()
            {
                List<Card> Cards;
                using (var db = new DatabaseContext())
                {
                    Cards = db.Cards.ToList();
                }

                CardRarity[] rarities = new CardRarity[] { CardRarity.Common, CardRarity.Uncommon, CardRarity.Rare, CardRarity.Epic, CardRarity.Undefined };
                float[] rarityWeights = new float[]
                {
                    Cards.Where(c=>c.Rarity == CardRarity.Common).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Uncommon).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Rare).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Epic).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Undefined).Sum(c=>c.Quantity),
                };

                DynamicRandomSelector<CardRarity> randomSelector = new DynamicRandomSelector<CardRarity>(rarities, rarityWeights);

                StringBuilder sb = new StringBuilder();
                int counter = 0;
                for (int i = 0; i < rarities.Length; i++)
                {
                    if (rarityWeights[i] > 0)
                    {
                        float prevCDL = 0;
                        if (i != 0)
                        {
                            prevCDL = randomSelector.CDL[counter - 1];
                        }
                        sb.Append($"{rarities[i]}: (Available: {rarityWeights[i]}) {(randomSelector.CDL[counter++] - prevCDL).ToString("P4")}, ");
                    }
                }

                await Context.Channel.SendMessageAsync($"Current quantities and chances: " + sb.ToString());
            }

            [Command("add")]
            public async Task AddQuantity(CardRarity rarity, int quantity)
            {
                int totalAdded = 0;
                int totalCardsAdjusted = 0;
                using (var db = new DatabaseContext())
                {
                    var cardsToAdjust = db.Cards.ToList().Where(r => r.Rarity == rarity);
                    foreach (var card in cardsToAdjust)
                    {
                        card.Quantity += quantity;
                        totalAdded += quantity;
                    }

                    totalCardsAdjusted = cardsToAdjust.Count();

                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Adjusted {totalCardsAdjusted} cards, added a total of {totalAdded} cards to the database.");
            }

            [Command("remove")]
            public async Task RemoveQuantity(CardRarity rarity, int quantity)
            {
                int totalRemoved = 0;
                int totalCardsAdjusted = 0;
                try
                {
                    using (var db = new DatabaseContext())
                    {
                        var cardsToAdjust = db.Cards.ToList().Where(r => r.Rarity == rarity);
                        foreach (var card in cardsToAdjust)
                        {
                            var orgQuantity = card.Quantity;
                            card.Quantity -= quantity;
                            if (card.Quantity < 0)
                            {
                                throw new Exception($"Card \"{card.Name}\" has not enough quantity available ({orgQuantity}) and will go below 0. No adjustments have been made.");
                            }

                            totalRemoved += quantity;
                        }

                        totalCardsAdjusted = cardsToAdjust.Count();

                        await db.SaveChangesAsync();
                    }

                    await Context.Channel.SendMessageAsync($"Adjusted {totalCardsAdjusted} cards, removed a total of {totalRemoved} cards from the database.");
                }
                catch (Exception e)
                {
                    await Context.Channel.SendMessageAsync($"{e.Message}");
                }
            }
        }

        [Group("set")]
        public class AdminCardSetModule : ModuleBase<SocketCommandContext>
        {
            public AdminCardSetModule(CommandHandler handler)
            {
                Handler = handler;
            }

            public CommandHandler Handler { get; }

            [Command]
            public async Task SetCard(Card card, CardProperty property, string value)
            {
                PropertyInfo[] prop = typeof(Card).GetProperties();
                var actualProp = prop.FirstOrDefault(p => p.Name.ToLower() == property.ToString().ToLower());

                TypeConverter tc = TypeDescriptor.GetConverter(actualProp.PropertyType);

                object convertedValue;
                try
                {
                    convertedValue = tc.ConvertFromString(null, CultureInfo.InvariantCulture, value);
                }
                catch
                {
                    await Context.Channel.SendMessageAsync("Could not parse \"" + value + "\" to " + actualProp.PropertyType.Name);
                    return;
                }

                actualProp.SetValue(card, convertedValue);

                using (var db = new DatabaseContext())
                {
                    db.Update(card);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Set {property} of {card.Name} to {value}");
            }

            [Command("team")]
            public async Task SetCard(Card card, Team team)
            {
                card.Team = team;

                using (var db = new DatabaseContext())
                {
                    db.Update(card);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Set Team of {card.Name} to {team.Name}");
            }

            [Command("template")]
            public async Task SetCard(Card card, Template template)
            {
                card.Template = template;

                using (var db = new DatabaseContext())
                {
                    db.Update(card);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Set Template of {card.Name} to {template.Name}");
            }

            [Command("image")]
            public async Task SetImage(Card card)
            {
                Discord.Attachment attachment;
                if (Context.Message.Attachments.Any())
                {
                    attachment = Context.Message.Attachments.First();
                    WebRequest req = WebRequest.Create(new Uri(attachment.Url));
                    var stream = req.GetResponse().GetResponseStream();
                    byte[] bytes = default;
                    using (var ms = new MemoryStream())
                    {
                        stream.CopyTo(ms);
                        bytes = ms.ToArray();
                    }
                    File.WriteAllBytes(Path.Combine(card.FullImagePath), bytes);
                }
                else
                {
                    await Context.Channel.SendMessageAsync("You need to attach an image.");
                    return;
                }

                using (var db = new DatabaseContext())
                {
                    db.Update(card);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Updated card {card.Name}");
            }

            protected override void AfterExecute(CommandInfo command)
            {
                Handler.UpdateCards();
            }
        }
    }
}