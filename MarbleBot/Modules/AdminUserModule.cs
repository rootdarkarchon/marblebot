﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using MarbleBot.Database;
using MarbleBot.Interfaces;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using MarbleBot.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    [Group("user")]
    [RequireAdminChannel]
    [RequireModerator]
    public class AdminUserModule : ModuleBase<SocketCommandContext>
    {
        public AdminUserModule(CommandHandler handler)
        {
            Handler = handler;
        }

        public CommandHandler Handler { get; }

        [Command("mod")]
        [Preconditions.RequireBotOwner]
        public async Task AddModerator(User user)
        {
            using (var db = new DatabaseContext())
            {
                user.IsModerator = true;
                db.Update(user);

                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"{user.DiscordId} set as moderator");
            Handler.UpdateUsers();
        }

        [Command("owner")]
        [RequireBotOwner]
        public async Task AddOwner(User user)
        {
            using (var db = new DatabaseContext())
            {
                user.IsOwner = true;
                db.Update(user);

                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"{user.DiscordId} set as owner");
            Handler.UpdateUsers();
        }

        [Command("ban")]
        public async Task BanUser(User user)
        {
            using (var db = new DatabaseContext())
            {
                user.IsBanned = true;
                db.Update(user);

                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"{user.DiscordId} was banned from the game");
            Handler.UpdateUsers();
        }

        [Command("cards")]
        public async Task GetUsercards(User user)
        {
            List<UserCard> cards;
            using (var db = new DatabaseContext())
            {
                cards = db.GetCardsForUser(user.DiscordId);
            }

            if (cards == null || cards.Count == 0)
            {
                await Context.Channel.SendMessageAsync("Cards for user " + user.DiscordId + " not found");
                return;
            }

            StringBuilder sb = new StringBuilder();

            foreach (var card in cards)
            {
                sb.AppendLine($"#{card.Id} {card}");
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("give")]
        public async Task GiveCard(User user, Card card)
        {
            ulong botchannel = 0;
            using (var db = new DatabaseContext())
            {
                var channel = (db.Channels.FirstOrDefault(c => c.ChannelType == Utilities.ChannelType.BotChatter));
                if (channel != null) botchannel = channel.ChannelId;
            }

            if (botchannel == 0)
            {
                await Context.Channel.SendMessageAsync("bot chatter channel not found.");
            }

            UserCard userCard = new UserCard(card);

            using (var db = new DatabaseContext())
            {
                user.LastEarnedCard = DateTime.Now;
                user.OwnedCards.Add(userCard);
                db.Update(user);
                card.Quantity--;
                db.Update(card);
                await db.SaveChangesAsync();
            }

            Handler.UpdateUsers();
            Handler.UpdateCards();

            IUser actualUser = Context.Guild.GetUser(user.DiscordId);

            await Context.Channel.SendMessageAsync($"Gave {userCard} [#{userCard.Id}] to {actualUser}");

            var botChannel = (Context.Guild.GetChannel(botchannel) as ISocketMessageChannel);
            await botChannel.SendMessageAsync("Congratulations " + actualUser.Mention + " you got a card: " + userCard.ToString(), embed: userCard.GetEmbed());
        }

        [Command("random")]
        public async Task GiveRandom(User user, CardRarity rarity = CardRarity.Undefined, CardType type = CardType.Normal)
        {
            Card card = null;
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (rarity == CardRarity.Undefined)
                    {
                        card = (db.Cards as IQueryable<Card>).Include(c => c.Template).Include(c => c.Template.TemplateFields).Include(c => c.Team).Where(c => c.Quantity > 0 && c.Type == type).PickRandom();
                    }
                    else
                    {
                        card = (db.Cards as IQueryable<Card>).Include(c => c.Template).Include(c => c.Template.TemplateFields).Include(c => c.Team).Where(c => c.Rarity == rarity && c.Quantity > 0).PickRandom();
                    }
                }
                catch { }
            }

            if (card == null)
            {
                await Context.Channel.SendMessageAsync("No more cards available.");
                return;
            }

            await GiveCard(user, card);
        }

        [Command("legendary")]
        public async Task GiveRandomLegendary(User user)
        {
            await GiveRandom(user, CardRarity.Undefined, CardType.Legendary);
        }

        [Command("locked")]
        public async Task Locked()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Users currently locked out:");
            foreach (var user in Handler.UsersBlockedFromTransactions)
            {
                sb.AppendLine(user.ToString());
            }
            if (Handler.UsersBlockedFromTransactions.Count == 0)
            {
                sb.AppendLine("Nobody");
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("unmod")]
        [RequireBotOwner]
        public async Task RemoveModerator(User user)
        {
            using (var db = new DatabaseContext())
            {
                user.IsModerator = false;
                db.Update(user);

                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"{user.DiscordId} removed as moderator");
            Handler.UpdateUsers();
        }

        [Command("unowner")]
        [RequireOwner]
        public async Task RemoveOwner(User user)
        {
            using (var db = new DatabaseContext())
            {
                user.IsOwner = false;
                db.Update(user);

                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"{user.DiscordId} removed as owner");
            Handler.UpdateUsers();
        }

        [Command("remove")]
        public async Task RemoveUserCard(User user, UserCard card)
        {
            if (card.Owner.DiscordId != user.DiscordId)
            {
                await Context.Channel.SendMessageAsync("Card with " + card.Id + " for user " + user.DiscordId + " not found");
            }

            using (var db = new DatabaseContext())
            {
                card.Owner.OwnedCards.Remove(card);
                db.UserCards.Remove(card);
                card.BaseCard.Quantity++;

                await db.SaveChangesAsync();
            }

            Handler.UpdateCards();
            Handler.UpdateUsers();

            await Context.Channel.SendMessageAsync($"Removed {card} from {user.DiscordId}.");
        }

        [Command("unban")]
        public async Task UnbanUser(User user)
        {
            using (var db = new DatabaseContext())
            {
                user.IsBanned = false;
                db.Update(user);

                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"{user.DiscordId} unbanned from the game");
            Handler.UpdateUsers();
        }

        [Command("unlock")]
        public async Task Unlock(User user)
        {
            List<ulong> unlockedUsers = new List<ulong>();
            foreach (var reactable in new List<ReactableBase>(Handler.Reactables.Where(p => p.Transaction.Participants.Any(c => c.Id == user.DiscordId))))
            {
                reactable.Transaction.Cancel();

                foreach (var part in reactable.Transaction.Participants)
                {
                    if (!unlockedUsers.Contains(part.Id))
                    {
                        unlockedUsers.Add(part.Id);
                    }

                    foreach (var reactablepart in new List<ReactableBase>(Handler.Reactables).Where(p => p.Transaction.Participants.Any(c => c.Id == part.Id)))
                    {
                        Handler.Reactables.Remove(reactablepart);

                        reactable.Transaction.Cancel();
                    }
                }
            }

            if (Handler.UsersBlockedFromTransactions.Contains(user.DiscordId))
            {
                unlockedUsers.Add(user.DiscordId);
                Handler.UsersBlockedFromTransactions.Remove(user.DiscordId);
            }

            if (unlockedUsers.Count > 0)
            {
                await Context.Channel.SendMessageAsync($"{string.Join(", ", unlockedUsers)} was/were unlocked from any ongoing transactions.");
            }
            else
            {
                await Context.Channel.SendMessageAsync($"No blocking transactions found for {user.DiscordId}");
            }
        }

        [Command("upgrade")]
        public async Task UpgradeUserCard(User user, UserCard card, [Remainder] UpgradeProperty[] properties)
        {
            if (card.UpgradeCount == 5) return;
            List<UpgradeProperty> upgradedProperties = new List<UpgradeProperty>();
            properties = properties.Distinct().ToArray();
            switch (card.UpgradeCount)
            {
                case 0:
                case 1:
                    upgradedProperties = properties.Take(1).ToList();
                    if (upgradedProperties.Count != 1) return;
                    break;

                case 2:
                case 3:
                    upgradedProperties = properties.Take(2).ToList();
                    if (upgradedProperties.Count != 2) return;
                    break;

                case 4:
                    upgradedProperties = properties.Take(3).ToList();
                    if (upgradedProperties.Count != 3) return;
                    break;

                default: break;
            }

            foreach (var property in upgradedProperties)
            {
                switch (property)
                {
                    case UpgradeProperty.Balance: card.UpgradedBalance++; break;
                    case UpgradeProperty.Strength: card.UpgradedStrength++; break;
                    case UpgradeProperty.Endurance: card.UpgradedEndurance++; break;
                    case UpgradeProperty.Power: card.UpgradedPower++; break;
                    case UpgradeProperty.Teamwork: card.UpgradedTeamwork++; break;
                }
            }

            card.UpgradeCount++;

            using (var db = new DatabaseContext())
            {
                db.Update(card);
                db.SaveChanges();
            }

            await Context.Channel.SendMessageAsync(embed: card.GetEmbed());
        }
    }
}