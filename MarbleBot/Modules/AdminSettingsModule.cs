﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    [Group("settings")]
    [RequireAdminChannel]
    [RequireModerator]
    public class AdminSettingsModule : ModuleBase<SocketCommandContext>
    {
        public AdminSettingsModule(CommandHandler handler)
        {
            Handler = handler;
        }

        public CommandHandler Handler { get; }

        [Command]
        public async Task GetAllSettings()
        {
            List<Setting> settings;
            using (var db = new DatabaseContext())
            {
                settings = db.Settings.ToList();
            }

            Dictionary<Setting, Type> settingToType = new Dictionary<Setting, Type>();

            foreach (var settingField in
            typeof(Setting).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.FlattenHierarchy)
            .Where(f => f.IsLiteral && !f.IsInitOnly))
            {
                var settingName = (string)settingField.GetRawConstantValue();
                var defaultValueType = (settingField.GetCustomAttributes(typeof(DefaultValueAttribute), false).Single() as DefaultValueAttribute).Value.GetType();
                settingToType.Add(settings.First(s => s.Name == settingName), defaultValueType);
            }

            var settingResponse = string.Join(Environment.NewLine, settingToType.Where(s => !s.Key.Hidden).OrderBy(s => s.Key.Name).Select(s => s.Key.Name + " [*" + s.Value.Name + "*] = " + s.Key.Value));

            await Context.Channel.SendMessageAsync(settingResponse);
        }

        [Command("set")]
        [RequireBotOwner]
        public async Task SetSetting(Setting setting, [Remainder] string settingValue)
        {
            var settingField = typeof(Setting).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.FlattenHierarchy)
            .Single(f => f.IsLiteral && !f.IsInitOnly && f.GetRawConstantValue().ToString().ToLower() == setting.Name.ToLower());

            var defaultValueType = (settingField.GetCustomAttributes(typeof(DefaultValueAttribute), false).Single() as DefaultValueAttribute).Value.GetType();

            TypeConverter tc = TypeDescriptor.GetConverter(defaultValueType);

            object result;
            try
            {
                result = tc.ConvertFromString(null, CultureInfo.InvariantCulture, settingValue);
            }
            catch
            {
                await Context.Channel.SendMessageAsync($"\"{settingValue}\" is illegal for {setting.Name}. Expected type {defaultValueType.Name}");
                return;
            }

            setting.Value = result.ToString();

            using (var db = new DatabaseContext())
            {
                db.Update(setting);
                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"Set {setting.Name} to {setting.Value}.");
            Handler.UpdateSettings();
        }
    }
}