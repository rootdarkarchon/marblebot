﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    [Group("template")]
    [RequireAdminChannel]
    [RequireModerator]
    public class AdminTemplateModule : ModuleBase<SocketCommandContext>
    {
        public AdminTemplateModule(CommandHandler handler)
        {
            Handler = handler;
        }

        public enum TemplateFieldProperty
        {
            Text,
            X,
            Y,
            Width,
            Height,
            FontSize,
            Color,
            FontName,
            Alignment,
            LineAlignment,
            DisplayWhenZero,
            FontStyle,
            ScaleMode
        }

        public CommandHandler Handler { get; }

        [Command("add")]
        public async Task AddTemplate(string name, string imagePath)
        {
            Template template = new Template()
            {
                Name = name,
                ImagePath = imagePath,
            };

            Discord.Attachment attachment;
            if (Context.Message.Attachments.Any())
            {
                attachment = Context.Message.Attachments.First();
                WebRequest req = WebRequest.Create(new Uri(attachment.Url));
                var stream = req.GetResponse().GetResponseStream();
                byte[] bytes = default;
                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    bytes = ms.ToArray();
                }
                File.WriteAllBytes(template.FullImagePath, bytes);
            }

            if (!File.Exists(template.FullImagePath))
            {
                await Context.Channel.SendMessageAsync($"Image \"{imagePath}\" not found. Correct the path or attach an image to this command.");
                return;
            }

            using (var db = new DatabaseContext())
            {
                db.Add(template);

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    await Context.Channel.SendMessageAsync(e.InnerException.Message);
                    return;
                }
            }

            await Context.Channel.SendMessageAsync(template.DetailString());
            Handler.UpdateCards();
        }

        [Command("clone")]
        public async Task CloneTemplate(Template template)
        {
            Template clone = template.Clone();
            using (var db = new DatabaseContext())
            {
                db.Add(clone);
                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"Cloned {template.ToString()}--> {clone.ToString()}");

            Handler.UpdateCards();
        }

        [Command("delete")]
        [Preconditions.RequireBotOwner]
        public async Task DeleteTemplate(Template template)
        {
            List<TemplateField> templateFields;
            using (var db = new DatabaseContext())
            {
                templateFields = (db.TemplateFields as IQueryable<TemplateField>).Where(c => c.AssociatedTemplate == template).ToList();
                db.Templates.Remove(template);
                db.TemplateFields.RemoveRange(templateFields);
                await db.SaveChangesAsync();
            }

            Handler.UpdateCards();
            Handler.UpdateUsers();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Deleted template {template.Name}");
            foreach (var ucard in templateFields)
            {
                sb.AppendLine($"Deleted field {ucard.Id}");
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("get")]
        public async Task GetTemplate(Template template)
        {
            StringBuilder sb = new StringBuilder();
            int stringLenth = 0;
            sb.AppendLine(template.ToString());
            foreach (var field in template.TemplateFields)
            {
                string append = field.ToString();
                if (stringLenth + append.Length + Environment.NewLine.Length > 1900)
                {
                    await Context.Channel.SendMessageAsync(sb.ToString());
                    sb = new StringBuilder();
                    stringLenth = 0;
                }
                stringLenth += append.Length + Environment.NewLine.Length;
                sb.AppendLine(append);
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("list")]
        public async Task GetTemplateList()
        {
            List<Template> templates;
            using (var db = new DatabaseContext())
            {
                templates = (db.Templates as IQueryable<Template>).Include(c => c.TemplateFields).ToList();
            }

            StringBuilder sb = new StringBuilder();
            int stringLenth = 0;
            foreach (var template in templates)
            {
                string append = template.ToString();
                if (stringLenth + append.Length + Environment.NewLine.Length > 2000)
                {
                    await Context.Channel.SendMessageAsync(sb.ToString());
                    sb = new StringBuilder();
                    stringLenth = 0;
                }
                stringLenth += append.Length + Environment.NewLine.Length;
                sb.AppendLine(append);
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("replace")]
        public async Task ReplaceTemplate(Template source, Template newtemplate)
        {
            List<Card> cardList;
            using (var db = new DatabaseContext())
            {
                cardList = db.Cards.Include(c => c.Template).Where(c => c.Template == source).ToList();
                foreach (var card in cardList)
                {
                    card.Template = newtemplate;
                }

                await db.SaveChangesAsync();
            }

            await Context.Channel.SendMessageAsync($"Replaced template {source.Name} with {newtemplate.Name} for {cardList.Count} cards.");
            Handler.UpdateCards();
        }

        [Group("field")]
        public class AdminTemplateFieldModule : ModuleBase<SocketCommandContext>
        {
            public AdminTemplateFieldModule(CommandHandler handler)
            {
                Handler = handler;
            }

            public CommandHandler Handler { get; }

            [Command("add")]
            public async Task AddTemplateField(Template template, string text, int x, int y,
                int width = 0, int height = 0, float fontsize = 0, string color = "", string fontname = "",
                System.Drawing.StringAlignment horizontalAlignment = System.Drawing.StringAlignment.Center,
                System.Drawing.StringAlignment verticalAlignment = System.Drawing.StringAlignment.Center,
                bool displayWhenZero = true, System.Drawing.FontStyle fontStyle = System.Drawing.FontStyle.Regular,
                ScaleMode scaleMode = ScaleMode.FitToContent)
            {
                TemplateField templateField = new TemplateField()
                {
                    AssociatedTemplate = template,
                    Text = text,
                    X = x,
                    Y = y,
                    Width = width,
                    Height = height,
                    FontSize = fontsize,
                    Color = color,
                    FontName = fontname,
                    Alignment = horizontalAlignment,
                    LineAlignment = verticalAlignment,
                    DisplayWhenZero = displayWhenZero,
                    FontStyle = fontStyle,
                    ScaleMode = scaleMode
                };

                if (text.StartsWith('[') && text.EndsWith(']'))
                {
                    string potentialImagePath = text.TrimStart('[').TrimEnd(']');

                    if (!File.Exists(potentialImagePath) && !Context.Message.Attachments.Any())
                    {
                        await Context.Channel.SendMessageAsync($"Image \"{potentialImagePath}\" not found. Correct the path or attach an image to this command.");
                        return;
                    }

                    Discord.Attachment attachment;
                    if (Context.Message.Attachments.Any())
                    {
                        attachment = Context.Message.Attachments.First();
                        WebRequest req = WebRequest.Create(new Uri(attachment.Url));
                        var stream = req.GetResponse().GetResponseStream();
                        byte[] bytes = default;
                        using (var ms = new MemoryStream())
                        {
                            stream.CopyTo(ms);
                            bytes = ms.ToArray();
                        }
                        if (!Directory.Exists(Path.GetDirectoryName(potentialImagePath)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(potentialImagePath));
                        }
                        File.WriteAllBytes(potentialImagePath, bytes);
                    }
                }

                using (var db = new DatabaseContext())
                {
                    db.Attach(template);
                    db.Add(templateField);

                    try
                    {
                        await db.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        await Context.Channel.SendMessageAsync(e.InnerException.Message);
                        return;
                    }
                }

                await Context.Channel.SendMessageAsync(templateField.ToString());
                Handler.UpdateCards();
            }

            [Command("delete")]
            [Preconditions.RequireBotOwner]
            public async Task DeleteTemplateField(TemplateField template)
            {
                using (var db = new DatabaseContext())
                {
                    db.TemplateFields.Remove(template);
                    await db.SaveChangesAsync();
                }

                Handler.UpdateCards();
                Handler.UpdateUsers();

                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"Deleted templatefield {template.Id}");

                await Context.Channel.SendMessageAsync(sb.ToString());
            }

            [Command("get")]
            public async Task GetTemplateField(TemplateField template)
            {
                await Context.Channel.SendMessageAsync(template.ToString());
            }

            [Command("list")]
            public async Task GetTemplateFieldList(Template t)
            {
                List<TemplateField> templates;
                using (var db = new DatabaseContext())
                {
                    templates = (db.Templates as IQueryable<TemplateField>).Include(c => c.AssociatedTemplate).Where(c => c.AssociatedTemplate == t).ToList();
                }

                StringBuilder sb = new StringBuilder();
                int stringLenth = 0;
                foreach (var template in templates)
                {
                    string append = template.ToString();
                    if (stringLenth + append.Length + Environment.NewLine.Length > 2000)
                    {
                        await Context.Channel.SendMessageAsync(sb.ToString());
                        sb = new StringBuilder();
                        stringLenth = 0;
                    }
                    stringLenth += append.Length + Environment.NewLine.Length;
                    sb.AppendLine(append);
                }

                await Context.Channel.SendMessageAsync(sb.ToString());
            }

            [Command("set")]
            public async Task SetTemplateField(TemplateField field, TemplateFieldProperty property, string value)
            {
                PropertyInfo[] prop = typeof(TemplateField).GetProperties();
                var actualProp = prop.FirstOrDefault(p => p.Name.ToLower() == property.ToString().ToLower());

                TypeConverter tc = TypeDescriptor.GetConverter(actualProp.PropertyType);

                object convertedValue;
                try
                {
                    convertedValue = tc.ConvertFromString(null, CultureInfo.InvariantCulture, value);
                }
                catch
                {
                    await Context.Channel.SendMessageAsync("Could not parse \"" + value + "\" to " + actualProp.PropertyType.Name);
                    return;
                }

                actualProp.SetValue(field, convertedValue);

                using (var db = new DatabaseContext())
                {
                    db.Update(field);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Set {property} of {field.Id} to {value}");
            }
        }

        [Group("set")]
        public class AdminTemplateSetModule : ModuleBase<SocketCommandContext>
        {
            public AdminTemplateSetModule(CommandHandler handler)
            {
                Handler = handler;
            }

            public CommandHandler Handler { get; }

            [Command("image")]
            public async Task SetImage(Template template)
            {
                Discord.Attachment attachment;
                if (Context.Message.Attachments.Any())
                {
                    attachment = Context.Message.Attachments.First();
                    WebRequest req = WebRequest.Create(new Uri(attachment.Url));
                    var stream = req.GetResponse().GetResponseStream();
                    byte[] bytes = default;
                    using (var ms = new MemoryStream())
                    {
                        stream.CopyTo(ms);
                        bytes = ms.ToArray();
                    }
                    File.WriteAllBytes(template.FullImagePath, bytes);
                }
                else
                {
                    await Context.Channel.SendMessageAsync("You need to attach an image.");
                    return;
                }

                using (var db = new DatabaseContext())
                {
                    db.Update(template);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Updated image of {template.Name}");
                Handler.UpdateCards();
            }

            [Command("imagepath")]
            public async Task SetImagePath(Template template, string imagePath)
            {
                template.ImagePath = imagePath;
                if (!File.Exists(template.FullImagePath))
                {
                    await Context.Channel.SendMessageAsync($"Image \"{imagePath}\" not found. Correct the path or attach an image to this command.");
                    return;
                }

                using (var db = new DatabaseContext())
                {
                    db.Update(template);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Set ImagePath of Template to {imagePath}");
                Handler.UpdateCards();
            }

            [Command("name")]
            public async Task SetNme(Template template, string name)
            {
                template.Name = name;

                using (var db = new DatabaseContext())
                {
                    db.Update(template);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Set Name of Template to {name}");
                Handler.UpdateCards();
            }
        }
    }
}