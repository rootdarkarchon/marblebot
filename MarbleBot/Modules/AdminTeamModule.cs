﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    [Group("team")]
    [RequireAdminChannel]
    [RequireModerator]
    public class AdminTeamModule : ModuleBase<SocketCommandContext>
    {
        public AdminTeamModule(CommandHandler handler)
        {
            Handler = handler;
        }

        public CommandHandler Handler { get; }

        [Command("add")]
        public async Task AddTeam(string name, string imagePath)
        {
            Team team = new Team()
            {
                Name = name,
                ImagePath = imagePath,
            };

            Discord.Attachment attachment;
            if (Context.Message.Attachments.Any())
            {
                attachment = Context.Message.Attachments.First();
                WebRequest req = HttpWebRequest.Create(new Uri(attachment.Url));
                var stream = req.GetResponse().GetResponseStream();
                byte[] bytes = default;
                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    bytes = ms.ToArray();
                }
                File.WriteAllBytes(team.FullImagePath, bytes);
            }

            if (!File.Exists(team.FullImagePath))
            {
                await Context.Channel.SendMessageAsync($"Image \"{imagePath}\" not found. Correct the path or attach an image to this command.");
                return;
            }

            using (var db = new DatabaseContext())
            {
                db.Add(team);

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    await Context.Channel.SendMessageAsync(e.InnerException.Message);
                    return;
                }
            }

            await Context.Channel.SendMessageAsync(team.DetailString());
            Handler.UpdateCards();
        }

        [Command("delete")]
        [Preconditions.RequireBotOwner]
        public async Task DeleteTeam(Team team)
        {
            List<Card> cards;
            List<UserCard> usercards;
            using (var db = new DatabaseContext())
            {
                cards = (db.Cards as IQueryable<Card>).Where(c => c.Team == team).ToList();
                usercards = db.UserCards.Include(c => c.BaseCard).Include(c => c.Owner).Where(c => cards.Any(b => b == c.BaseCard)).ToList();
                db.UserCards.RemoveRange(usercards);
                db.Cards.RemoveRange(cards);
                db.Teams.Remove(team);

                await db.SaveChangesAsync();
            }

            Handler.UpdateCards();
            Handler.UpdateUsers();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Deleted team {team.Name}");
            foreach (var card in cards)
            {
                sb.AppendLine($"Deleted card {card.Name}");
            }
            foreach (var card in usercards.GroupBy(c => c.Owner))
            {
                sb.AppendLine($"Deleted {card.Count()} cards from {card.Key.DiscordId}");
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("get")]
        public async Task GetTeam(Team team)
        {
            await Context.Channel.SendMessageAsync(team.DetailString());
        }

        [Command("list")]
        public async Task GetTeamList()
        {
            List<Team> teams;
            using (var db = new DatabaseContext())
            {
                teams = (db.Teams as IQueryable<Team>).ToList();
            }

            StringBuilder sb = new StringBuilder();
            int stringLenth = 0;
            foreach (var team in teams)
            {
                string append = team.DetailString();
                if (stringLenth + append.Length + Environment.NewLine.Length > 2000)
                {
                    await Context.Channel.SendMessageAsync(sb.ToString());
                    sb = new StringBuilder();
                    stringLenth = 0;
                }
                stringLenth += append.Length + Environment.NewLine.Length;
                sb.AppendLine(append);
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Group("set")]
        public class AdminTeamSetModule : ModuleBase<SocketCommandContext>
        {
            public AdminTeamSetModule(CommandHandler handler)
            {
                Handler = handler;
            }

            public CommandHandler Handler { get; }

            [Command("name")]
            public async Task SetCard(Team team, string name)
            {
                team.Name = name;

                using (var db = new DatabaseContext())
                {
                    db.Update(team);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Set Name of Team to {name}");
                Handler.UpdateCards();
            }

            [Command("image")]
            public async Task SetImage(Team team)
            {
                Discord.Attachment attachment;
                if (Context.Message.Attachments.Any())
                {
                    attachment = Context.Message.Attachments.First();
                    WebRequest req = WebRequest.Create(new Uri(attachment.Url));
                    var stream = req.GetResponse().GetResponseStream();
                    byte[] bytes = default;
                    using (var ms = new MemoryStream())
                    {
                        stream.CopyTo(ms);
                        bytes = ms.ToArray();
                    }
                    File.WriteAllBytes(team.FullImagePath, bytes);
                }
                else
                {
                    await Context.Channel.SendMessageAsync("You need to attach an image.");
                    return;
                }

                using (var db = new DatabaseContext())
                {
                    db.Update(team);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Updated image of {team.Name}");
                Handler.UpdateCards();
            }

            [Command("imagepath")]
            public async Task SetImagePath(Team team, string imagePath)
            {
                team.ImagePath = imagePath;

                if (!File.Exists(team.FullImagePath))
                {
                    await Context.Channel.SendMessageAsync($"Image \"{imagePath}\" not found. Correct the path or attach an image to this command.");
                    return;
                }

                using (var db = new DatabaseContext())
                {
                    db.Update(team);
                    await db.SaveChangesAsync();
                }

                await Context.Channel.SendMessageAsync($"Set ImagePath of Team to {imagePath}");
                Handler.UpdateCards();
            }
        }
    }
}