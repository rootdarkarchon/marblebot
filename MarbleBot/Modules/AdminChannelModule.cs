﻿using Discord;
using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    [Group("channel")]
    [RequireModerator]
    public class AdminChannelModule : ModuleBase<SocketCommandContext>
    {
        public AdminChannelModule(CommandHandler handler)
        {
            Handler = handler;
        }

        public CommandHandler Handler { get; }

        [Command("earning add")]
        [RequireAdminChannel]
        public async Task AddEarningChannel(IChannel channel)
        {
            bool handled = false;
            using (var db = new DatabaseContext())
            {
                var dbchannel = db.Channels.SingleOrDefault(c => c.ChannelType == Utilities.ChannelType.Earning && c.ChannelId == channel.Id);
                if (dbchannel == null)
                {
                    db.Channels.Add(new Channel
                    {
                        ChannelId = channel.Id,
                        ChannelType = Utilities.ChannelType.Earning
                    });

                    handled = true;

                    await db.SaveChangesAsync();
                }
            }

            if (handled)
            {
                await Context.Channel.SendMessageAsync($"{channel} set as earning channel");
                Handler.UpdateChannels();
            }
        }

        [Command("earning remove")]
        [RequireAdminChannel]
        public async Task RemoveEarningChannel(IChannel channel)
        {
            bool handled = false;
            using (var db = new DatabaseContext())
            {
                var dbchannel = db.Channels.SingleOrDefault(c => c.ChannelType == Utilities.ChannelType.Earning && c.ChannelId == channel.Id);
                if (dbchannel != null)
                {
                    db.Channels.Remove(dbchannel);

                    handled = true;

                    await db.SaveChangesAsync();
                }
            }

            if (handled)
            {
                await Context.Channel.SendMessageAsync($"{channel} removed as earning channel");
                Handler.UpdateChannels();
            }
        }

        [Command("admin")]
        [Preconditions.RequireBotOwner]
        public async Task SetAdminChannel(IChannel channel)
        {
            bool handled = false;
            using (var db = new DatabaseContext())
            {
                var dbchannel = db.Channels.SingleOrDefault(c => c.ChannelType == Utilities.ChannelType.Admininistrative);
                if (dbchannel != null)
                {
                    db.Channels.Remove(dbchannel);
                }
                db.Channels.Add(new Channel
                {
                    ChannelId = channel.Id,
                    ChannelType = Utilities.ChannelType.Admininistrative
                });

                handled = true;

                await db.SaveChangesAsync();
            }

            if (handled)
            {
                await Context.Channel.SendMessageAsync($"{channel} set as admin channel");
                Handler.UpdateChannels();
            }
        }

        [Command("admin")]
        [RequireBotOwner]
        public async Task SetAdminChannel(ulong channelid)
        {
            bool handled = false;
            using (var db = new DatabaseContext())
            {
                var dbchannel = db.Channels.SingleOrDefault(c => c.ChannelType == Utilities.ChannelType.Admininistrative);
                if (dbchannel != null)
                {
                    db.Channels.Remove(dbchannel);
                }
                db.Channels.Add(new Channel
                {
                    ChannelId = channelid,
                    ChannelType = Utilities.ChannelType.Admininistrative
                });

                handled = true;

                await db.SaveChangesAsync();
            }

            if (handled)
            {
                await Context.Channel.SendMessageAsync($"{channelid} set as admin channel");
                Handler.UpdateChannels();
            }
        }

        [Command("bot")]
        [RequireBotOwner]
        [RequireAdminChannel]
        public async Task SetBotChannel(IChannel channel)
        {
            bool handled = false;
            using (var db = new DatabaseContext())
            {
                var dbchannel = db.Channels.SingleOrDefault(c => c.ChannelType == Utilities.ChannelType.BotChatter);
                if (dbchannel != null)
                {
                    db.Channels.Remove(dbchannel);
                }
                db.Channels.Add(new Channel
                {
                    ChannelId = channel.Id,
                    ChannelType = Utilities.ChannelType.BotChatter
                });

                handled = true;

                await db.SaveChangesAsync();
            }

            if (handled)
            {
                await Context.Channel.SendMessageAsync($"{channel} set as bot channel");
                Handler.UpdateChannels();
            }
        }
    }
}