﻿using Discord.Commands;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using MarbleBot.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    [Group("selftest")]
    [RequireAdminChannel]
    [Preconditions.RequireBotOwner]
    public class AdminSelfTestModule : ModuleBase<SocketCommandContext>
    {
        [Command("reboot")]
        public async Task Reboot()
        {
            await Context.Channel.SendMessageAsync("[Shutdown] Engine powering down");
            Program.IS_RUNNING = false;
        }

        [Command]
        public async Task Selftest()
        {
            List<Channel> channels;
            List<Setting> settings;
            List<Card> cards;
            using (var db = new DatabaseContext())
            {
                channels = db.Channels.ToList();
                settings = db.Settings.ToList();
                cards = db.Cards.ToList();
            }
            StringBuilder sb = new StringBuilder();

            await Context.Channel.SendMessageAsync("Performing selftest, give me a second");

            sb.AppendLine("```Checking channels```");
            // check if bot chatter channels exist
            sb.Append("Checking bot chatter channels: ");
            var botChatterChannels = channels.Where(c => c.ChannelType == ChannelType.BotChatter);
            sb.Append($"found {botChatterChannels.Count()} - ");
            if (botChatterChannels.Count() > 0)
            {
                sb.Append("**SUCCESS**");
            }
            else
            {
                sb.Append("***FAILURE***");
            }
            sb.AppendLine();
            // check if at least one admin channel exists
            sb.Append("Checking bot admin channels: ");
            var botAdminChannels = channels.Where(c => c.ChannelType == ChannelType.Admininistrative);
            sb.Append($"found {botAdminChannels.Count()} - ");
            if (botAdminChannels.Count() > 0)
            {
                sb.Append("**SUCCESS**");
            }
            else
            {
                sb.Append("***FAILURE***");
            }
            sb.AppendLine();
            // check if at least one earning channel exists
            sb.Append("Checking bot earning channels: ");
            var botEarningChannels = channels.Where(c => c.ChannelType == ChannelType.Earning);
            sb.Append($"found {botEarningChannels.Count()} - ");
            if (botEarningChannels.Count() > 0)
            {
                sb.Append("**SUCCESS**");
            }
            else
            {
                sb.Append("***FAILURE***");
            }
            sb.AppendLine();
            await Context.Message.Channel.SendMessageAsync(sb.ToString());
            sb = new StringBuilder();
            // check bot channel category setup
            sb.AppendLine("```Category settings and access```");
            ulong categoryid = 0;
            sb.Append("Checking category id is set: ");
            try
            {
                categoryid = settings.GetULongValue(Setting.BotChannelsCategoryId);
                sb.Append($"found {categoryid} - **SUCCESS**");
            }
            catch
            {
                sb.Append("not found - ***FAILURE***");
            }
            sb.AppendLine();
            if (categoryid > 0)
            {
                Discord.WebSocket.SocketCategoryChannel category;
                sb.Append("Checking if category id is valid: ");
                category = Context.Guild.GetCategoryChannel(categoryid);
                if (category != null)
                {
                    sb.Append($"found {category.Name} - **SUCCESS**");
                }
                else
                {
                    sb.Append("not found - ***FAILURE***");
                }
                sb.AppendLine();
                if (category != null)
                {
                    sb.Append("Checking for creating channel access in category: ");
                    Discord.Rest.RestTextChannel channel = null;
                    try
                    {
                        channel = await Context.Guild.CreateTextChannelAsync("selftest", (e) => e.CategoryId = categoryid);
                    }
                    catch { }

                    if (channel != null)
                    {
                        sb.Append($"created {channel.Name} - **SUCCESS**");
                    }
                    else
                    {
                        sb.Append("could not create channel - ***FAILURE***");
                    }
                    sb.AppendLine();
                    if (channel != null)
                    {
                        sb.Append("Checking for permission change for channel in category: ");

                        bool canAddPermissions = false;
                        try
                        {
                            await channel.AddPermissionOverwriteAsync(Context.User, new Discord.OverwritePermissions(addReactions: Discord.PermValue.Allow));
                            canAddPermissions = true;
                        }
                        catch
                        {
                        }
                        if (canAddPermissions)
                        {
                            sb.Append("can add permissions - **SUCCESS**");
                        }
                        else
                        {
                            sb.Append("cannot add permissions - ***FAILURE***");
                        }

                        sb.AppendLine();
                        sb.Append("Checking for deleting channel in category: ");
                        var channelid = channel.Id;
                        try
                        {
                            await channel.DeleteAsync();
                        }
                        catch { }
                        await Task.Delay(TimeSpan.FromSeconds(1));
                        var channelDeleted = Context.Guild.GetTextChannel(channelid);
                        if (channelDeleted == null)
                        {
                            sb.Append("channel gone - **SUCCESS**");
                        }
                        else
                        {
                            sb.Append("could not delete channel (probably missing rights to see the category) - ***FAILURE***");
                        }
                        sb.AppendLine();
                    }
                }
            }
            await Context.Message.Channel.SendMessageAsync(sb.ToString());
            sb = new StringBuilder();
            // check if card find chances are valid
            sb.AppendLine("```Checking chances settings```");

            sb.Append("Checking BASE find chance: ");
            int value;
            try
            {
                value = settings.GetIntValue(Setting.BaseFindChance);
                sb.Append($"found {value} - **SUCCESS**");
            }
            catch
            {
                value = 0;
                sb.Append("value was not found or invalid - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking COMMON find chance: ");
            try
            {
                value = settings.GetIntValue(Setting.CommonCardRarity);
                sb.Append($"found {value} - **SUCCESS**");
            }
            catch
            {
                value = 0;
                sb.Append("value was not found or invalid - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking UNCOMMON find chance: ");
            try
            {
                value = settings.GetIntValue(Setting.UncommonCardRarity);
                sb.Append($"found {value} - **SUCCESS**");
            }
            catch
            {
                value = 0;
                sb.Append("value was not found or invalid - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking RARE find chance: ");
            try
            {
                value = settings.GetIntValue(Setting.RareCardRarity);
                sb.Append($"found {value} - **SUCCESS**");
            }
            catch
            {
                value = 0;
                sb.Append("value was not found or invalid - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking EPIC find chance: ");
            try
            {
                value = settings.GetIntValue(Setting.EpicCardRarity);
                sb.Append($"found {value} - **SUCCESS**");
            }
            catch
            {
                value = 0;
                sb.Append("value was not found or invalid - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking LEGENDARY find chance: ");
            try
            {
                value = settings.GetIntValue(Setting.LegendaryCardRarity);
                sb.Append($"found {value} - **SUCCESS**");
            }
            catch
            {
                value = 0;
                sb.Append("value was not found or invalid - ***FAILURE***");
            }

            sb.AppendLine();
            await Context.Message.Channel.SendMessageAsync(sb.ToString());
            sb = new StringBuilder();
            sb.AppendLine("```Card database```");
            sb.Append("Checking COMMON card count: ");
            var commoncards = cards.Where(c => c.Rarity == CardRarity.Common).ToList();
            if (commoncards.Count > 0)
            {
                sb.Append($"found {commoncards.Count()} cards with a total of {commoncards.Sum(e => e.Quantity)} available cards - **SUCCESS**");
            }
            else
            {
                sb.Append("not found any - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking UNCOMMON card count: ");
            var uncommonCards = cards.Where(c => c.Rarity == CardRarity.Uncommon).ToList();
            if (uncommonCards.Count > 0)
            {
                sb.Append($"found {uncommonCards.Count()} cards with a total of {uncommonCards.Sum(e => e.Quantity)} available cards - **SUCCESS**");
            }
            else
            {
                sb.Append("not found any - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking RARE card count: ");
            var rareCards = cards.Where(c => c.Rarity == CardRarity.Rare).ToList();
            if (rareCards.Count > 0)
            {
                sb.Append($"found {rareCards.Count()} cards with a total of {rareCards.Sum(e => e.Quantity)} available cards - **SUCCESS**");
            }
            else
            {
                sb.Append("not found any - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking EPIC card count: ");
            var epicCards = cards.Where(c => c.Rarity == CardRarity.Epic).ToList();
            if (rareCards.Count > 0)
            {
                sb.Append($"found {epicCards.Count()} cards with a total of {epicCards.Sum(e => e.Quantity)} available cards - **SUCCESS**");
            }
            else
            {
                sb.Append("not found any - ***FAILURE***");
            }
            sb.AppendLine();
            sb.Append("Checking LEGENDARY card count: ");
            var legendaryCards = cards.Where(c => c.Type == CardType.Legendary).ToList();
            if (legendaryCards.Count > 0)
            {
                sb.Append($"found {legendaryCards.Count()} cards with a total of {legendaryCards.Sum(e => e.Quantity)} available cards - **SUCCESS**");
            }
            else
            {
                sb.Append("not found any - ***FAILURE***");
            }
            sb.AppendLine();
            await Context.Message.Channel.SendMessageAsync(sb.ToString());
            sb = new StringBuilder();
            // check if base image folder is set and can be written to
            sb.AppendLine("```Image creation paths and access```");
            string imagePath = "";
            sb.Append("Checking folder is set: ");
            try
            {
                imagePath = settings.GetStringValue(Setting.ImageFolderBase);
                if (string.IsNullOrEmpty(imagePath)) throw new Exception();
                sb.Append($"found {imagePath} - **SUCCESS**");
            }
            catch
            {
                sb.Append("value was not found or not set - ***FAILURE***");
            }
            sb.AppendLine();
            bool canWrite = false;
            if (!string.IsNullOrEmpty(imagePath))
            {
                sb.Append("Checking if folder exists: ");
                bool dirExists = Directory.Exists(imagePath);
                if (dirExists)
                {
                    sb.Append($"directory {new DirectoryInfo(imagePath).FullName} exists - **SUCCESS**");
                }
                else
                {
                    sb.Append($"directory does not exist - ***FAILURE***");
                }
                sb.AppendLine();

                if (dirExists)
                {
                    sb.Append("Checking if can write to directory: ");
                    var testFilePath = Path.Combine(imagePath, "test.txt");
                    try
                    {
                        File.WriteAllText(Path.Combine(imagePath, "test.txt"), "test");
                        canWrite = true;
                        sb.Append($"wrote file {testFilePath} - **SUCCESS**");
                    }
                    catch
                    {
                        sb.Append($"could not write file {testFilePath} - ***FAILURE***");
                        canWrite = false;
                    }
                    sb.AppendLine();

                    if (canWrite)
                    {
                        sb.Append("Checking if can delete from directory: ");
                        try
                        {
                            File.Delete(testFilePath);
                            sb.Append($"deleted {testFilePath} - **SUCCESS**");
                        }
                        catch
                        {
                            sb.Append($"could not delete {testFilePath} - ***FAILURE***");
                        }
                        sb.AppendLine();
                    }
                }
            }
            await Context.Message.Channel.SendMessageAsync(sb.ToString());
            sb = new StringBuilder();
            sb.AppendLine("```Web access```");
            string baseUrl = "";
            sb.Append("Checking URL is set: ");
            try
            {
                baseUrl = settings.GetStringValue(Setting.ExternalUrlBase);
                sb.Append($"found {baseUrl} - **SUCCESS**");
            }
            catch
            {
                sb.Append("value was not found or not set - ***FAILURE***");
            }
            sb.AppendLine();

            if (!string.IsNullOrEmpty(baseUrl) && canWrite)
            {
                sb.Append("Testing web access: ");
                File.WriteAllText(Path.Combine(imagePath, "test.txt"), "test");
                var fetchUrl = baseUrl + "test.txt";
                WebResponse response = null;
                try
                {
                    var req = WebRequest.Create(new Uri(fetchUrl));
                    response = req.GetResponse();
                }
                catch
                {
                    sb.Append($"could not fetch {fetchUrl} - ***FAILURE***");
                }

                if (response != null)
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using StreamReader reader = new StreamReader(stream);
                        var responseText = reader.ReadToEnd();
                        if (responseText == "test")
                        {
                            sb.Append("downloaded and read file successfully - **SUCCESS**");
                        }
                        else
                        {
                            sb.Append("downloaded and read file but contents differed - ***FAILURE***");
                        }
                    }
                    response.Close();
                }

                File.Delete(Path.Combine(imagePath, "test.txt"));
            }
            else if (!string.IsNullOrEmpty(baseUrl) && !canWrite)
            {
                sb.Append("Cannot proceed with testing web access, image path not found or writeable.");
            }

            // check if base url external is set and correct
            await Context.Message.Channel.SendMessageAsync(sb.ToString());
        }
    }
}