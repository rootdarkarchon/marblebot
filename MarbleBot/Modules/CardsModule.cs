﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using MarbleBot.Database;
using MarbleBot.Interfaces;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using MarbleBot.Reactables;
using MarbleBot.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    public class CardsModule : ModuleBase<SocketCommandContext>
    {
        private static bool IsDuelRunning = false;
        private static DateTime Top10LastUsed = default;

        public CardsModule(CommandHandler handler)
        {
            Handler = handler;
        }

        public CommandHandler Handler { get; }

        [Command("cards")]
        public async Task Cards()
        {
            List<UserCard> cards = new List<UserCard>();
            using (var db = new DatabaseContext())
            {
                cards = db.GetCardsForUser(Context.Message.Author.Id).ToList();
            }

            await Context.Message.Channel.SendMessageAsync($"{Context.User.Mention} you have {cards.Count} card(s) with a total value of {cards.Sum(c => c.Value + c.InvestedValue)}P.");
        }

        [Command("duel")]
        [RequireBotChatterChannel]
        [RequireUserInNoTransaction]
        [RequireMentionedUserInNoTransaction]
        [RequireMentionedUserToBeDifferent]
        [RequireUserCards]
        [RequireMentionedUserCards]
        public async Task Duel(SocketUser mentionedUser)
        {
            if (IsDuelRunning)
            {
                await Context.Channel.SendMessageAsync("Only one duel can run at a time.");
                return;
            }
            IsDuelRunning = true;
            // get users and cards
            var author = Context.Message.Author;
            var mention = mentionedUser;

            List<UserCard> authorCards;
            List<UserCard> mentionCards;
            double timeout = 2;
            int deckLimit = 30;

            using (var db = new DatabaseContext())
            {
                authorCards = db.GetCardsForUser(author.Id).Where(t => t.BaseCard.Type == CardType.Normal).ToList();
                mentionCards = db.GetCardsForUser(mention.Id).Where(t => t.BaseCard.Type == CardType.Normal).ToList();
                timeout = db.Settings.GetDoubleValue(Setting.InteractiveChannelTimeout) * 2;
                deckLimit = db.Settings.GetIntValue(Setting.UserInventoryLimit);
            }

            // create base transaction for the duel
            UserTransaction baseTransaction = new UserTransaction(Handler, Context.Client.CurrentUser, Context.Channel as ITextChannel, true, true, author, mention)
            {
                OnFinished = new Action(() => { IsDuelRunning = false; })
            };

            var duelChannel = await baseTransaction.Start($"{author.Username}-{mention.Username}-duel", timeout, Context.Guild, $"{author.Mention} this channel is for the duel with {mention.Mention}.");

            UserConfirmReactable confirmMessage = new UserConfirmReactable(mention, baseTransaction, Handler);

            // ask mentioned user if they want to accept the duel
            await confirmMessage.PostConfirmationMessage($"{mentionedUser.Mention} do you want to duel with {Context.Message.Author.Mention}?");

            if (!await confirmMessage.WaitUntilCompletion())
            {
                return;
            }

            await duelChannel.AddPermissionOverwriteAsync(Context.Guild.EveryoneRole, new OverwritePermissions(viewChannel: PermValue.Allow));
            await Context.Channel.SendMessageAsync($"A duel has started between {author.Username} and {mention.Username}! Come on in and watch in {duelChannel.Mention}!");

            // create non-blocking sub-transactions for each user
            UserTransaction authorTransaction = new UserTransaction(Handler, Context.Client.CurrentUser, duelChannel, false, false, author)
            {
                ParentTransaction = baseTransaction
            };
            UserTransaction mentionTransaction = new UserTransaction(Handler, Context.Client.CurrentUser, duelChannel, false, false, mention)
            {
                ParentTransaction = baseTransaction
            };

            DeckReactable authorDeck = new DeckReactable(author, 5, authorTransaction, Handler);
            DeckReactable mentionDeck = new DeckReactable(mention, 5, mentionTransaction, Handler);

            ExpandCardsDeckAddon expandExt = new ExpandCardsDeckAddon(Context.Client.CurrentUser);
            ExpandCardsDeckAddon expandExt2 = new ExpandCardsDeckAddon(Context.Client.CurrentUser);

            SelectCardsDeckAddon authorExt = new SelectCardsDeckAddon(Context.Client.CurrentUser, 1, 1);
            SelectCardsDeckAddon mentionExt = new SelectCardsDeckAddon(Context.Client.CurrentUser, 1, 1);

            UserEffortReactable authorEffort = new UserEffortReactable(author, authorTransaction, Handler);
            UserEffortReactable mentionEffort = new UserEffortReactable(mention, mentionTransaction, Handler);

            authorDeck.AddCardDeckReactionExtension(authorExt);
            authorDeck.AddFooterReactionAddon(authorExt);
            authorDeck.AddHeaderMessageExtension(authorExt);
            authorDeck.AddCardDeckReactionExtension(expandExt);
            authorDeck.AddFooterMessageExtension(expandExt);

            mentionDeck.AddCardDeckReactionExtension(mentionExt);
            mentionDeck.AddFooterReactionAddon(mentionExt);
            mentionDeck.AddHeaderMessageExtension(mentionExt);
            mentionDeck.AddCardDeckReactionExtension(expandExt2);
            mentionDeck.AddFooterMessageExtension(expandExt2);

            var authorChannel = await authorTransaction.Start($"{author.Username}-duel-selection", timeout, Context.Guild, $"{author.Mention} this channel is for the duelist selection.");
            var mentionChannel = await mentionTransaction.Start($"{mention.Username}-duel-selection", timeout, Context.Guild, $"{mention.Mention} this channel is for the duelist selection.");

            var duelProperties = new CardProperty[] { CardProperty.Strength, CardProperty.Endurance, CardProperty.Power, CardProperty.Balance, CardProperty.Teamwork };

            Task userDeckSelection = Task.Run(async () =>
            {
                // post deck of user A in channel A
                await authorDeck.PostReactiveDeck(author, authorCards);

                if (!await authorDeck.WaitUntilCompletion())
                {
                    return;
                }

                await authorChannel.SendMessageAsync($"Thank you for selecting, head to {duelChannel.Mention} and wait for the duel to start.");
            });

            await Task.Delay(TimeSpan.FromSeconds(5));

            Task mentionDeckSelection = Task.Run(async () =>
            {
                // post deck of user B in channel B
                await mentionDeck.PostReactiveDeck(mention, mentionCards);

                if (!await mentionDeck.WaitUntilCompletion())
                {
                    return;
                }

                await mentionChannel.SendMessageAsync($"Thank you for selecting, head to {duelChannel.Mention} and wait for the duel to start.");
            });

            // wait until both transactions are completed and both decks have a selected card and both effort choosers have cards
            await duelChannel.SendMessageAsync("Waiting for duelists to select a competitor.");

            Task.WaitAll(new Task[]
            {
                userDeckSelection, mentionDeckSelection
            });

            // if either fails cancel
            if (authorExt.SelectedCards.Count == 0)
            {
                await duelChannel.SendMessageAsync($"{author.Mention} did not select a participant in time! The duel is cancelled.");
                await Task.Delay(TimeSpan.FromSeconds(15));
                baseTransaction.Cancel();
                return;
            }

            if (mentionExt.SelectedCards.Count == 0)
            {
                await duelChannel.SendMessageAsync($"{mention.Mention} did not select a participant in time! The duel is cancelled.");
                await Task.Delay(TimeSpan.FromSeconds(15));
                baseTransaction.Cancel();
                return;
            }

            var authorCard = authorExt.SelectedCards.First();
            var mentionCard = mentionExt.SelectedCards.First();

            await duelChannel.SendMessageAsync($"**{author.Username}** has chosen **{authorCard}**!", embed: authorCard.GetEmbed());
            await duelChannel.SendMessageAsync($"**{mention.Username}** has chosen **{mentionCard}**!", embed: mentionCard.GetEmbed());

            await duelChannel.SendMessageAsync($"The duel between {author.Mention} and {mention.Mention} begins in 15 seconds!");

            await Task.Delay(TimeSpan.FromSeconds(15));

            baseTransaction.LastInteraction = DateTime.Now;

            List<Duel> duels = new List<Duel>();

            // run duel for each stat
            EmbedBuilder embedBuild = new EmbedBuilder();

            embedBuild.AddField(new EmbedFieldBuilder().WithName("Participants").WithValue(author.Username + ": " + authorCard.NameRaw() + Environment.NewLine + mention.Username + ": " + mentionCard.NameRaw()));

            int categoryCount = 0;

            await authorChannel.SendMessageAsync("Your selected competitor", embed: authorExt.SelectedCards.First().GetEmbed());
            await authorEffort.PostInitialEffortMessage(authorChannel);
            await mentionChannel.SendMessageAsync("Your selected competitor", embed: mentionExt.SelectedCards.First().GetEmbed());
            await mentionEffort.PostInitialEffortMessage(mentionChannel);

            bool continueAuthor = false;
            bool continueMention = false;

            foreach (var stat in duelProperties.OrderBy(r => Guid.NewGuid()))
            {
                await duelChannel.SendMessageAsync($"> *We are now waiting for the coaches to talk to their marbles before the next challenge.*");

                Task.WaitAll(new Task[]
                {
                    Task.Run(async () => {
                        await authorEffort.PostConfirmationMessage(stat, authorCard.GetTotalProperty(stat), mentionCard.GetTotalProperty(stat));
                        continueAuthor = await authorEffort.WaitUntilCompletion();
                    }),
                    Task.Run(async () => {
                        await mentionEffort.PostConfirmationMessage(stat, mentionCard.GetTotalProperty(stat), authorCard.GetTotalProperty(stat));
                        continueMention = await mentionEffort.WaitUntilCompletion();
                    })
                });

                if (!continueAuthor)
                {
                    await duelChannel.SendMessageAsync($"**{author.Username} has forfeited the match!**");
                    break;
                }
                else if (!continueMention)
                {
                    await duelChannel.SendMessageAsync($"**{mention.Username} has forfeited the match!**");
                    break;
                }

                Duel d = new Duel(authorCard, authorEffort.SelectedEffort(),
                    mentionCard, mentionEffort.SelectedEffort(),
                    stat, duelChannel);
                duels.Add(d);

                await CommandHandler.Log(new LogMessage(LogSeverity.Info, "Duel begin", $"{stat} - U1E: {authorEffort.SelectedEffort()}, U2E: {mentionEffort.SelectedEffort()}"));

                await d.Execute(categoryCount++);

                embedBuild.AddField(new EmbedFieldBuilder().WithName($"Category {stat} winner").WithValue(d.Winner.NameRaw()));
                await duelChannel.SendMessageAsync($"Standings" +
                    Environment.NewLine + "> **" + authorCard.BaseCard.Name + "**: " + duels.Count(c => c.Winner == authorCard) +
                    " - **" + mentionCard.BaseCard.Name + "**: " + duels.Count(c => c.Winner == mentionCard), embed: d.GetResultEmbed());
                baseTransaction.LastInteraction = DateTime.Now;
                await Task.Delay(TimeSpan.FromSeconds(10));
            }

            embedBuild.Title = "Summary";

            await duelChannel.SendMessageAsync(embed: embedBuild.Build());

            await Task.Delay(TimeSpan.FromSeconds(5));

            UserCard overallWinner = null;
            if (!continueAuthor)
            {
                overallWinner = mentionCard;
            }
            else if (!continueMention)
            {
                overallWinner = authorCard;
            }
            else
            {
                overallWinner = duels.GroupBy(c => c.Winner).OrderByDescending(c => c.Count()).FirstOrDefault().Key;
            }
            SocketUser userWinner = authorCards.Contains(overallWinner) ? author : mention;

            await duelChannel.SendMessageAsync($"And the winner of this duel is **{userWinner.Mention}**!", embed: overallWinner.GetEmbed());

            embedBuild.AddField(new EmbedFieldBuilder().WithName("OVERALL WINNER").WithValue($"{userWinner.Username} with {overallWinner}!"));

            embedBuild.Title = "Duel summary";

            embedBuild.ImageUrl = overallWinner.GetEmbedImageUrl().ToString();

            await Context.Channel.SendMessageAsync($"The duel between {author.Username} and {mention.Username} has concluded!", embed: embedBuild.Build());

            authorTransaction.Cancel(5);
            mentionTransaction.Cancel(5);

            baseTransaction.Cancel(60);
        }

        [Command("optin")]
        [Summary("Lets you opt in from earning cards.")]
        [RequireBotChatterChannel]
        public async Task OptIn()
        {
            using (var db = new DatabaseContext())
            {
                var userid = Context.Message.Author.Id;
                db.Users.Single(u => u.DiscordId == userid).IsOptedOut = false;
                await db.SaveChangesAsync();
            }

            Handler.UpdateUsers();

            await Context.Channel.SendMessageAsync($"{Context.Message.Author.Mention} you have opted in to the cards game.");
        }

        [Command("optout")]
        [Summary("Lets you opt out from earning cards.")]
        [RequireBotChatterChannel]
        public async Task OptOut()
        {
            using (var db = new DatabaseContext())
            {
                var userid = Context.Message.Author.Id;
                db.Users.Single(u => u.DiscordId == userid).IsOptedOut = true;
                await db.SaveChangesAsync();
            }

            Handler.UpdateUsers();

            await Context.Channel.SendMessageAsync($"{Context.Message.Author.Mention} you have opted out from the cards game.");
        }

        [Command("search team")]
        public async Task Search(Team team)
        {
            List<User> allUsers;
            List<Card> teamCards;
            using (var db = new DatabaseContext())
            {
                teamCards = db.Cards.Include(d => d.Team).Where(d => d.Team == team).ToList();
                allUsers = db.Users.Include(u => u.OwnedCards).ThenInclude(u => u.BaseCard).ThenInclude(u => u.Team).Where(u => u.OwnedCards.Any(c => teamCards.Contains(c.BaseCard))).ToList();
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Users owning cards from {team}: ");
            foreach (var card in teamCards.OrderByDescending(c => c.Rarity).ThenBy(c => c.Name))
            {
                sb.Append($"{card} (Remaining: {card.Quantity}): ");
                var users = allUsers.Where(u => u.OwnedCards.Any(u => u.BaseCard == card)).ToList();
                if (users.Count == 0)
                {
                    sb.Append("*Nobody*");
                }
                else
                {
                    var cards = users.SelectMany(c => c.OwnedCards.Where(c => c.BaseCard.Id == card.Id)).OrderByDescending(u => u.Value);
                    List<string> userwithcards = new List<string>();
                    foreach (var usercards in cards.GroupBy(c => c.Owner).OrderByDescending(u => u.Key.OwnedCards.Where(o => o.BaseCard == card).Sum(o => o.Value)))
                    {
                        var username = usercards.Key.DiscordId.ToString();
                        try
                        {
                            username = Context.Guild.GetUser(usercards.Key.DiscordId).Username;
                        }
                        catch { }
                        userwithcards.Add(username + " (" + string.Join(", ", usercards.Select(u => "Lvl " + u.UpgradeCount)) + ")");
                    }
                    sb.Append(string.Join(", ", userwithcards));
                }
                sb.AppendLine();
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("search card")]
        public async Task Search(Card card)
        {
            List<User> users;
            using (var db = new DatabaseContext())
            {
                users = db.Users.Include(u => u.OwnedCards).ThenInclude(u => u.BaseCard).ThenInclude(u => u.Team).Include(u => u.OwnedCards).ThenInclude(u => u.Owner).Where(u => u.OwnedCards.Any(c => c.BaseCard == card)).ToList();
            }

            StringBuilder sb = new StringBuilder();
            sb.Append($"Users owning {card} (Remaining: {card.Quantity}): ");
            if (users.Count == 0)
            {
                sb.Append("Nobody");
            }
            else
            {
                var cards = users.SelectMany(c => c.OwnedCards.Where(c => c.BaseCard.Id == card.Id)).OrderByDescending(u => u.Value);
                List<string> userwithcards = new List<string>();
                foreach (var usercards in cards.GroupBy(c => c.Owner).OrderByDescending(u => u.Key.OwnedCards.Where(o => o.BaseCard == card).Sum(o => o.Value)))
                {
                    var username = usercards.Key.DiscordId.ToString();
                    try
                    {
                        username = Context.Guild.GetUser(usercards.Key.DiscordId).Username;
                    }
                    catch { }

                    userwithcards.Add(username + " (" + string.Join(", ", usercards.Select(u => "Lvl " + u.UpgradeCount)) + ")");
                }
                sb.Append(string.Join(", ", userwithcards));
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("deck")]
        [Summary("Shows you your owned cards")]
        [RequireBotChatterChannel]
        [RequireUserCards]
        public async Task ShowDeck(SocketUser mention = null)
        {
            // init and get user
            List<UserCard> ownedCards;
            ulong userId = Context.Message.Author.Id;
            if (mention != null) userId = mention.Id;
            double timeout = 2;
            using (var db = new DatabaseContext())
            {
                ownedCards = db.GetCardsForUser(userId);
                timeout = db.Settings.GetDoubleValue(Setting.InteractiveChannelTimeout);
            }

            if (ownedCards.Count == 0)
            {
                await Context.Channel.SendMessageAsync($"{Context.Message.Author} you own no cards.");
                return;
            }

            UserTransaction transaction = new UserTransaction(Handler, Context.Client.CurrentUser, Context.Channel as ITextChannel, false, false, Context.Message.Author);

            // post new upgrade deck to select a card to upgrade
            DeckReactable deck = new DeckReactable(Context.Message.Author, 5, transaction, Handler);

            ExpandCardsDeckAddon cardExpand = new ExpandCardsDeckAddon(Context.Client.CurrentUser);
            deck.AddCardDeckReactionExtension(cardExpand);
            deck.AddFooterMessageExtension(cardExpand);

            if (Context.Message.Author.Id == userId)
            {
                ShareCardsDeckAddon cardShare = new ShareCardsDeckAddon(Context.Client.CurrentUser, Context.Channel);
                deck.AddCardDeckReactionExtension(cardShare);
                deck.AddFooterMessageExtension(cardShare);

                ShareDeckDeckAddon deckShare = new ShareDeckDeckAddon(Context.Client.CurrentUser, Context.Message.Author, ownedCards
                    .OrderByDescending(c => c.BaseCard.Type).ThenByDescending(c => c.Value).ThenByDescending(c => c.BaseCard.Rarity).ThenBy(c => c.Team.Name).ThenBy(c => c.Name).ToList());
                deck.AddFooterReactionAddon(deckShare);
            }

            ConfirmAndCloseDeckAddon confirmAndCloseExt = new ConfirmAndCloseDeckAddon(Context.Client.CurrentUser);
            deck.AddFooterReactionAddon(confirmAndCloseExt);

            var specificBotChannel = await transaction.Start(Context.User.Username + ((mention != null) ? "-" + mention.Username : "") + "-deck", timeout, Context.Guild, $"{Context.User.Mention} this channel is for your deck.");

            await deck.PostReactiveDeck(mention, ownedCards.ToList());

            await deck.WaitUntilCompletion();

            transaction.Cancel();
        }

        [Command("top10")]
        public async Task Top10()
        {
            if (DateTime.Now.Subtract(Top10LastUsed) < TimeSpan.FromHours(1))
            {
                await Context.Channel.SendMessageAsync("Command is on cooldown for another " + (int)(TimeSpan.FromHours(1) - DateTime.Now.Subtract(Top10LastUsed)).TotalMinutes + " minutes. Keep your cool man.");
                return;
            }

            Top10LastUsed = DateTime.Now;

            List<User> users;
            using (var db = new DatabaseContext())
            {
                users = db.Users.Include(u => u.OwnedCards).ThenInclude(u => u.BaseCard).ToList();
            }

            users = users.OrderByDescending(u => u.OwnedCards.Sum(c => c.Value + c.InvestedValue)).Take(10).ToList();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Top 10 Users by Value");
            for (int i = 0; i < users.Count; i++)
            {
                var duser = Context.Guild.GetUser(users[i].DiscordId);

                sb.AppendLine($"#{i + 1} - {duser.Username} - {users[i].OwnedCards.Count} cards - {users[i].OwnedCards.Sum(e => e.Value + e.InvestedValue)} total P");
            }

            await Context.Channel.SendMessageAsync(sb.ToString());
        }

        [Command("trade")]
        [RequireBotChatterChannel]
        [RequireUserInNoTransaction]
        [RequireMentionedUserInNoTransaction]
        [RequireMentionedUserToBeDifferent]
        [RequireUserCards]
        [RequireMentionedUserCards]
        public async Task Trade(SocketUser mentionedUser)
        {
            // init and get user
            ulong userId = Context.Message.Author.Id;

            List<UserCard> userCards;
            List<UserCard> otherUserCards;
            double timeout = 2;
            int deckLimit = 30;

            using (var db = new DatabaseContext())
            {
                userCards = db.GetCardsForUser(userId).ToList();
                otherUserCards = db.GetCardsForUser(mentionedUser.Id).ToList();
                timeout = db.Settings.GetDoubleValue(Setting.InteractiveChannelTimeout);
                deckLimit = db.Settings.GetIntValue(Setting.UserInventoryLimit);
            }

            Handler.UsersBlockedFromTransactions.Add(userId);
            Handler.UsersBlockedFromTransactions.Add(mentionedUser.Id);

            UserTransaction transaction = new UserTransaction(Handler, Context.Client.CurrentUser, Context.Channel as ITextChannel, true, false, Context.Message.Author, mentionedUser);

            // post new upgrade deck to select a card to upgrade
            DeckReactable deck = new DeckReactable(Context.Message.Author, 5, transaction, Handler);
            UserConfirmReactable confirmMessage = new UserConfirmReactable(mentionedUser, transaction, Handler);

            ExpandCardsDeckAddon cardExpand = new ExpandCardsDeckAddon(Context.Client.CurrentUser);
            deck.AddCardDeckReactionExtension(cardExpand);
            deck.AddFooterMessageExtension(cardExpand);

            SelectCardsDeckAddon cardSelect = new SelectCardsDeckAddon(Context.Client.CurrentUser, 0, otherUserCards.Count);
            deck.AddCardDeckReactionExtension(cardSelect);
            deck.AddFooterReactionAddon(cardSelect);
            deck.AddHeaderMessageExtension(cardSelect);

            transaction.OnFinished = new Action(() =>
            {
                Handler.Reactables.RemoveAll(d => d == deck);
                Handler.Reactables.RemoveAll(d => d == confirmMessage);
            });

            var newChannel = await transaction.Start(Context.User.Username + "-" + mentionedUser.Username + "-trade", timeout, Context.Guild, $"{Context.User.Mention} this channel is for the trade with {mentionedUser.Mention}.");

            // confirm the other user wants to trade at all
            await confirmMessage.PostConfirmationMessage($"{mentionedUser.Mention} do you want to trade with {Context.Message.Author.Mention}?");

            if (!await confirmMessage.WaitUntilCompletion())
            {
                return;
            }

            var hintMsg = await newChannel.SendMessageAsync($"{Context.Message.Author.Mention} select the cards you want from {mentionedUser.Username}.");

            // post deck of other user
            await deck.PostReactiveDeck(mentionedUser, otherUserCards);

            if (!await deck.WaitUntilCompletion())
            {
                return;
            }

            await hintMsg.DeleteAsync();

            List<UserCard> wantInTrade = cardSelect.Result;
            cardSelect.ClearResult();

            var wantedCards = string.Join(Environment.NewLine, wantInTrade);
            if (string.IsNullOrEmpty(wantedCards)) wantedCards = "Nothing";

            EmbedBuilder wantBuilder = new EmbedBuilder();
            wantBuilder.AddField(new EmbedFieldBuilder().WithName($"The cards {Context.Message.Author.Username} wants").WithValue(wantedCards));
            await newChannel.SendMessageAsync(embed: wantBuilder.Build());

            hintMsg = await newChannel.SendMessageAsync($"{Context.Message.Author.Mention} select the cards you want to offer in return to {mentionedUser.Username}.");

            // calculate new min and max selection
            cardSelect.MinSelect = deckLimit - (userCards.Count + wantInTrade.Count) < 0
                ? Math.Abs(deckLimit - (userCards.Count + wantInTrade.Count))
                : 0;
            cardSelect.MaxSelect = Math.Abs(deckLimit - (otherUserCards.Count - wantInTrade.Count)) > (userCards.Count)
                ? userCards.Count
                : Math.Abs(deckLimit - (otherUserCards.Count - wantInTrade.Count));

            // post deck of self
            await deck.PostReactiveDeck(Context.Message.Author, userCards);

            if (!await deck.WaitUntilCompletion())
            {
                return;
            }

            await hintMsg.DeleteAsync();

            List<UserCard> offersInTrade = cardSelect.Result;
            cardSelect.ClearResult();

            var offeredCards = string.Join(Environment.NewLine, offersInTrade);
            if (string.IsNullOrEmpty(offeredCards)) offeredCards = "Nothing";

            EmbedBuilder offerBuilder = new EmbedBuilder();
            offerBuilder.AddField(new EmbedFieldBuilder().WithName($"The cards {Context.Message.Author.Username} offers").WithValue(offeredCards));
            await newChannel.SendMessageAsync(embed: offerBuilder.Build());

            // post confirm message for the other user
            EmbedBuilder embed = new EmbedBuilder();
            EmbedFieldBuilder field = new EmbedFieldBuilder();
            embed.AddField(new EmbedFieldBuilder().WithName($"Offered cards by {Context.Message.Author.Username}").WithValue(offeredCards));
            embed.AddField(new EmbedFieldBuilder().WithName($"Requested cards by {Context.Message.Author.Username}").WithValue(wantedCards));

            await confirmMessage.PostConfirmationMessage($"{mentionedUser.Mention} do you accept this trade suggestion of {Context.Message.Author.Mention}?", embed.Build());

            if (!await confirmMessage.WaitUntilCompletion())
            {
                return;
            }

            // swap cards

            using (var db = new DatabaseContext())
            {
                var mainUser = db.Users.Single(u => u.DiscordId == userId);
                var otherUser = db.Users.Single(u => u.DiscordId == mentionedUser.Id);
                foreach (var card in offersInTrade)
                {
                    card.Owner = otherUser;
                    db.Update(card);
                }
                foreach (var card in wantInTrade)
                {
                    card.Owner = mainUser;
                    db.Update(card);
                }
                await db.SaveChangesAsync();
            }

            Handler.UpdateUsers();

            await Context.Channel.SendMessageAsync(text: $"{Context.User.Username} traded with {mentionedUser.Username}.", embed: embed.Build());

            await Task.Delay(TimeSpan.FromSeconds(5));

            transaction.Cancel();
        }

        [Command("unlock")]
        public async Task Unlock()
        {
            List<ulong> unlockedUsers = new List<ulong>();
            foreach (var reactable in new List<ReactableBase>(Handler.Reactables.Where(p => p.Transaction.Participants.Any(c => c.Id == Context.Message.Author.Id))))
            {
                reactable.Transaction.Cancel();

                foreach (var part in reactable.Transaction.Participants)
                {
                    if (!unlockedUsers.Contains(part.Id))
                    {
                        unlockedUsers.Add(part.Id);
                    }

                    foreach (var reactablepart in new List<ReactableBase>(Handler.Reactables).Where(p => p.Transaction.Participants.Any(c => c.Id == part.Id)))
                    {
                        Handler.Reactables.Remove(reactablepart);

                        reactable.Transaction.Cancel();
                    }
                }
            }

            if (Handler.UsersBlockedFromTransactions.Contains(Context.Message.Author.Id))
            {
                unlockedUsers.Add(Context.Message.Author.Id);
                Handler.UsersBlockedFromTransactions.Remove(Context.Message.Author.Id);
            }

            if (unlockedUsers.Count > 0)
            {
                await Context.Channel.SendMessageAsync($"{string.Join(", ", unlockedUsers.Select(c => Context.Guild.GetUser(c).Username))} was / were unlocked from any ongoing transactions.");
            }
            else
            {
                await Context.Channel.SendMessageAsync($"No blocking transactions found for {Context.Message.Author.Username}");
            }
        }

        [Command("upgrade")]
        [RequireBotChatterChannel]
        [RequireUserInNoTransaction]
        [RequireUserCards(2, CardType.Normal)]
        public async Task Upgrade()
        {
            // init and get user
            List<UserCard> ownedCards;
            ulong userId = Context.Message.Author.Id;

            double timeout = 2;

            using (var db = new DatabaseContext())
            {
                ownedCards = db.GetCardsForUser(userId).Where(c => c.BaseCard.Type == CardType.Normal).ToList();
                timeout = db.Settings.GetDoubleValue(Setting.InteractiveChannelTimeout);
            }

            Handler.UsersBlockedFromTransactions.Add(userId);
            UserTransaction transaction = new UserTransaction(Handler, Context.Client.CurrentUser, Context.Channel as ITextChannel, true, false, Context.Message.Author);

            // post new upgrade deck to select a card to upgrade
            DeckReactable deck = new DeckReactable(Context.Message.Author, 5, transaction, Handler);
            UserUpgradeReactable msg = new UserUpgradeReactable(Context.User, transaction, Handler);

            ExpandCardsDeckAddon cardExpand = new ExpandCardsDeckAddon(Context.Client.CurrentUser);
            deck.AddCardDeckReactionExtension(cardExpand);
            deck.AddFooterMessageExtension(cardExpand);

            SelectCardsDeckAddon upgradeCardExt = new SelectCardsDeckAddon(Context.Client.CurrentUser, 1, 1)
            {
                HeaderMsg = "> ***Select a card to upgrade***"
            };
            deck.AddCardDeckReactionExtension(upgradeCardExt);
            deck.AddFooterReactionAddon(upgradeCardExt);
            deck.AddHeaderMessageExtension(upgradeCardExt);

            ITextChannel newChannel = await transaction.Start(Context.User.Username + "-upgrade", timeout, Context.Guild, $"{Context.User.Mention} this channel is for your card upgrade. Once you finalize the upgrade your upgraded card will be posted to {(Context.Channel as ITextChannel).Mention}");

            await deck.PostReactiveDeck(Context.Message.Author, ownedCards.Where(e => e.UpgradeCount < UserCard.MaxUpgrade).ToList());

            if (!await deck.WaitUntilCompletion())
            {
                return;
            }

            var cardToUpgrade = upgradeCardExt.Result.First();

            await newChannel.SendMessageAsync($"> ***Following card was selected for the upgrade***",
                embed: cardToUpgrade.GetEmbed());

            // post new upgrade deck to select cards to sacrifice
            deck = new DeckReactable(Context.Message.Author, 5, transaction, Handler);
            deck.AddCardDeckReactionExtension(cardExpand);
            deck.AddFooterMessageExtension(cardExpand);

            UpgradeMultiSelectCardsDeckAddon sacrificeCardExt = new UpgradeMultiSelectCardsDeckAddon(Context.Client.CurrentUser, cardToUpgrade);
            deck.AddCardDeckReactionExtension(sacrificeCardExt);
            deck.AddFooterReactionAddon(sacrificeCardExt);
            deck.AddHeaderMessageExtension(sacrificeCardExt);

            await deck.PostReactiveDeck(Context.Message.Author, ownedCards.Except(new List<UserCard>() { cardToUpgrade }).ToList(), true);

            if (!await deck.WaitUntilCompletion())
            {
                return;
            }

            await newChannel.SendMessageAsync($"Following cards will be invested for the upgrade: {Environment.NewLine}- {string.Join(Environment.NewLine + "- ", sacrificeCardExt.Result)}");

            cardToUpgrade.InvestedValue += sacrificeCardExt.Result.Sum(e => e.Value);

            if (cardToUpgrade.InvestedValue > cardToUpgrade.Value)
            {
                await newChannel.SendMessageAsync("> You invested enough P and your card ranked up! You can now upgrade the card.");
            }

            while (cardToUpgrade.InvestedValue >= cardToUpgrade.Value)
            {
                await msg.PostConfirmationMessage(cardToUpgrade);

                if (!await msg.WaitUntilCompletion())
                {
                    return;
                }

                // handle upgrading the card
                var prop = msg.UpgradeToCardProperty();

                foreach (var property in prop)
                {
                    var oldValue = (int)property.Key.GetValue(cardToUpgrade);
                    var newValue = oldValue + 1;

                    await newChannel.SendMessageAsync($"> Upgraded {property.Key.Name} from {oldValue} to {newValue}");

                    property.Value.SetValue(cardToUpgrade, (int)property.Value.GetValue(cardToUpgrade) + 1);
                }

                cardToUpgrade.AddUpgrade();
            }

            using (var db = new DatabaseContext())
            {
                db.Attach(cardToUpgrade);
                db.Update(cardToUpgrade);
                db.Users.Include(u => u.OwnedCards).Single(u => u.DiscordId == userId)
                    .OwnedCards.RemoveAll(c => sacrificeCardExt.Result.Contains(c));
                foreach (var card in sacrificeCardExt.Result)
                {
                    card.BaseCard.Quantity++;
                    db.Update(card.BaseCard);
                }
                db.UserCards.RemoveRange(sacrificeCardExt.Result);
                await db.SaveChangesAsync();
            }

            Handler.UpdateUsers();
            Handler.UpdateCards();

            await Context.Channel.SendMessageAsync(text: $"{Context.User.Username} upgraded a card: **" + cardToUpgrade.ToString() + "**", embed: cardToUpgrade.GetEmbed());

            transaction.Cancel(15);
        }
    }
}