﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Preconditions;
using MarbleBot.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Modules
{
    [RequireAdminChannel]
    [RequireBotOwner]
    [Group("cleanup")]
    public class AdminCleanupModule : ModuleBase<SocketCommandContext>
    {
        public Task CleanUpTask;
        public bool ForcedRun;
        private readonly DiscordSocketClient client;

        public AdminCleanupModule(CommandHandler handler, DiscordSocketClient client)
        {
            Handler = handler;
            ForcedRun = false;
            this.client = client;
            client.Ready += Client_Ready;
        }

        public CommandHandler Handler { get; }

        [Command]
        public async Task CleanUp(int cleanUpDays = 30)
        {
            await CleanUp(cleanUpDays, Context.Channel);
        }

        [Command("start")]
        public async Task CleanUp()
        {
            if (CleanUpTask == null)
            {
                CleanUpTask = CleanupInactiveUsersTask(Context.Channel);
            }
            else
            {
                await Context.Channel.SendMessageAsync("Cleanup task already running");
            }
        }

        public async Task CleanUp(int cleanUpDays, IMessageChannel channel)
        {
            await channel.SendMessageAsync("[CleanUp] Cleaning up users that didn't receive a card for the past " + cleanUpDays + " days");

            List<User> cleanUpUsers = new List<User>();
            List<UserCard> cleanedUpCards = new List<UserCard>();
            using (var db = new DatabaseContext())
            {
                cleanUpUsers = (db.Users as IQueryable<User>).ToList().Where(u => (DateTime.Now - u.LastEarnedCard).TotalDays >= cleanUpDays).ToList();
                foreach (var user in cleanUpUsers)
                {
                    var cards = (db.UserCards as IQueryable<UserCard>).Include(u => u.BaseCard).Where(u => u.Owner == user).ToList();
                    cleanedUpCards.AddRange(cards);
                    foreach (var card in cards)
                    {
                        db.Remove(card);
                        card.BaseCard.Quantity++;
                        db.Update(card.BaseCard);
                    }
                }

                await db.SaveChangesAsync();
            }

            if (cleanUpUsers.Count > 0 && cleanedUpCards.Count > 0)
            {
                await channel.SendMessageAsync("[CleanUp] Cleaned up " + cleanUpUsers.Count + " users and freed up " + cleanedUpCards.Count + " cards");
            }
            else
            {
                await channel.SendMessageAsync("[CleanUp] No users found to clean up.");
            }

            Handler.UpdateCards();
            Handler.UpdateUsers();
        }

        [Command("cache")]
        public async Task CleanUpImages()
        {
            string folder = "";
            using (var db = new DatabaseContext())
            {
                folder = db.Settings.GetStringValue(Setting.ImageFolderBase);
            }

            var files = Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                File.Delete(file);
            }

            await Context.Channel.SendMessageAsync($"Deleted {files.Count()} cached image files.");
        }

        public async Task CleanupInactiveUsersTask(IMessageChannel channel)
        {
            bool firstRun = true;
            while (true)
            {
                int cleanUpDays = 0;
                ulong adminChannel = 0;

                if (firstRun)
                {
                    using (var db = new DatabaseContext())
                    {
                        adminChannel = db.Channels.FirstOrDefault(c => c.ChannelType == Utilities.ChannelType.Admininistrative).ChannelId;
                        cleanUpDays = db.Settings.GetIntValue(Setting.DaysBeforeCleanupUser);
                    }
                    await channel.SendMessageAsync("Scheduled cleanup task now running every 12 hours and cleaning up users older than " + cleanUpDays + " days.");
                    firstRun = false;
                }

                await Task.Delay(TimeSpan.FromHours(12));

                using (var db = new DatabaseContext())
                {
                    adminChannel = db.Channels.FirstOrDefault(c => c.ChannelType == Utilities.ChannelType.Admininistrative).ChannelId;
                    cleanUpDays = db.Settings.GetIntValue(Setting.DaysBeforeCleanupUser);
                }

                await CleanUp(cleanUpDays, channel);
            }
        }

        private Task Client_Ready()
        {
            if (CleanUpTask == null)
            {
                _ = Task.Run(async () =>
                {
                    await Task.Delay(TimeSpan.FromSeconds(5));
                    using var db = new DatabaseContext();
                    var channelid = db.Channels.SingleOrDefault(c => c.ChannelType == Utilities.ChannelType.Admininistrative).ChannelId;
                    CleanUpTask = CleanupInactiveUsersTask(client.Guilds.FirstOrDefault().GetTextChannel(channelid));
                });
            }

            return Task.CompletedTask;
        }
    }
}