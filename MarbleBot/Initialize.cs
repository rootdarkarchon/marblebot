﻿using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace MarbleBot
{
    public class Initialize
    {
        private readonly CommandService _commands;
        private readonly DiscordSocketClient _client;

        public Initialize(CommandService commands = null, DiscordSocketClient client = null)
        {
            _commands = commands ?? new CommandService();

            DiscordSocketConfig config = new DiscordSocketConfig
            {
                DefaultRetryMode = Discord.RetryMode.RetryRatelimit,
                ExclusiveBulkDelete = true,
#if DEBUG
                LogLevel = Discord.LogSeverity.Verbose,
#endif
                MessageCacheSize = 3000,
                RateLimitPrecision = Discord.RateLimitPrecision.Millisecond,
            };
            _client = client ?? new DiscordSocketClient(config);
            _client.Disconnected += Client_Disconnected;
        }

        private Task Client_Disconnected(Exception arg)
        {
            _ = Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(15));
                if (_client.ConnectionState == Discord.ConnectionState.Disconnected)
                {
                    await _client.StartAsync();
                }
            });

            return Task.CompletedTask;
        }

        public IServiceProvider BuildServiceProvider() => new ServiceCollection()
            .AddSingleton(_client)
            .AddSingleton(_commands)
            .AddSingleton<CommandHandler>()
            .BuildServiceProvider();
    }
}