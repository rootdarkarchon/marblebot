﻿using DataStructures.RandomSelector;
using Discord;
using Discord.WebSocket;
using MarbleBot.Database;
using MarbleBot.Models;
using MarbleBot.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarbleBot
{
    internal class Program
    {
        private DiscordSocketClient _client;
        private CommandHandler _commandHandler;
        public static bool IS_RUNNING = true;

        private static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            Initialize init = new Initialize();
            List<Setting> settings;

            using (var dataContext = new DatabaseContext())
            {
                settings = dataContext.Settings.ToList();
                foreach (var settingField in
                    typeof(Setting).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.FlattenHierarchy)
                    .Where(f => f.IsLiteral && !f.IsInitOnly))
                {
                    var settingName = (string)settingField.GetRawConstantValue();
                    var defaultValue = (settingField.GetCustomAttributes(typeof(DefaultValueAttribute), false).Single() as DefaultValueAttribute).Value.ToString();
                    if (!settings.Any(s => s.Name == settingName))
                    {
                        Setting set = new Setting { Name = settingName, Value = defaultValue };
                        dataContext.Add(set);
                        settings.Add(set);
                    }
                }

                await dataContext.SaveChangesAsync();
            }

            try
            {
                string folder = "";
                using (var db = new DatabaseContext())
                {
                    folder = db.Settings.GetStringValue(Setting.ImageFolderBase);
                }

                var files = Directory.GetFiles(folder, "*.*", SearchOption.AllDirectories);

                foreach (var file in files)
                {
                    FileInfo fi = new FileInfo(file);
                    if (Math.Abs((DateTime.Now - fi.LastWriteTime).TotalDays) > 1)
                    {
                        File.Delete(file);
                    }
                }
            }
            catch { }

#if DEBUG
            await CreateTestData();
#endif

            string botToken = "";

            using (var db = new DatabaseContext())
            {
                botToken = settings.GetStringValue(Setting.DiscordBotToken);
            }

            if (string.IsNullOrEmpty(botToken))
            {
                throw new Exception($"{Setting.DiscordBotToken} must be set in the Database.");
            }

            init.BuildServiceProvider();

            _client = (init.BuildServiceProvider().GetService(typeof(DiscordSocketClient)) as DiscordSocketClient);
            _commandHandler = (init.BuildServiceProvider().GetService(typeof(CommandHandler)) as CommandHandler);

            _client.Connected += Connected;
            _client.Ready += StartupReady;

            await _commandHandler.InstallCommandsAsync();
            await _client.LoginAsync(TokenType.Bot, botToken);
            await _client.StartAsync();

            while (IS_RUNNING)
            {
                await Task.Delay(1000);
            }

            throw new Exception("Shutting down");
        }

        private async Task Connected()
        {
            List<Channel> channels;
            using (var db = new DatabaseContext())
            {
                channels = (db.Channels as IQueryable<Channel>).Where(c => c.ChannelType == Utilities.ChannelType.Transaction).ToList();
                db.RemoveRange(channels);
                await db.SaveChangesAsync();
            }

            foreach (var channel in channels)
            {
                try
                {
                    var tchannel = _client.Guilds.FirstOrDefault().GetChannel(channel.ChannelId) as ITextChannel;
                    await tchannel.DeleteAsync();
                }
                catch { }
            }
        }

        private Task StartupReady()
        {
            ulong adminid = 0;
            List<Channel> channels = new List<Channel>();
            using (var db = new DatabaseContext())
            {
                adminid = db.Channels.FirstOrDefault(c => c.ChannelType == Utilities.ChannelType.Admininistrative).ChannelId;
            }

            if (adminid == 0) return Task.CompletedTask;

            if (_client.Guilds.Count == 0) return Task.CompletedTask;

            if (!(_client.Guilds.FirstOrDefault().GetChannel(adminid) is ITextChannel adminchannel)) return Task.CompletedTask;

            _ = adminchannel.SendMessageAsync("[Startup complete] Reactor online. Sensors online. Weapons online. All systems nominal.");
            _ = SetStatus();

            return Task.CompletedTask;
        }

        private async Task SetStatus()
        {
            while (true)
            {
                List<Card> Cards;
                using (var db = new DatabaseContext())
                {
                    Cards = db.Cards.ToList();
                }

                await _client.SetGameAsync($"over {Cards.Sum(c => c.Quantity)} cards", type: ActivityType.Watching);

                CardRarity[] rarities = new CardRarity[] { CardRarity.Common, CardRarity.Uncommon, CardRarity.Rare, CardRarity.Epic, CardRarity.Undefined };
                float[] rarityWeights = new float[]
                {
                    Cards.Where(c=>c.Rarity == CardRarity.Common).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Uncommon).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Rare).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Epic).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Undefined).Sum(c=>c.Quantity),
                };

                DynamicRandomSelector<CardRarity> randomSelector = new DynamicRandomSelector<CardRarity>(rarities, rarityWeights);

                StringBuilder sb = new StringBuilder();
                int counter = 0;
                for (int i = 0; i < rarities.Length; i++)
                {
                    if (rarityWeights[i] > 0)
                    {
                        float prevCDL = 0;
                        if (i != 0)
                        {
                            prevCDL = randomSelector.CDL[counter - 1];
                        }
                        sb.Append($"{rarities[i]}: (Available: {rarityWeights[i]}) {(randomSelector.CDL[counter++] - prevCDL).ToString("P4")}, ");
                    }
                }

                await CommandHandler.Log(new LogMessage(LogSeverity.Info, "Earning",
                    $"Current chances: " + sb.ToString()));

                await Task.Delay(TimeSpan.FromMinutes(5));
            }
        }

        private static async Task CreateTestData()
        {
            using var dataContext = new DatabaseContext();
            CreateBaseTemplates(dataContext);

            await dataContext.SaveChangesAsync();

            CreateTeams(dataContext);

            await dataContext.SaveChangesAsync();

            CreateTestCards(dataContext);

            await dataContext.SaveChangesAsync();

            Channel earnchan = new Channel { ChannelId = 661496657245896714, ChannelType = Utilities.ChannelType.Earning };

            Channel botchan = new Channel { ChannelType = Utilities.ChannelType.BotChatter, ChannelId = 661496613994233858 };

            Channel adminchan = new Channel { ChannelId = 661496635355824173, ChannelType = Utilities.ChannelType.Admininistrative };

            dataContext.Channels.Add(earnchan);
            dataContext.Channels.Add(botchan);
            dataContext.Channels.Add(adminchan);

            await dataContext.SaveChangesAsync();
        }

        private static void CreateBaseTemplates(DatabaseContext context)
        {
            Template t = new Template { Name = "Normal Card", ImagePath = @"BaseTemplate.png" };

            TemplateField strengthField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310,
                Y = 493 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                Text = "{Strength}"
            };
            t.TemplateFields.Add(strengthField);

            TemplateField enduranceField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310,
                Y = 529 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                Text = "{Endurance}"
            };
            t.TemplateFields.Add(enduranceField);

            TemplateField powerField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310,
                Y = 565 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                Text = "{Power}"
            };
            t.TemplateFields.Add(powerField);

            TemplateField statBalanceField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310,
                Y = 601 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                Text = "{Balance}"
            };
            t.TemplateFields.Add(statBalanceField);

            TemplateField teamworkField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310,
                Y = 637 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                Text = "{Teamwork}"
            };
            t.TemplateFields.Add(teamworkField);

            TemplateField upgradedstrengthField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310 + 30,
                Y = 493 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                DisplayWhenZero = false,
                Text = "+{UpgradedStrength}"
            };
            t.TemplateFields.Add(upgradedstrengthField);

            TemplateField upgradedenduranceField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310 + 30,
                Y = 529 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                DisplayWhenZero = false,
                Text = "+{UpgradedEndurance}"
            };
            t.TemplateFields.Add(upgradedenduranceField);

            TemplateField upgradedpowerField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310 + 30,
                Y = 565 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                DisplayWhenZero = false,
                Text = "+{UpgradedPower}"
            };
            t.TemplateFields.Add(upgradedpowerField);

            TemplateField upgradedstatBalanceField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310 + 30,
                Y = 601 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                DisplayWhenZero = false,
                Text = "+{UpgradedBalance}"
            };
            t.TemplateFields.Add(upgradedstatBalanceField);

            TemplateField upgradedteamworkField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 310 + 30,
                Y = 637 - 22,
                FontName = "Montserrat Medium",
                FontSize = 24,
                DisplayWhenZero = false,
                Text = "+{UpgradedTeamwork}"
            };
            t.TemplateFields.Add(upgradedteamworkField);

            TemplateField nameField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF"),
                X = 240,
                Y = 110,
                FontName = "Montserrat",
                FontSize = 30,
                Alignment = System.Drawing.StringAlignment.Center,
                LineAlignment = System.Drawing.StringAlignment.Center,
                FontStyle = System.Drawing.FontStyle.Bold,
                Text = "{Name}"
            };
            t.TemplateFields.Add(nameField);

            TemplateField teamField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF"),
                X = 306,
                Y = 693,
                FontName = "Montserrat",
                FontSize = 24,
                Alignment = System.Drawing.StringAlignment.Center,
                FontStyle = System.Drawing.FontStyle.Bold,
                Text = "{Team}"
            };
            t.TemplateFields.Add(teamField);

            TemplateField valueField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF"),
                X = 42,
                Y = 720,
                FontName = "Montserrat",
                FontSize = 34,
                Width = 80,
                FontStyle = System.Drawing.FontStyle.Bold,
                LineAlignment = System.Drawing.StringAlignment.Near,
                Text = "{Value}P"
            };
            t.TemplateFields.Add(valueField);

            TemplateField investedvalueField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF"),
                X = 42,
                Y = 700,
                FontName = "Montserrat",
                FontSize = 18,
                Width = 50,
                DisplayWhenZero = false,
                FontStyle = System.Drawing.FontStyle.Bold,
                Text = "(+{InvestedValue}P)"
            };
            t.TemplateFields.Add(investedvalueField);

            TemplateField imageField = new TemplateField
            {
                AssociatedTemplate = t,
                X = 34,
                Y = 141,
                Width = 413,
                Height = 281,
                Text = "[ImagePath]"
            };
            t.TemplateFields.Add(imageField);

            TemplateField teamImageField = new TemplateField
            {
                AssociatedTemplate = t,
                X = 132,
                Y = 685,
                Width = 43,
                Height = 43,
                ScaleMode = ScaleMode.FitToHeight,
                Text = "[Team.ImagePath]"
            };
            t.TemplateFields.Add(teamImageField);

            context.Add(t);

            Template l = new Template { Name = "Legendary Card", ImagePath = @"LegendaryTemplate.png" };

            TemplateField legendaryIcon = new TemplateField
            {
                AssociatedTemplate = t,
                X = 132,
                Y = 685,
                Width = 43,
                Height = 43,
                ScaleMode = ScaleMode.FitToHeight,
                Text = @"[Images\Teams\Legendary.png]"
            };
            l.TemplateFields.Add(legendaryIcon);

            TemplateField legendaryField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF"),
                X = 306,
                Y = 693,
                FontName = "Montserrat",
                FontSize = 24,
                Alignment = System.Drawing.StringAlignment.Center,
                LineAlignment = System.Drawing.StringAlignment.Near,
                FontStyle = System.Drawing.FontStyle.Bold,
                Text = "L E G E N D A R Y"
            };
            l.TemplateFields.Add(legendaryField);

            TemplateField legendaryimageField = new TemplateField
            {
                AssociatedTemplate = t,
                X = 34,
                Y = 142,
                Width = 413,
                Height = 354,
                Text = @"[ImagePath]"
            };
            l.TemplateFields.Add(legendaryimageField);

            TemplateField legendarynameField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = Card.GetColor(CardType.Legendary, CardRarity.Undefined),
                X = 240,
                Y = 110,
                FontName = "Montserrat",
                FontSize = 30,
                Alignment = System.Drawing.StringAlignment.Center,
                LineAlignment = System.Drawing.StringAlignment.Center,
                FontStyle = System.Drawing.FontStyle.Bold,
                Text = "{Name}"
            };
            l.TemplateFields.Add(legendarynameField);

            TemplateField legendarydescriptionField = new TemplateField
            {
                AssociatedTemplate = t,
                DrawingColor = System.Drawing.ColorTranslator.FromHtml("#211f1f"),
                X = 240 - 190,
                Y = 590,
                FontName = "Montserrat Medium",
                FontSize = 24,
                Width = 380,
                Height = 150,
                Alignment = System.Drawing.StringAlignment.Center,
                LineAlignment = System.Drawing.StringAlignment.Center,
                FontStyle = System.Drawing.FontStyle.Italic,
                Text = "{Description}"
            };
            l.TemplateFields.Add(legendarydescriptionField);

            context.Add(l);
        }

        private static void CreateTeams(DatabaseContext context)
        {
            foreach (var file in Directory.GetFiles(Path.Combine("Images", "Teams")))
            {
                FileInfo f = new FileInfo(file);
                Team t = new Team
                {
                    Name = Path.GetFileNameWithoutExtension(file).Replace('_', ' '),
                    ImagePath = Path.GetFileName(file)
                };
                context.Add(t);
            }
        }

        private static void CreateTestCards(DatabaseContext dataContext)
        {
            var lines = File.ReadAllLines("marbles.csv");
            System.Random random = new Random();
            var teams = dataContext.Teams.ToList();
            foreach (var line in lines)
            {
                var split = line.Split('\t');
                var imagePath = split[1].Replace(' ', '_') + ".jpg";
                if (!File.Exists(Path.Combine("Images", "Cards", imagePath))) imagePath = "NOT FOUND";
                Card c = new Card
                {
                    Name = split[1],
                    Team = teams.First(t => t.Name.ToLower() == split[0].ToLower()),
                    Endurance = int.Parse(split[6]),
                    Rarity = (CardRarity)int.Parse(split[3]),
                    Power = int.Parse(split[5]),
                    Balance = int.Parse(split[7]),
                    Strength = int.Parse(split[4]),
                    Teamwork = int.Parse(split[8]),
                    Value = Card.GetCardValue((CardRarity)int.Parse(split[3])),
                    Quantity = Card.GetCardQuantity((CardRarity)int.Parse(split[3])),
                    Type = (CardType)int.Parse(split[2]),
                    Template = dataContext.Templates.ToList()[int.Parse(split[2])],
                    Description = split[9],
                    ImagePath = imagePath
                };
                dataContext.Cards.Add(c);
            }
        }
    }
}