﻿using DataStructures.RandomSelector;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using MarbleBot.Database;
using MarbleBot.Interfaces;
using MarbleBot.Models;
using MarbleBot.TypeReaders;
using MarbleBot.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MarbleBot
{
    public class CommandHandler
    {
        public readonly DiscordSocketClient Client;
        public readonly List<ReactableBase> Reactables;
        public readonly List<ulong> UsersBlockedFromTransactions;
        public ulong BotChannelCategoryId;
        private readonly CommandService _commands;
        private readonly IServiceProvider _services;
        private List<Card> Cards;
        private List<Channel> Channels;
        private List<Setting> Settings;
        private List<User> Users;

        public CommandHandler(IServiceProvider services, DiscordSocketClient client, CommandService commands)
        {
            _services = services;
            _commands = commands;
            Client = client;
            Reactables = new List<ReactableBase>();
            UsersBlockedFromTransactions = new List<ulong>();
            using (var db = new DatabaseContext())
            {
                Cards = db.Cards.Include(c => c.Template).Include(c => c.Template.TemplateFields).Include(c => c.Team).ToList();
                Channels = db.Channels.ToList();
                Users = db.Users.Include(u => u.OwnedCards).ToList();
                Settings = db.Settings.ToList();
            }

            client.Connected += Connected;

            BotChannelCategoryId = Settings.GetULongValue(Setting.BotChannelsCategoryId);
        }

        public static Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        public Task Connected()
        {
            using (var db = new DatabaseContext())
            {
                Cards = db.Cards.Include(c => c.Template).Include(c => c.Template.TemplateFields).Include(c => c.Team).ToList();
                Channels = db.Channels.ToList();
                Users = db.Users.Include(u => u.OwnedCards).ToList();
                Settings = db.Settings.ToList();
            }

            UsersBlockedFromTransactions.Clear();
            Reactables.RemoveAll(e => e != null);

            return Task.CompletedTask;
        }

        public async Task InstallCommandsAsync()
        {
            // Hook the MessageReceived event into our command handler
            Client.MessageReceived += HandleCommandAsync;

            Client.ReactionAdded += HandleReactionAsync;

            Client.ReactionRemoved += HandleRemoveReactionAsync;

            Client.Log += Log;

            _commands.AddTypeReader<User>(new DbUserTypeReader());
            _commands.AddTypeReader<Card>(new DbCardTypeReader());
            _commands.AddTypeReader<UserCard>(new DbUserCardTypeReader());
            _commands.AddTypeReader<Setting>(new DbSettingTypeReader());
            _commands.AddTypeReader<Team>(new DbTeamTypeReader());
            _commands.AddTypeReader<Template>(new DbTemplateTypeReader());
            _commands.AddTypeReader<TemplateField>(new DbTemplateFieldTypeReader());
            _commands.AddTypeReader<UpgradeProperty[]>(new UpgradePropertiesArrayTypeReader());

            await _commands.AddModulesAsync(assembly: Assembly.GetEntryAssembly(),
                                            services: _services);
        }

        public void UpdateCards()
        {
            using var db = new DatabaseContext();
            Cards = db.Cards.Include(c => c.Template).Include(c => c.Template.TemplateFields).Include(c => c.Team).ToList();
        }

        public void UpdateSettings()
        {
            using var db = new DatabaseContext();
            Settings = db.Settings.ToList();
        }

        public void UpdateUsers()
        {
            using var db = new DatabaseContext();
            Users = db.Users.Include(u => u.OwnedCards).ToList();
        }

        internal void UpdateChannels()
        {
            using var db = new DatabaseContext();
            Channels = db.Channels.ToList();
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a system message
            if (!(messageParam is SocketUserMessage message)) return;
            if (message.Author.IsBot) return;

            var user = Users.SingleOrDefault(u => u.DiscordId == messageParam.Author.Id);

            if (user == null)
            {
                user = new User
                {
                    DiscordId = messageParam.Author.Id,
                    OwnedCards = new List<UserCard>()
                };
                using (var db = new DatabaseContext())
                {
                    db.Users.Add(user);
                    await db.SaveChangesAsync();
                }

                Users.Add(user);
            }

            if (user.IsBanned) return;

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;

            if (!(message.HasCharPrefix('!', ref argPos)) &&
                Channels.Where(e => e.ChannelType == Utilities.ChannelType.Earning)
                .Any(c => c.ChannelId == message.Channel.Id))
            {
                _ = Task.Run(async () =>
                {
                    if (UsersBlockedFromTransactions.Contains(messageParam.Author.Id)) return;
                    UsersBlockedFromTransactions.Add(messageParam.Author.Id);
                    bool hasEarned = await HandleEarning(message, user);
                    if (hasEarned)
                    {
                        using var db = new DatabaseContext();
                        user.LastEarnedCard = DateTime.Now;
                        db.Update(user);
                        await db.SaveChangesAsync();
                    }
                    UsersBlockedFromTransactions.Remove(messageParam.Author.Id);
                });
                return;
            }

            // Determine if the message is a command based on the prefix and make sure no bots trigger commands
            if (!(message.HasCharPrefix('!', ref argPos) ||
                message.HasMentionPrefix(Client.CurrentUser, ref argPos)) ||
                message.Author.IsBot)
                return;

            if (!Channels.Where(c => c.ChannelType == Utilities.ChannelType.Admininistrative || c.ChannelType == Utilities.ChannelType.BotChatter).Any(c => c.ChannelId == message.Channel.Id))
            {
                return;
            }

            var context = new SocketCommandContext(Client, message);

            _ = Task.Run(async () =>
            {
                var result = await _commands.ExecuteAsync(
                    context: context,
                    argPos: argPos,
                    services: _services);

                if (!result.IsSuccess)
                    await context.Channel.SendMessageAsync(result.ErrorReason);
            });
        }

        private async Task<bool> HandleEarning(SocketUserMessage message, User user)
        {
            System.Random random = new Random(DateTime.Now.Millisecond);
            if (user.IsOptedOut) return true;
            int cardRoll = random.Next(0, 100);
            int baseFindChance = Settings.GetIntValue(Setting.BaseFindChance);
            bool timedOut = Math.Abs(DateTime.Now.Subtract(user.LastEarnedCard).TotalMinutes) < Settings.GetIntValue(Setting.UserTimeoutMinutes);
            await Log(new LogMessage(LogSeverity.Info, "Earning", user.DiscordId + " chance " + cardRoll + ", required " + baseFindChance + ", timed out " + timedOut));
            // handle earning
            if (!timedOut && cardRoll < baseFindChance && user.OwnedCards.Count < Settings.GetIntValue(Setting.UserInventoryLimit))
            {
                CardRarity[] rarities = new CardRarity[] { CardRarity.Common, CardRarity.Uncommon, CardRarity.Rare, CardRarity.Epic, CardRarity.Undefined };
                float[] rarityWeights = new float[]
                {
                    Cards.Where(c=>c.Rarity == CardRarity.Common).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Uncommon).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Rare).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Epic).Sum(c=>c.Quantity),
                    Cards.Where(c=>c.Rarity == CardRarity.Undefined).Sum(c=>c.Quantity),
                };

                DynamicRandomSelector<CardRarity> randomSelector = new DynamicRandomSelector<CardRarity>(rarities, rarityWeights);
                var cardRarity = randomSelector.SelectRandomItem();

                await Log(new LogMessage(LogSeverity.Info, "Earning", $"{user.DiscordId} found rarity {cardRarity}"));

                DynamicRandomSelector<Card> randomCardSelector = new DynamicRandomSelector<Card>(Cards.Where(c => c.Rarity == cardRarity).ToList(), Cards.Where(c => c.Rarity == cardRarity).Select(c => (float)c.Quantity).ToList());
                var card = randomCardSelector.SelectRandomItem();

                if (card == null)
                {
                    await Log(new LogMessage(LogSeverity.Info, "Earning", user.DiscordId + " found no available cards"));
                    return true;
                }

                await Log(new LogMessage(LogSeverity.Info, "Earning", user.DiscordId + " found: " + card.Name));

                UserCard userCard = new UserCard(card);

                user.OwnedCards.Add(userCard);

                using (var db = new DatabaseContext())
                {
                    card.Quantity--;
                    db.Update(card);
                    await db.SaveChangesAsync();
                }

                var chatterChanId = Channels.First(c => c.ChannelType == Utilities.ChannelType.BotChatter).ChannelId;
                var channel = Client.GetChannel(chatterChanId) as ISocketMessageChannel;
                await channel.SendMessageAsync("Congratulations " + message.Author.Mention + " you got a card: **" + userCard.ToString() + "**", embed: userCard.GetEmbed());
                if (user.OwnedCards.Count == Settings.GetIntValue(Setting.UserInventoryLimit))
                {
                    await channel.SendMessageAsync($"{message.Author.Mention} your deck is full. You will need to discard cards before you can earn more cards again.");
                }

                return true;
            }

            if (!timedOut && cardRoll < baseFindChance)
            {
                return true;
            }

            return false;
        }

        private async Task HandleReactionAsync(Cacheable<IUserMessage, ulong> arg1, ISocketMessageChannel arg2, SocketReaction arg3)
        {
            if (!Channels.Any(c => c.ChannelId == arg2.Id))
                return;

            try
            {
                await Log(new LogMessage(LogSeverity.Info, "Handler", "Handling Reactions async add in channel " + arg2.Name + ", Reactables: " + string.Join(", ", Reactables)));
                var reactablesInChannel = Reactables.Where(c =>
                {
                    try
                    {
                        return c.Channel.Id == arg2.Id;
                    }
                    catch
                    {
                        Log(new LogMessage(LogSeverity.Critical, "Handler", $"Could not get channel id for IReactable {c}")).RunSynchronously();
                        return false;
                    }
                }).ToList();
                await Log(new LogMessage(LogSeverity.Info, "Handler", $"Found {reactablesInChannel.Count} reactables in channel, with {string.Join(", ", reactablesInChannel)}"));

                var udeck = reactablesInChannel.FirstOrDefault(e =>
                {
                    try
                    {
                        return e.WasReactedTo(arg3.UserId, arg3.MessageId);
                    }
                    catch (Exception ex)
                    {
                        Log(new LogMessage(LogSeverity.Critical, nameof(e), "Critical failure during reactedto", ex)).RunSynchronously();
                    }

                    return false;
                });

                if (udeck != null)
                {
                    _ = Task.Run(() => udeck.ReactAdd(arg3));
                }
            }
            catch (Exception ex)
            {
                await Log(new LogMessage(LogSeverity.Error, "Handler", "Something happened during add", ex));
            }
        }

        private async Task HandleRemoveReactionAsync(Cacheable<IUserMessage, ulong> arg1, ISocketMessageChannel arg2, SocketReaction arg3)
        {
            if (!Channels.Any(c => c.ChannelId == arg2.Id))
                return;

            try
            {
                await Log(new LogMessage(LogSeverity.Info, "Handler", "Handling Reactions async remove in channel " + arg2.Name + ", Reactables: " + string.Join(", ", Reactables)));
                var reactablesInChannel = Reactables.Where(c =>
                {
                    try
                    {
                        return c.Channel.Id == arg2.Id;
                    }
                    catch
                    {
                        Log(new LogMessage(LogSeverity.Critical, "Handler", $"Could not get channel id for IReactable {c}")).RunSynchronously();
                        return false;
                    }
                }).ToList();
                await Log(new LogMessage(LogSeverity.Info, "Handler", $"Found {reactablesInChannel.Count} reactables in channel, with {string.Join(", ", reactablesInChannel)}"));

                var udeck = reactablesInChannel.FirstOrDefault(e =>
                {
                    try
                    {
                        return e.WasReactedTo(arg3.UserId, arg3.MessageId);
                    }
                    catch (Exception ex)
                    {
                        Log(new LogMessage(LogSeverity.Critical, nameof(e), "Critical failure during reactedto", ex)).RunSynchronously();
                    }

                    return false;
                });

                if (udeck != null)
                {
                    _ = Task.Run(() => udeck.ReactRemove(arg3));
                }
            }
            catch (Exception ex)
            {
                await Log(new LogMessage(LogSeverity.Error, "Handler", "Something happened during add", ex));
            }
        }
    }
}