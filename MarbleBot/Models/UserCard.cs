﻿using Discord;
using MarbleBot.Database;
using MarbleBot.Utilities;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Shapes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.IO;
using System.Linq;

namespace MarbleBot.Models
{
    public class UserCard
    {
        [NotMapped]
        public const int MaxCardStat = 10;

        [NotMapped]
        public const int MaxUpgrade = 5;

        public UserCard()
        {
        }

        public UserCard(Card card)
        {
            BaseCard = card;
            UpgradeCount = 0;
            InvestedValue = 0;
        }

        [NotMapped]
        public int Balance => BaseCard.Balance;

        public Card BaseCard { get; set; }

        public string CachedImageFile
        {
            get
            {
                string imagePath = "";
                using (var db = new DatabaseContext())
                {
                    imagePath = db.Settings.GetStringValue(Setting.ImageFolderBase);
                }

                string pathWithUser;
                if (Owner != null) pathWithUser = System.IO.Path.Combine(imagePath, Owner.DiscordId.ToString());
                else pathWithUser = System.IO.Path.Combine(imagePath, "display");
                return System.IO.Path.Combine(pathWithUser, Id.ToString() + GetHashCode() + ".jpg");
            }
        }

        [NotMapped]
        public string Description => BaseCard.Description;

        [NotMapped]
        public int Endurance => BaseCard.Endurance;

        public int HashCode { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [NotMapped]
        public string ImagePath => BaseCard.ImagePath;

        public int InvestedValue { get; set; }

        public Uri Link
        {
            get
            {
                string url;
                using (var db = new DatabaseContext())
                {
                    url = db.Settings.GetStringValue(Setting.ExternalUrlBase);
                }

                if (Owner != null)
                {
                    return new Uri(url + Owner.DiscordId.ToString() + "/" + Id.ToString() + GetHashCode() + ".jpg");
                }
                else
                {
                    return new Uri(url + "display" + "/" + Id.ToString() + GetHashCode() + ".jpg");
                }
            }
        }

        [NotMapped]
        public string Name
        {
            get
            {
                string suffix = "";
                string prefix = "";
                if (UpgradeCount == 5)
                {
                    suffix = " ¤";
                    prefix = "¤ ";
                }
                else
                {
                    int i = 0;
                    while (i < UpgradeCount)
                    {
                        i++;
                        suffix += "+";
                    }
                }

                return prefix + BaseCard.Name + suffix;
            }
        }

        public User Owner { get; set; }

        [NotMapped]
        public int Power => BaseCard.Power;

        [NotMapped]
        public CardRarity Rarity => BaseCard.Rarity;

        [NotMapped]
        public int Strength => BaseCard.Strength;

        [NotMapped]
        public Team Team => BaseCard.Team;

        [NotMapped]
        public int Teamwork => BaseCard.Teamwork;

        [NotMapped]
        public Template Template => BaseCard.Template;

        [NotMapped]
        public int TotalBalance => BaseCard.Balance + UpgradedBalance;

        [NotMapped]
        public int TotalEndurance => BaseCard.Endurance + UpgradedEndurance;

        [NotMapped]
        public int TotalPower => BaseCard.Power + UpgradedPower;

        [NotMapped]
        public int TotalStrength => BaseCard.Strength + UpgradedStrength;

        [NotMapped]
        public int TotalTeamwork => BaseCard.Teamwork + UpgradedTeamwork;

        public int UpgradeCount { get; set; }
        public int UpgradedBalance { get; set; }
        public int UpgradedEndurance { get; set; }
        public int UpgradedPower { get; set; }
        public int UpgradedStrength { get; set; }
        public int UpgradedTeamwork { get; set; }

        [NotMapped]
        public int Value
        {
            get
            {
                return BaseCard.Value * (int)Math.Pow(2, UpgradeCount);
            }
        }

        public void AddUpgrade()
        {
            InvestedValue -= Value;
            UpgradeCount++;
        }

        public Uri CreateImageImageSharp()
        {
            try
            {
                if (!File.Exists(CachedImageFile))
                {
                    List<TemplateField> templateFields;
                    using (var db = new DatabaseContext())
                    {
                        templateFields = (db.TemplateFields as IQueryable<TemplateField>).Where(e => e.AssociatedTemplate == Template).ToList();
                    }

                    byte[] imageArray = Array.Empty<byte>();

                    System.Drawing.Image templateBitmap = Bitmap.FromFile(Template.FullImagePath);

                    SixLabors.Fonts.FontCollection fonts = new SixLabors.Fonts.FontCollection();
                    foreach (var font in Directory.GetFiles(".", "*.ttf"))
                    {
                        fonts.Install(font);
                    }

                    using var templateImage = Image<Rgba32>.Load(Template.FullImagePath);
                    using var image = new Image<Rgba32>(templateImage.Width, templateImage.Height);
                    image.Mutate(ctx =>
                        {
                            var c = BaseCard.GetColor();
                            ctx.Fill(new SixLabors.ImageSharp.Color(new Argb32(c.R, c.G, c.B)));
                            ctx.DrawImage(templateImage, 1);

                            foreach (var field in templateFields.Where(e => e.Text.StartsWith('[') && e.Text.EndsWith(']')))
                            {
                                DrawImageFromTemplateImageSharp(ctx, field);
                            }

                            foreach (var field in templateFields.Where(e => !e.Text.StartsWith('[') && !e.Text.EndsWith(']')))
                            {
                                DrawStringFromTemplateImageSharp(ctx, field, fonts);
                            }
                        });

                    HashCode = GetHashCode();

                    if (Owner != null)
                    {
                        using var db = new DatabaseContext();
                        db.Update(this);
                        db.SaveChanges();
                    }

                    if (!Directory.Exists(System.IO.Path.GetDirectoryName(CachedImageFile)))
                    {
                        Directory.CreateDirectory(System.IO.Path.GetDirectoryName(CachedImageFile));
                    }

                    using FileStream fs = File.OpenWrite(CachedImageFile);
                    image.SaveAsJpeg(fs);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return Link;
        }

        public override bool Equals(object obj)
        {
            return obj is UserCard card &&
                   EqualityComparer<User>.Default.Equals(Owner, card.Owner) &&
                   EqualityComparer<Card>.Default.Equals(BaseCard, card.BaseCard) &&
                   Id == card.Id &&
                   UpgradeCount == card.UpgradeCount &&
                   InvestedValue == card.InvestedValue &&
                   UpgradedStrength == card.UpgradedStrength &&
                   UpgradedEndurance == card.UpgradedEndurance &&
                   UpgradedPower == card.UpgradedPower &&
                   UpgradedBalance == card.UpgradedBalance &&
                   UpgradedTeamwork == card.UpgradedTeamwork &&
                   Value == card.Value &&
                   Name == card.Name &&
                   HashCode == card.HashCode &&
                   Strength == card.Strength &&
                   Endurance == card.Endurance &&
                   Power == card.Power &&
                   Balance == card.Balance &&
                   Teamwork == card.Teamwork &&
                   EqualityComparer<Team>.Default.Equals(Team, card.Team) &&
                   EqualityComparer<Template>.Default.Equals(Template, card.Template) &&
                   ImagePath == card.ImagePath;
        }

        public Embed GetEmbed()
        {
            Uri baseUrl = CreateImageImageSharp();

            GC.Collect();

            var builder = new EmbedBuilder
            {
                ImageUrl = baseUrl.ToString(),
                Url = baseUrl.ToString(),
                Color = (Discord.Color)BaseCard.GetColor()
            };

            return builder.Build();
        }

        public Uri GetEmbedImageUrl()
        {
            return new Uri(CreateImageImageSharp().ToString());
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(BaseCard);
            hash.Add(UpgradeCount);
            hash.Add(InvestedValue);
            return hash.ToHashCode();
        }

        public int GetTotalProperty(CardProperty property)
        {
            var cardProperty = typeof(UserCard).GetProperty("Total" + property);
            return (int)cardProperty.GetValue(this);
        }

        public int GetUpgradeProperty(CardProperty property)
        {
            var cardProperty = typeof(UserCard).GetProperty("Upgraded" + property);
            return (int)cardProperty.GetValue(this);
        }

        public string NameRaw(bool withTeam = true)
        {
            return (withTeam ? BaseCard.Team.Name + " - " : "") + BaseCard.Name;
        }

        public override string ToString()
        {
            switch (BaseCard.Type)
            {
                default:
                case CardType.Normal: return Team + " - " + Name + " (" + Enum.GetName(typeof(CardRarity), (int)BaseCard.Rarity) + $" - {Value}P)";
                case CardType.Legendary: return "**LEGENDARY - " + Name + "**";
            }
        }

        private void DrawImageFromTemplateImageSharp(IImageProcessingContext ctx, TemplateField field)
        {
            string value;
            if (field.Text.StartsWith("[") && field.Text.EndsWith("]"))
            {
                value = field.Text.Replace("[", "").Replace("]", "");
            }
            else return;

            if (value == nameof(ImagePath))
            {
                value = BaseCard.FullImagePath;
            }
            else if (value == nameof(Team) + "." + nameof(Team.ImagePath))
            {
                value = Team.FullImagePath;
            }

            if (!File.Exists(value))
            {
                var files = Directory.GetFiles(System.IO.Path.Combine("Images", "Cards", "Default"));
                value = files[new Random().Next(0, files.Length)];
            }

            try
            {
                using var image = SixLabors.ImageSharp.Image.Load(value);
                var width = (int)field.Width;
                var height = (int)field.Height;
                var ratio = (double)image.Width / (double)image.Height;

                switch (field.ScaleMode)
                {
                    case ScaleMode.FitToHeight:
                        width = (int)(width * ratio);
                        break;

                    case ScaleMode.FitToWidth:
                        height = (int)(height * ratio);
                        break;

                    case ScaleMode.FitToContent:
                    default:
                        break;
                }

                image.Mutate(c => c.Resize(width, height));

                ctx.DrawImage(image, new SixLabors.Primitives.Point(field.X, field.Y), 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void DrawStringFromTemplateImageSharp(IImageProcessingContext ctx, TemplateField field, FontCollection fonts)
        {
            string value = "";
            var propName = "";
            try
            {
                propName = field.Text.Substring(field.Text.IndexOf('{'), (field.Text.LastIndexOf('}') - field.Text.IndexOf('{') + 1));
            }
            catch { }
            if (!string.IsNullOrEmpty(propName))
            {
                try
                {
                    value = GetType().GetProperty(propName.Replace("{", "").Replace("}", "")).GetValue(this).ToString();
                    int.TryParse(value, out var valueInt);
                    if ((string.IsNullOrEmpty(value) || valueInt == 0) && !field.DisplayWhenZero)
                    {
                        return;
                    }

                    value = field.Text.Replace(propName, value);
                }
                catch
                {
                    value = field.Text;
                }
            }
            else value = field.Text;

            value = value.Trim();

            var family = fonts.Families.First(f => f.AvailableStyles.Any(s => s == (SixLabors.Fonts.FontStyle)(int)field.FontStyle) && f.Name == field.FontName);

            SixLabors.Fonts.Font f = new SixLabors.Fonts.Font(family, field.FontSize, (SixLabors.Fonts.FontStyle)(int)field.FontStyle);

            if (field.Text.Contains("Name") || field.Text.Contains("Team")) { value = value.ToUpper(); }

            if (field.Width != 0 && field.Height == 0)
            {
                while (TextMeasurer.Measure(value, new RendererOptions(f)).Width > field.Width)
                {
                    f = new SixLabors.Fonts.Font(family, f.Size - 0.25f, (SixLabors.Fonts.FontStyle)(int)field.FontStyle);
                }
            }

            HorizontalAlignment alignment = HorizontalAlignment.Left;
            if (field.Alignment == StringAlignment.Center) alignment = HorizontalAlignment.Center;
            if (field.Alignment == StringAlignment.Far) alignment = HorizontalAlignment.Right;

            VerticalAlignment valignment = VerticalAlignment.Top;
            if (field.LineAlignment == StringAlignment.Center) valignment = VerticalAlignment.Center;
            if (field.LineAlignment == StringAlignment.Far) valignment = VerticalAlignment.Bottom;

            var text = TextBuilder.GenerateGlyphs(value, new RendererOptions(f)
            {
                HorizontalAlignment = alignment,
                VerticalAlignment = valignment,
                WrappingWidth = field.Width,
                Origin = new SixLabors.Primitives.PointF(field.X, field.Y),
            });

            TextGraphicsOptions options = new TextGraphicsOptions(true);

            ctx.Fill((GraphicsOptions)options, new SixLabors.ImageSharp.Color(new Argb32(field.DrawingColor.R, field.DrawingColor.G, field.DrawingColor.B)), text);
        }
    }
}