﻿using MarbleBot.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarbleBot.Models
{
    public class Channel
    {
        public ulong ChannelId { get; set; }

        public ChannelType ChannelType { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}