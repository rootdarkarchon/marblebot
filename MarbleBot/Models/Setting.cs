﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace MarbleBot.Models
{
    public class Setting
    {
        [DefaultValue(100)]
        public const string BaseFindChance = "CARD_FIND_CHANCE";

        [DefaultValue((ulong)661138455069458433)]
        public const string BotChannelsCategoryId = "BOT_CHANNEL_CATEGORY";

        [DefaultValue("#2ECC71")]
        public const string CommonCardColor = "CARD_COLOR_COMMON";

        [DefaultValue(100)]
        public const string CommonCardRarity = "CARD_RARITY_COMMON";

        [DefaultValue(0)]
        public const string DaysBeforeCleanupUser = "USER_INACTIVITY_DAYS_CLEANUP";

        [DefaultValue("NjYxMTU2NjUyMDE5NjEzNzA3.XgnUYQ.GLSmqY9b_J2g-VkKDJP4RQWFm3c")]
        public const string DiscordBotToken = "DISCORD_BOT_TOKEN";

        [DefaultValue("#9B59B6")]
        public const string EpicCardColor = "CARD_COLOR_EPIC";

        [DefaultValue(2)]
        public const string EpicCardRarity = "CARD_RARITY_EPIC";

        [DefaultValue("http://darkarchon.i234.me:500/marblebot/")]
        public const string ExternalUrlBase = "BASE_URL_EXTERNAL";

        [DefaultValue(@"\\insane-nas\web\marblebot")]
        public const string ImageFolderBase = "BASE_IMAGE_FOLDER";

        [DefaultValue(2.0)]
        public const string InteractiveChannelTimeout = "USER_INTERACTIVE_DECK_TIMEOUT_MINUTES";

        [DefaultValue("#F1C40F")]
        public const string LegendaryCardColor = "CARD_COLOR_LEGENDARY";

        [DefaultValue(10)]
        public const string LegendaryCardRarity = "CARD_RARITY_LEGENDARY";

        [DefaultValue("#E67E22")]
        public const string RareCardColor = "CARD_COLOR_RARE";

        [DefaultValue(10)]
        public const string RareCardRarity = "CARD_RARITY_RARE";

        [DefaultValue("#3498DB")]
        public const string UncommonCardColor = "CARD_COLOR_UNCOMMON";

        [DefaultValue(35)]
        public const string UncommonCardRarity = "CARD_RARITY_UNCOMMON";

        [DefaultValue(30)]
        public const string UserInventoryLimit = "USER_INVENTORY_LIMIT";

        [DefaultValue(0)]
        public const string UserTimeoutMinutes = "USER_TIMEOUT_MINUTES";

        public double DoubleValue => double.Parse(Value, CultureInfo.InvariantCulture);

        public bool Hidden { get; set; }

        public int IntValue => int.Parse(Value);

        [Key]
        public string Name { get; set; }

        public ulong ULongValue => ulong.Parse(Value);
        public string Value { get; set; }
    }
}