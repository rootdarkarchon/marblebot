﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MarbleBot.Models
{
    public class Template
    {
        [NotMapped]
        public string FullImagePath
        {
            get
            {
                return System.IO.Path.Combine("Images", "Templates", ImagePath);
            }
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ImagePath { get; set; }
        public string Name { get; set; }
        public List<TemplateField> TemplateFields { get; set; } = new List<TemplateField>();

        public Template Clone()
        {
            var clone = new Template
            {
                Name = Name + "_1",
                ImagePath = ImagePath,
            };

            clone.TemplateFields = TemplateFields.Select(t => t.Clone(clone)).ToList();
            return clone;
        }

        public override bool Equals(object obj)
        {
            return obj is Template template &&
                   Id == template.Id &&
                   Name == template.Name &&
                   ImagePath == template.ImagePath &&
                   EqualityComparer<List<TemplateField>>.Default.Equals(TemplateFields, template.TemplateFields);
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(Id);
            hash.Add(Name);
            hash.Add(ImagePath);
            foreach (var field in TemplateFields.OrderBy(t => t.Id))
            {
                hash.Add(field);
            }
            return hash.ToHashCode();
        }

        public override string ToString()
        {
            return $"#{Id} {Name} I({ImagePath}), Defined Fields({TemplateFields.Count})";
        }

        internal string DetailString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(this.ToString());
            foreach (var field in TemplateFields)
            {
                sb.AppendLine(field.ToString());
            }
            return sb.ToString();
        }
    }
}