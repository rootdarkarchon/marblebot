﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarbleBot.Models
{
    public class Team
    {
        [NotMapped]
        public string FullImagePath
        {
            get
            {
                return System.IO.Path.Combine("Images", "Teams", ImagePath);
            }
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ImagePath { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Team team &&
                   Id == team.Id &&
                   Name == team.Name &&
                   ImagePath == team.ImagePath;
        }

        public override int GetHashCode()
        {
            HashCode code = new HashCode();
            code.Add(Name);
            code.Add(ImagePath);
            return code.ToHashCode();
        }

        public override string ToString()
        {
            return Name;
        }

        internal string DetailString()
        {
            return $"#{Id} {Name} I({ImagePath})";
        }
    }
}