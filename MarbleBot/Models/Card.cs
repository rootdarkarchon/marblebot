﻿using Discord;
using MarbleBot.Database;
using MarbleBot.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarbleBot.Models
{
    public class Card
    {
        public int Balance { get; set; }

        public string Description { get; set; }

        public int Endurance { get; set; }

        [NotMapped]
        public string FullImagePath
        {
            get
            {
                return System.IO.Path.Combine("Images", "Cards", ImagePath);
            }
        }

        public int HashCode { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ImagePath { get; set; }
        public string Name { get; set; }
        public int Power { get; set; }
        public int Quantity { get; set; }
        public CardRarity Rarity { get; set; }
        public int Strength { get; set; }
        public Team Team { get; set; }
        public int Teamwork { get; set; }
        public Template Template { get; set; }
        public CardType Type { get; set; }
        public int Value { get; set; }

        public static int GetCardQuantity(CardRarity rarity)
        {
            return rarity switch
            {
                CardRarity.Common => 40,
                CardRarity.Uncommon => 20,
                CardRarity.Rare => 10,
                CardRarity.Epic => 5,
                CardRarity.Undefined => 2,
                _ => 2,
            };
        }

        public static int GetCardValue(CardRarity rarity)
        {
            return rarity switch
            {
                CardRarity.Common => 1,
                CardRarity.Uncommon => 2,
                CardRarity.Rare => 4,
                CardRarity.Epic => 8,
                CardRarity.Undefined => 0,
                _ => 0,
            };
        }

        public static System.Drawing.Color GetColor(CardType type, CardRarity rarity)
        {
            using var db = new DatabaseContext();
            if (type == CardType.Legendary)
            {
                return System.Drawing.ColorTranslator.FromHtml(db.Settings.GetStringValue(Setting.LegendaryCardColor));
            }
            if (rarity == CardRarity.Common)
            {
                return System.Drawing.ColorTranslator.FromHtml(db.Settings.GetStringValue(Setting.CommonCardColor));
            }
            else if (rarity == CardRarity.Uncommon)
            {
                return System.Drawing.ColorTranslator.FromHtml(db.Settings.GetStringValue(Setting.UncommonCardColor));
            }
            else if (rarity == CardRarity.Rare)
            {
                return System.Drawing.ColorTranslator.FromHtml(db.Settings.GetStringValue(Setting.RareCardColor));
            }
            else if (rarity == CardRarity.Epic)
            {
                return System.Drawing.ColorTranslator.FromHtml(db.Settings.GetStringValue(Setting.EpicCardColor));
            }
            else
            {
                return Color.LightGrey;
            }
        }

        public string DetailString()
        {
            return $"#{Id} {Name}, {Team.Name} | S{Strength} E{Endurance} P{Power} B{Balance} T{Teamwork} | {Rarity} {Type} R({Quantity}) | T({Template.Name}) I({ImagePath})";
        }

        public override bool Equals(object obj)
        {
            return obj is Card @base &&
                   Id == @base.Id &&
                   Rarity == @base.Rarity &&
                   Type == @base.Type &&
                   Name == @base.Name &&
                   EqualityComparer<Team>.Default.Equals(Team, @base.Team) &&
                   EqualityComparer<Template>.Default.Equals(Template, @base.Template) &&
                   Strength == @base.Strength &&
                   Endurance == @base.Endurance &&
                   Power == @base.Power &&
                   Value == @base.Value &&
                   Description == @base.Description &&
                   Balance == @base.Balance &&
                   Teamwork == @base.Teamwork &&
                   HashCode == @base.HashCode;
        }

        public System.Drawing.Color GetColor()
        {
            return GetColor(Type, Rarity);
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(Id);
            hash.Add(Rarity);
            hash.Add(Type);
            hash.Add(Name);
            hash.Add(Team);
            hash.Add(Template);
            hash.Add(Strength);
            hash.Add(Endurance);
            hash.Add(Power);
            hash.Add(Value);
            hash.Add(Description);
            hash.Add(Balance);
            hash.Add(Teamwork);
            return hash.ToHashCode();
        }

        public override string ToString()
        {
            switch (Type)
            {
                default:
                case CardType.Normal: return Team + " - " + Name + " (" + Enum.GetName(typeof(CardRarity), (int)Rarity) + $" - {Value}P)";
                case CardType.Legendary: return "**LEGENDARY - " + Name + "**";
            }
        }
    }
}