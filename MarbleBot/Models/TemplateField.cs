﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;

namespace MarbleBot.Models
{
    public enum ScaleMode
    {
        FitToContent = 0,
        FitToHeight = 1,
        FitToWidth = 2
    }

    public class TemplateField
    {
        public StringAlignment Alignment { get; set; } = StringAlignment.Near;

        public Template AssociatedTemplate { get; set; }

        public string Color { get; set; }

        public bool DisplayWhenZero { get; set; } = true;

        [NotMapped]
        public System.Drawing.Color DrawingColor
        {
            get
            {
                return System.Drawing.ColorTranslator.FromHtml(Color);
            }
            set
            {
                Color = System.Drawing.ColorTranslator.ToHtml(value);
            }
        }

        public string FontName { get; set; }

        public float FontSize { get; set; }

        public FontStyle FontStyle { get; set; } = System.Drawing.FontStyle.Regular;

        public int Height { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public StringAlignment LineAlignment { get; set; } = StringAlignment.Near;
        public ScaleMode ScaleMode { get; set; } = ScaleMode.FitToContent;
        public string Text { get; set; }
        public int Width { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public TemplateField Clone(Template parent)
        {
            return new TemplateField
            {
                Text = Text,
                AssociatedTemplate = parent,
                X = X,
                Y = Y,
                Width = Width,
                Height = Height,
                FontSize = FontSize,
                Color = Color,
                FontName = FontName,
                Alignment = Alignment,
                LineAlignment = LineAlignment,
                DisplayWhenZero = DisplayWhenZero,
                FontStyle = FontStyle,
                ScaleMode = ScaleMode
            };
        }

        public override bool Equals(object obj)
        {
            return obj is TemplateField field &&
                   Id == field.Id &&
                   Text == field.Text &&
                   EqualityComparer<Template>.Default.Equals(AssociatedTemplate, field.AssociatedTemplate) &&
                   X == field.X &&
                   Y == field.Y &&
                   Width == field.Width &&
                   Height == field.Height &&
                   FontSize == field.FontSize &&
                   Color == field.Color &&
                   FontName == field.FontName &&
                   Alignment == field.Alignment &&
                   LineAlignment == field.LineAlignment &&
                   DisplayWhenZero == field.DisplayWhenZero &&
                   ScaleMode == field.ScaleMode &&
                   FontStyle == field.FontStyle &&
                   DrawingColor.Equals(field.DrawingColor);
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(Id);
            hash.Add(Text);
            hash.Add(X);
            hash.Add(Y);
            hash.Add(Width);
            hash.Add(Height);
            hash.Add(FontSize);
            hash.Add(Color);
            hash.Add(FontName);
            hash.Add(Alignment);
            hash.Add(LineAlignment);
            hash.Add(DisplayWhenZero);
            hash.Add(ScaleMode);
            hash.Add(FontStyle);
            hash.Add(DrawingColor);
            return hash.ToHashCode();
        }

        public override string ToString()
        {
            return $"**#{Id} T({Text})**{Environment.NewLine}\tX{X}, Y{Y}, W{Width}, H{Height}{Environment.NewLine}\tF({FontName}), FSize{FontSize}, FS({FontStyle}), C({Color}), H({Alignment}), V({LineAlignment}){Environment.NewLine}\t0({DisplayWhenZero}), Scale({ScaleMode})";
        }
    }
}