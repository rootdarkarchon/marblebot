﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarbleBot.Models
{
    public class User
    {
        [Key]
        public ulong DiscordId { get; set; }

        public bool IsBanned { get; set; }
        public bool IsModerator { get; set; }
        public bool IsOptedOut { get; set; }
        public bool IsOwner { get; set; }
        public DateTime LastEarnedCard { get; set; }
        public List<UserCard> OwnedCards { get; set; } = new List<UserCard>();
    }
}