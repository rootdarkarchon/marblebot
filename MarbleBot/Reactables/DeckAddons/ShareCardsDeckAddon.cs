﻿using Discord;
using Discord.WebSocket;
using MarbleBot.Interfaces;
using MarbleBot.Reactables;
using MarbleBot.Utilities.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class ShareCardsDeckAddon : ICardsReactionDeckAddon, IFooterMessageDeckAddon
    {
        public IEmote ReactShareEmote = new Emoji("⭐");
        private readonly ISocketMessageChannel Channel;
        private DateTime LastShare;

        public ShareCardsDeckAddon(SocketUser self, ISocketMessageChannel channel)
        {
            Self = self;
            Channel = channel;
        }

        public DeckReactable Deck { get; set; }
        public IEnumerable<IEmote> DeckReactionEmotes => new List<IEmote> { ReactShareEmote };
        public string FooterMessage => $"React to the cards with {ReactShareEmote} to share the cards to {(Channel as ITextChannel).Mention}";

        public SocketUser Self { get; private set; }

        public async Task HandleDeckReactionAdd(object sender, ReactionEventArgs reaction)
        {
            if (reaction.Reaction.Emote.Name != ReactShareEmote.Name) return;

            if ((DateTime.Now - LastShare).TotalSeconds < 30)
            {
                await reaction.MessageWithCard.Message.Channel.SendMessageAsync($"You have to wait another {30 - (int)(DateTime.Now - LastShare).TotalSeconds} seconds before you can share again.");
            }
            else if (reaction.MessageWithCard.Card != null)
            {
                await Channel.SendMessageAsync($"{Deck.Initiator.Username} shows off this card from their deck: **" + reaction.MessageWithCard.Card.ToString() + "**", embed: reaction.MessageWithCard.Card.GetEmbed());
                LastShare = DateTime.Now;
            }

            await Deck.RemoveReactions(reaction.MessageWithCard.Message, reaction.Reaction.User.Value, ReactShareEmote);
        }

        public async Task ManageReactionsToDeck(object sender, AddToDeckEventArgs messageEvent)
        {
            foreach (var msg in new List<MessageWithCard>(messageEvent.Messages.Where(e => e.Card != null)))
            {
                try
                {
                    if (!msg.Message.Reactions.Any(r => r.Key.Name == ReactShareEmote.Name))
                    {
                        await Deck.AddReactions(msg.Message, ReactShareEmote);
                    }
                }
                catch { }
            }
            foreach (var msg in new List<MessageWithCard>(messageEvent.Messages.Where(e => e.Card == null)))
            {
                try
                {
                    if (msg.Message.Reactions.Any(r => r.Key.Name == ReactShareEmote.Name && r.Value.ReactionCount > 0))
                    {
                        await Deck.RemoveReactions(msg.Message, Self, ReactShareEmote);
                    }
                }
                catch { }
            }
        }
    }
}