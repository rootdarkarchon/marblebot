﻿using Discord;
using Discord.WebSocket;
using MarbleBot.Interfaces;
using MarbleBot.Reactables;
using MarbleBot.Utilities.Event;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class ExpandCardsDeckAddon : ICardsReactionDeckAddon, IFooterMessageDeckAddon
    {
        public IEmote ReactExpandEmote = new Emoji("🔎");

        public ExpandCardsDeckAddon(SocketUser self)
        {
            Self = self;
        }

        public DeckReactable Deck { get; set; }
        public IEnumerable<IEmote> DeckReactionEmotes => new List<IEmote> { ReactExpandEmote };

        public string FooterMessage => $"React to the cards with {ReactExpandEmote} to display the card info.";

        public SocketUser Self { get; private set; }

        public async Task HandleDeckReactionAdd(object sender, ReactionEventArgs reaction)
        {
            if (reaction.Reaction.Emote.Name != ReactExpandEmote.Name) return;

            var deck = sender as DeckReactable;

            if (reaction.MessageWithCard.Card != null && reaction.MessageWithCard.Message.Embeds.Count == 0)
            {
                await reaction.MessageWithCard.Message.ModifyAsync((e) =>
                {
                    e.Embed = reaction.MessageWithCard.Card.GetEmbed();
                    e.Content = "";
                });

                foreach (var msg in Deck.DeckMessages.Where(k => k.Message != reaction.MessageWithCard.Message))
                {
                    if (msg.Message.Embeds.Count > 0)
                    {
                        await msg.Message.ModifyAsync((e) =>
                        {
                            e.Embed = null;
                            e.Content = msg.Card == null ? "-" : $"**{msg.Card}**";
                        });
                    }
                }
            }
            else if (reaction.MessageWithCard.Card != null && reaction.MessageWithCard.Message.Embeds.Count > 0)
            {
                await reaction.MessageWithCard.Message.ModifyAsync((e) =>
                {
                    e.Embed = null;
                    e.Content = reaction.MessageWithCard.Card == null ? "-" : $"**{reaction.MessageWithCard.Card}**";
                });
            }

            await Deck.RemoveReactions(reaction.MessageWithCard.Message, Deck.Initiator, ReactExpandEmote);
        }

        public async Task ManageReactionsToDeck(object sender, AddToDeckEventArgs messageEvent)
        {
            foreach (var msg in new List<MessageWithCard>(messageEvent.Messages.Where(e => e.Card != null)))
            {
                try
                {
                    if (!msg.Message.Reactions.Any(r => r.Key.Name == ReactExpandEmote.Name))
                    {
                        await Deck.AddReactions(msg.Message, ReactExpandEmote);
                    }
                }
                catch { }
            }
            foreach (var msg in new List<MessageWithCard>(messageEvent.Messages.Where(e => e.Card == null)))
            {
                try
                {
                    if (msg.Message.Reactions.Any(r => r.Key.Name == ReactExpandEmote.Name && r.Value.ReactionCount > 0))
                    {
                        await Deck.RemoveReactions(msg.Message, Self, ReactExpandEmote);
                    }
                }
                catch { }
            }
        }
    }
}