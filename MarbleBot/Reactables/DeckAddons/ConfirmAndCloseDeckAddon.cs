﻿using Discord;
using Discord.WebSocket;
using MarbleBot.Interfaces;
using MarbleBot.Reactables;
using MarbleBot.Utilities.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class ConfirmAndCloseDeckAddon : IFooterReactionAddon<object>
    {
        protected readonly IEmote ReactConfirmEmote = new Emoji("✅");
        protected bool ReactedToFooter = false;

        public ConfirmAndCloseDeckAddon(SocketUser self)
        {
            Self = self;
            ReactedToFooter = false;
        }

        public bool ConfirmResult { get; protected set; }
        public DeckReactable Deck { get; set; }
        public virtual string FooterMessage => $"Close this deck with {ReactConfirmEmote} once you are done.";
        public IEnumerable<IEmote> FooterReactionEmotes => new List<IEmote> { ReactConfirmEmote };
        public virtual string HeaderMessage => HeaderMsg;

        public string HeaderMsg { get; set; }

        public object Result => null;

        public SocketUser Self { get; private set; }

        public bool WaitForExtension { get; protected set; } = true;

        public virtual async Task AddReactionToFooter(object sender, ReactionAddedEventArgs messageEvent)
        {
            await Deck.AddReactions(messageEvent.Message, ReactConfirmEmote);
        }

        public void ClearResult()
        {
            ReactedToFooter = false;
        }

        public virtual Task HandleFooterReactionAdd(object sender, ReactionEventArgs reaction)
        {
            if (FooterReactionEmotes.Select(c => c.Name).Contains(reaction.Reaction.Emote.Name) && !ReactedToFooter)
            {
                ConfirmResult = reaction.Reaction.Emote.Name == ReactConfirmEmote.Name;

                ReactedToFooter = true;

                WaitForExtension = false;
            }

            return Task.CompletedTask;
        }

        public virtual Task HandleFooterReactionRemove(object sender, ReactionEventArgs reaction)
        {
            ReactedToFooter = false;

            return Task.CompletedTask;
        }
    }
}