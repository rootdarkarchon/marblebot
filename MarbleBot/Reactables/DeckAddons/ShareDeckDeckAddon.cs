﻿using Discord;
using Discord.WebSocket;
using MarbleBot.Database;
using MarbleBot.Interfaces;
using MarbleBot.Models;
using MarbleBot.Reactables;
using MarbleBot.Utilities.Event;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class ShareDeckDeckAddon : IFooterReactionAddon<string>
    {
        public IEmote ReactPictureEmote = new Emoji("📷");
        private readonly List<UserCard> Cards;
        private readonly SocketUser Initiator;

        public ShareDeckDeckAddon(SocketUser self, SocketUser initiator, List<UserCard> cards)
        {
            Self = self;
            this.Initiator = initiator;
            this.Cards = cards;
        }

        public bool ConfirmResult => false;
        public DeckReactable Deck { get; set; }
        public string FooterMessage => $"React to this message with {ReactPictureEmote} generate a full deck picture.";

        public IEnumerable<IEmote> FooterReactionEmotes => new List<IEmote>() { ReactPictureEmote };
        public string Result => "";
        public SocketUser Self { get; private set; }
        public bool WaitForExtension => false;

        private Uri DeckUri
        {
            get
            {
                var urlBase = "";
                using (var db = new DatabaseContext())
                {
                    urlBase = db.Settings.GetStringValue(Setting.ExternalUrlBase);
                }

                var uriReturn = new Uri(urlBase + "" + Initiator.Id + "/" + "deck-" + DateTime.Now.ToString("yyyyMMdd") + ".jpg");
                return uriReturn;
            }
        }

        private string GeneratedDeckPath
        {
            get
            {
                using var db = new DatabaseContext();

                return Path.Combine(db.Settings.GetStringValue(Setting.ImageFolderBase), Initiator.Id.ToString(), "deck-" + DateTime.Now.ToString("yyyyMMdd") + ".jpg");
            }
        }

        public async Task AddReactionToFooter(object sender, ReactionAddedEventArgs messageEvent)
        {
            await Deck.AddReactions(messageEvent.Message, ReactPictureEmote);
        }

        public void ClearResult()
        {
        }

        public async Task HandleFooterReactionAdd(object sender, ReactionEventArgs reaction)
        {
            if (reaction.Reaction.Emote.Name != ReactPictureEmote.Name) return;

            await Deck.RemoveReactions(reaction.MessageWithCard.Message, reaction.Reaction.User.Value, ReactPictureEmote);

            if (File.Exists(GeneratedDeckPath))
            {
                await reaction.MessageWithCard.Message.Channel.SendMessageAsync($"You cannot generate your deck more than once per day. Your current deck is found here: " + DeckUri.ToString());
            }
            else
            {
                // generate image out of cards
                GenerateDeckCollage(Cards);

                // send
                await reaction.MessageWithCard.Message.Channel.SendMessageAsync($"Here is your generated deck image: " + DeckUri.ToString());
            }
        }

        public Task HandleFooterReactionRemove(object sender, ReactionEventArgs reaction)
        {
            return Task.CompletedTask;
        }

        private void GenerateDeckCollage(List<UserCard> cards)
        {
            int singleImgWidth = 0;
            int singleImgHeight = 0;

            foreach (var card in cards)
            {
                _ = card.CreateImageImageSharp();
            }

            using (var img = Image<Argb32>.Load(cards.First().CachedImageFile))
            {
                singleImgHeight = img.Height;
                singleImgWidth = img.Width;
            }

            int imageColumns = 5;
            int imageRows = (int)Math.Ceiling(cards.Count / (double)imageColumns);

            int headerSize = 150;

            using (Image<Argb32> img = new Image<Argb32>(imageColumns * singleImgWidth, (imageRows * singleImgHeight) + headerSize))
            {
                SixLabors.Fonts.FontCollection fonts = new SixLabors.Fonts.FontCollection();
                foreach (var font in Directory.GetFiles(".", "*.ttf"))
                {
                    fonts.Install(font);
                }

                img.Mutate(ctx =>
                {
                    ctx.Fill(new SixLabors.ImageSharp.Color(new Argb32(20, 20, 20)));

                    int curRow = 0;

                    Font textFont = new Font(fonts.Families.First(f => f.AvailableStyles.Contains(FontStyle.Bold)), 72);

                    string username = Initiator.Username;

                    Regex regex = new Regex("[^\u0000-\u007F]+");

                    username = regex.Replace(username, "");
                    if (string.IsNullOrEmpty(username))
                    {
                        username = Initiator.Id.ToString();
                    }

                    ctx.DrawText("Deck of " + username + " (Total Value: " + cards.Sum(c => c.Value + c.InvestedValue) + "P)",
                        textFont, new SixLabors.ImageSharp.Color(new Argb32(200, 200, 200)),
                        new Point(20, 25));

                    for (int image = 0; image < cards.Count; image++)
                    {
                        using var tempImg = Image<Argb32>.Load(cards[image].CachedImageFile);
                        if (image != 0 && image % imageColumns == 0) curRow++;
                        ctx.DrawImage(tempImg, new Point((image % imageColumns) * singleImgWidth, (curRow * singleImgHeight) + headerSize), 1.0f);
                    }
                });

                using var fs = File.OpenWrite(GeneratedDeckPath);
                img.SaveAsJpeg(fs);
            }
        }
    }
}