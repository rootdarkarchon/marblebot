﻿using Discord;
using Discord.WebSocket;
using MarbleBot.Interfaces;
using MarbleBot.Models;
using MarbleBot.Reactables;
using MarbleBot.Utilities.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class SelectCardsDeckAddon : ICardsReactionDeckAddon, IFooterReactionAddon<List<UserCard>>, IHeaderMessageExtension
    {
        public int MaxSelect;
        public int MinSelect;
        public List<UserCard> SelectedCards;
        protected readonly IEmote ReactCancelEmote = new Emoji("❌");
        protected readonly IEmote ReactConfirmEmote = new Emoji("✅");
        protected readonly IEmote ReactUndoEmote = new Emoji("↩️");
        protected bool ReactedToFooter = false;
        private string headerMsg = "";

        public SelectCardsDeckAddon(SocketUser self, int minSelect, int maxSelect)
        {
            Self = self;
            ReactedToFooter = false;
            SelectedCards = new List<UserCard>();
            MinSelect = minSelect;
            MaxSelect = maxSelect;
        }

        public bool ConfirmResult { get; protected set; }
        public DeckReactable Deck { get; set; }
        public IEnumerable<IEmote> DeckReactionEmotes => new List<IEmote> { ReactConfirmEmote };
        public virtual string FooterMessage => $"Select with {ReactConfirmEmote}, unselect with {ReactUndoEmote}. React to this message with {ReactConfirmEmote} to confirm and {ReactCancelEmote} to cancel.";
        public IEnumerable<IEmote> FooterReactionEmotes => new List<IEmote> { ReactConfirmEmote, ReactCancelEmote };
        public virtual string HeaderMessage => HeaderMsg;

        public string HeaderMsg
        {
            get
            {
                if (string.IsNullOrEmpty(headerMsg))
                {
                    return $"> Select " + ((MinSelect == 1 && MaxSelect == 1) ? "a card" : $"minimum **{MinSelect}** to maximum **{MaxSelect}** card(s)");
                }

                return headerMsg;
            }
            set
            {
                headerMsg = value;
            }
        }

        public List<UserCard> Result => SelectedCards;

        public SocketUser Self { get; private set; }

        public bool WaitForExtension { get; protected set; } = true;

        public virtual async Task AddReactionToFooter(object sender, ReactionAddedEventArgs messageEvent)
        {
            if (MinSelect == 0)
            {
                await Deck.AddReactions(Deck.DeckFooter, ReactConfirmEmote);
            }
            await Deck.AddReactions(Deck.DeckFooter, ReactCancelEmote);
        }

        public void ClearResult()
        {
            SelectedCards = new List<UserCard>();
            ReactedToFooter = false;
            WaitForExtension = true;
        }

        public virtual async Task HandleDeckReactionAdd(object sender, ReactionEventArgs reaction)
        {
            if (reaction.Reaction.UserId == Self.Id) return;

            try
            {
                if (reaction.MessageWithCard.Card != null && reaction.Reaction.Emote.Name == ReactConfirmEmote.Name && !SelectedCards.Contains(reaction.MessageWithCard.Card))
                {
                    SelectedCards.Add(reaction.MessageWithCard.Card);
                    await Deck.AddReactions(reaction.MessageWithCard.Message, ReactUndoEmote);
                    await Deck.RemoveReactions(reaction.MessageWithCard.Message, Deck.Initiator, ReactConfirmEmote);

                    if (MinSelect == 1 && MaxSelect == 1)
                    {
                        SelectedCards.RemoveAll(c => c != reaction.MessageWithCard.Card);
                        foreach (var kvp in Deck.DeckMessages
                            .Where(k => k.Message != reaction.MessageWithCard.Message && k.Message.Reactions.Any(r => r.Key.Name == ReactUndoEmote.Name)))
                        {
                            await Deck.RemoveReactions(kvp.Message, Self, ReactUndoEmote);
                        }
                    }

                    if (SelectedCards.Count >= MinSelect && SelectedCards.Count <= MaxSelect)
                    {
                        if (!Deck.DeckFooter.Reactions.Any(e => e.Key.Name == ReactConfirmEmote.Name))
                        {
                            await Deck.AddReactions(Deck.DeckFooter, ReactConfirmEmote);
                        }
                    }
                    else
                    {
                        if (Deck.DeckFooter.Reactions.Any(e => e.Key.Name == ReactConfirmEmote.Name))
                        {
                            await Deck.RemoveReactions(Deck.DeckFooter, Self, ReactConfirmEmote);
                        }
                    }
                }
                else if (reaction.MessageWithCard.Card != null && reaction.Reaction.Emote.Name == ReactUndoEmote.Name)
                {
                    SelectedCards.Remove(reaction.MessageWithCard.Card);
                    await Deck.RemoveReactions(reaction.MessageWithCard.Message, Deck.Initiator, ReactUndoEmote);
                    await Deck.RemoveReactions(reaction.MessageWithCard.Message, Self, ReactUndoEmote);
                    if (SelectedCards.Count >= MinSelect && SelectedCards.Count <= MaxSelect)
                    {
                        await Deck.AddReactions(Deck.DeckFooter, ReactConfirmEmote);
                    }
                    else
                    {
                        await Deck.RemoveReactions(Deck.DeckFooter, Self, ReactConfirmEmote);
                    }
                }
            }
            catch { }
        }

        public virtual async Task HandleFooterReactionAdd(object sender, ReactionEventArgs reaction)
        {
            if (FooterReactionEmotes.Select(c => c.Name).Contains(reaction.Reaction.Emote.Name) && !ReactedToFooter
                && reaction.MessageWithCard.Message.Reactions.Any(e => e.Key.Name == reaction.Reaction.Emote.Name && e.Value.IsMe))
            {
                ConfirmResult = reaction.Reaction.Emote.Name == ReactConfirmEmote.Name;

                if (ConfirmResult && (SelectedCards.Count > MaxSelect || SelectedCards.Count < MinSelect))
                {
                    await Deck.Channel.SendMessageAsync("You selected " + SelectedCards.Count + " cards but " + MinSelect + " is the minimum and " + MaxSelect + " is the maximum");
                    return;
                }

                if (!ConfirmResult)
                {
                    SelectedCards = new List<UserCard>();
                }

                ReactedToFooter = true;

                WaitForExtension = false;
            }
        }

        public virtual Task HandleFooterReactionRemove(object sender, ReactionEventArgs reaction)
        {
            ReactedToFooter = false;

            return Task.CompletedTask;
        }

        public virtual async Task ManageReactionsToDeck(object sender, AddToDeckEventArgs messageEvent)
        {
            foreach (var msg in new List<MessageWithCard>(messageEvent.Messages.Where(e => e.Card != null)))
            {
                try
                {
                    if (!msg.Message.Reactions.Any(e => e.Key.Name == ReactConfirmEmote.Name))
                    {
                        await Deck.AddReactions(msg.Message, ReactConfirmEmote);
                    }

                    if (SelectedCards.Contains(msg.Card) && !msg.Message.Reactions.Any(e => e.Key.Name == ReactUndoEmote.Name))
                    {
                        await Deck.AddReactions(msg.Message, ReactUndoEmote);
                    }
                    else if (!SelectedCards.Contains(msg.Card) && msg.Message.Reactions.Any(e => e.Key.Name == ReactUndoEmote.Name))
                    {
                        await Deck.RemoveReactions(msg.Message, Self, ReactUndoEmote);
                    }
                }
                catch { }
            }

            foreach (var msg in new List<MessageWithCard>(messageEvent.Messages.Where(e => e.Card == null)))
            {
                try
                {
                    if (msg.Message.Reactions.Any(e => e.Key.Name == ReactUndoEmote.Name && e.Value.ReactionCount > 0))
                    {
                        await Deck.RemoveReactions(msg.Message, Self, ReactUndoEmote);
                    }
                    if (msg.Message.Reactions.Any(e => e.Key.Name == ReactConfirmEmote.Name && e.Value.ReactionCount > 0))
                    {
                        await Deck.RemoveReactions(msg.Message, Self, ReactConfirmEmote);
                    }
                }
                catch { }
            }
        }
    }
}