﻿using Discord.WebSocket;
using MarbleBot.Models;
using MarbleBot.Utilities.Event;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class UpgradeMultiSelectCardsDeckAddon : SelectCardsDeckAddon
    {
        public UserCard CardToUpgrade;
        public int TotalValue;

        public UpgradeMultiSelectCardsDeckAddon(SocketUser self, UserCard cardToUpgrade) : base(self, 1, int.MaxValue)
        {
            CardToUpgrade = cardToUpgrade;
        }

        public override string FooterMessage => $"Select cards to invest with {ReactConfirmEmote}. React to this message with {ReactConfirmEmote} to confirm the selection and {ReactCancelEmote} to cancel.";

        public override string HeaderMessage => $"> Select the cards you want to invest. You need at least a total value of {CardToUpgrade.Value - CardToUpgrade.InvestedValue}P to fully upgrade the card.";

        public override async Task HandleDeckReactionAdd(object sender, ReactionEventArgs reaction)
        {
            await base.HandleDeckReactionAdd(sender, reaction);
            if (reaction.MessageWithCard.Card != null && reaction.Reaction.Emote.Name == ReactConfirmEmote.Name
                && reaction.MessageWithCard.Message.Reactions.Any(e => e.Key.Name == reaction.Reaction.Emote.Name && e.Value.IsMe))
            {
                TotalValue += reaction.MessageWithCard.Card.Value;
            }
            else if (reaction.MessageWithCard.Card != null && reaction.Reaction.Emote.Name == ReactUndoEmote.Name)
            {
                TotalValue -= reaction.MessageWithCard.Card.Value;
            }
        }
    }
}