﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using MarbleBot.Interfaces;
using MarbleBot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Reactables
{
    public class UserEffortReactable : ReactableBase
    {
        public readonly IEmote ConfirmEmote = new Emoji("✅");
        public readonly IEmote ForfeitEmote = new Emoji("🏳️");
        public readonly IEmote HighEffort = new Emoji("💪");
        public readonly IEmote LowEffort = new Emoji("🐢");
        public readonly IEmote MediumEffort = new Emoji("🏃");
        public List<IEmote> EffortEmotes;
        public UserEffortMessage EffortMessage;
        private ulong footerMessageId;

        public UserEffortReactable(SocketUser user, UserTransaction transaction, CommandHandler handler) : base(user, transaction, handler)
        {
            EffortEmotes = new List<IEmote>();
        }

        public IUserMessage FooterMessage => GetMessageById(footerMessageId);
        protected override bool AdditionalCompletion => false;
        protected override bool ConfirmResult { get; set; }

        public async Task PostConfirmationMessage(CardProperty property, int ownStat, int otherStat)
        {
            MessageReacted = false;
            ConfirmResult = false;
            EffortMessage = new UserEffortMessage(null, property, null);

            await Execute(async (msgs) =>
            {
                var msg = $"{Initiator.Mention}, select your effort in the **{property}** category. Your {property} is {ownStat}, your opponents {property} is {otherStat}.";
                var effortMsg = await Channel.SendMessageAsync(msg) as RestUserMessage;
                msgs.Add(effortMsg);

                await AddReactions(effortMsg, EffortEmotes.ToArray());
                EffortMessage = new UserEffortMessage(() => GetMessageById(effortMsg.Id), property, null);

                string messageToSend = $"Select {ConfirmEmote} to confirm. Select {ForfeitEmote} to forfeit this match.";
                var footer = await Channel.SendMessageAsync(messageToSend) as RestUserMessage;
                footerMessageId = footer.Id;
                msgs.Add(footer);

                await AddReactions(footer, ForfeitEmote);
            });
        }

        public async Task PostInitialEffortMessage(IMessageChannel channel)
        {
            EffortEmotes = new List<IEmote>
            {
                LowEffort,
                MediumEffort,
                HighEffort
            };

            string messageToSend = $"Select the effort you want to put in each category. The categories will be chosen live during the duel." + Environment.NewLine + "*Generally: high stats = low effort, low stats = high effort, but this is up to you to decide.*" + Environment.NewLine;

            messageToSend += $"> {LowEffort} = Low Effort, No Risk, No reward" + Environment.NewLine;
            messageToSend += $"> {MediumEffort} = Medium Effort, Medium Risk, Medium Reward" + Environment.NewLine;
            messageToSend += $"> {HighEffort} = High Effort, High Risk, High Reward" + Environment.NewLine;

            _ = await channel.SendMessageAsync(messageToSend) as RestUserMessage;
        }

        public Effort SelectedEffort()
        {
            var effort = Effort.LowEffort;
            if (EffortMessage.Emote.Name == HighEffort.Name)
            {
                effort = Effort.HighEffort;
            }
            if (EffortMessage.Emote.Name == MediumEffort.Name)
            {
                effort = Effort.MediumEffort;
            }
            return effort;
        }

        protected override async Task ReactAddImplementation(SocketReaction reaction)
        {
            if ((reaction.Emote.Name == ConfirmEmote.Name || reaction.Emote.Name == ForfeitEmote.Name)
                && reaction.Message.Value.Reactions.Any(e => e.Key.Name == reaction.Emote.Name && e.Value.IsMe))
            {
                MessageReacted = true;
                ConfirmResult = (reaction.Emote.Name == ConfirmEmote.Name);

                if (ConfirmResult)
                {
                    await Channel.SendMessageAsync($"You chose {SelectedEffort()} for {EffortMessage.Property} - head back to {Transaction.ParentTransaction.Channel.Mention} for the results.");
                }
                else
                {
                    await Channel.SendMessageAsync("You forfeited the match.");
                }
            }
            else if (EffortEmotes.Select(e => e.Name).Contains(reaction.Emote.Name))
            {
                _ = HandleEffortSelection(reaction.Message.Value, reaction.Emote, true);
            }
        }

        protected override Task ReactRemoveImplementation(SocketReaction reaction)
        {
            if (reaction.Emote.Name == ConfirmEmote.Name)
            {
                MessageReacted = false;
            }
            else if (EffortEmotes.Select(e => e.Name).Contains(reaction.Emote.Name))
            {
                _ = HandleEffortSelection(reaction.Message.Value, reaction.Emote, false);
            }

            return Task.CompletedTask;
        }

        private async Task HandleEffortSelection(SocketUserMessage message, IEmote emote, bool wasAdded)
        {
            if (wasAdded == false && EffortMessage.Emote?.Name == emote.Name)
            {
                EffortMessage.Emote = null;

                await RemoveReactions(FooterMessage, Self, ConfirmEmote);
            }
            else if (wasAdded == true)
            {
                var prevEmote = EffortMessage.Emote;

                EffortMessage.Emote = emote;

                if (prevEmote != null)
                {
                    await RemoveReactions(message, Initiator, prevEmote);
                }

                await AddReactions(FooterMessage, ConfirmEmote);
            }
        }
    }
}