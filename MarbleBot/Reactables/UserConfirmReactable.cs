﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using MarbleBot.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class UserConfirmReactable : ReactableBase
    {
        public readonly IEmote ConfirmEmote = new Emoji("✅");
        public readonly IEmote DenyEmote = new Emoji("❌");
        private ulong displaymessageid;

        public UserConfirmReactable(SocketUser user, UserTransaction transaction, CommandHandler handler) : base(user, transaction, handler)
        {
        }

        public IUserMessage DisplayMessage => GetMessageById(displaymessageid);
        protected override bool AdditionalCompletion => false;
        protected override bool ConfirmResult { get; set; }

        public async Task PostConfirmationMessage(string message, Embed embed = null)
        {
            MessageReacted = false;

            await Execute(async (msgs) =>
            {
                var msg = await Channel.SendMessageAsync(message, embed: embed) as RestUserMessage;
                displaymessageid = msg.Id;
                msgs.Add(msg);

                List<IEmote> emotes = new List<IEmote>
                    {
                        ConfirmEmote,
                        DenyEmote
                    };

                await AddReactions(msg, emotes.ToArray());
            });
        }

        protected override Task ReactAddImplementation(SocketReaction reaction)
        {
            if ((reaction.Emote.Name == ConfirmEmote.Name || reaction.Emote.Name == DenyEmote.Name)
                && reaction.Message.Value.Reactions.Any(e => e.Key.Name == reaction.Emote.Name && e.Value.IsMe))
            {
                ConfirmResult = (reaction.Emote.Name == ConfirmEmote.Name);
                MessageReacted = true;
            }

            return Task.CompletedTask;
        }

        protected override Task ReactRemoveImplementation(SocketReaction reaction)
        {
            if (reaction.Emote.Name == ConfirmEmote.Name || reaction.Emote.Name == DenyEmote.Name)
            {
                MessageReacted = false;
            }

            return Task.CompletedTask;
        }
    }
}