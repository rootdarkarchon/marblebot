﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using MarbleBot.Interfaces;
using MarbleBot.Models;
using MarbleBot.Utilities;
using MarbleBot.Utilities.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarbleBot.Reactables
{
    public class DeckReactable : ReactableBase
    {
        public AsyncEvent<AddToDeckEventArgs> AddedToDeck;
        public AsyncEvent<ReactionAddedEventArgs> AddedToFooter;
        public AsyncEvent<ReactionEventArgs> AddReactedToDeck;
        public AsyncEvent<ReactionEventArgs> AddReactedToFooter;
        public IEmote BackEmote = new Emoji("◀️");
        public int CurrentPage;
        public List<MessageWithCard> DeckMessages;
        public IUser DeckToDisplay;

        public IEmote FirstPageEmote = new Emoji("⏮️");
        public IEmote ForwardEmote = new Emoji("▶️");
        public IEmote LastPageEmote = new Emoji("⏭️");
        public int PaginationLimit;
        public ulong PreviousDisplayedCard;
        public AsyncEvent<ReactionEventArgs> RemoveReactedToFooter;
        private readonly List<IFooterMessageDeckAddon> FooterMessageExtensions;
        private readonly List<IHeaderMessageExtension> HeaderMessageExtensions;
        private readonly int initialPaginationLimit;
        private readonly List<IWaitableExtension> WaitableExtensions;
        private List<UserCard> CardList;
        private ulong footerid;
        private ulong headerid;

        public DeckReactable(SocketUser initiator, int pageLimit, UserTransaction transaction, CommandHandler handler) : base(initiator, transaction, handler)
        {
            PaginationLimit = pageLimit;
            initialPaginationLimit = PaginationLimit;
            DeckMessages = new List<MessageWithCard>();
            FooterMessageExtensions = new List<IFooterMessageDeckAddon>();
            WaitableExtensions = new List<IWaitableExtension>();
            HeaderMessageExtensions = new List<IHeaderMessageExtension>();
        }

        public IUserMessage DeckFooter => GetMessageById(footerid);
        public IUserMessage DeckHeader => GetMessageById(headerid);

        protected override bool AdditionalCompletion
        {
            get
            {
                return WaitableExtensions.All(e => !e.WaitForExtension);
            }
        }

        protected override bool ConfirmResult
        {
            get
            {
                return !WaitableExtensions.Any(c => c.ConfirmResult == false);
            }
            set
            {
                // do nothing
            }
        }

        public void AddCardDeckReactionExtension(ICardsReactionDeckAddon extension)
        {
            AddReactedToDeck += async (s, e) =>
            {
                try
                {
                    await extension.HandleDeckReactionAdd(s, e);
                }
                catch (Exception ex)
                {
                    _ = Task.Run(() => CommandHandler.Log(new LogMessage(LogSeverity.Error, "ReactiveDeck", "Failed to add handle base deck reaction add of " + extension.GetType(), ex)));
                }
            };
            AddedToDeck += async (s, e) => await extension.ManageReactionsToDeck(s, e);
        }

        public void AddFooterMessageExtension(IFooterMessageDeckAddon extension)
        {
            FooterMessageExtensions.Add(extension);
            extension.Deck = this;
        }

        public void AddFooterReactionAddon<T>(IFooterReactionAddon<T> extension)
        {
            extension.Deck = this;
            WaitableExtensions.Add(extension);
            AddReactedToFooter += async (s, e) =>
            {
                try
                {
                    await extension.HandleFooterReactionAdd(s, e);
                }
                catch (Exception ex)
                {
                    await CommandHandler.Log(new LogMessage(LogSeverity.Error, "ReactiveDeck", "Failed to add handle base footer reaction add of " + extension.GetType(), ex));
                }
            };
            RemoveReactedToFooter += async (s, e) =>
            {
                try
                {
                    await extension.HandleFooterReactionRemove(s, e);
                }
                catch (Exception ex)
                {
                    await CommandHandler.Log(new LogMessage(LogSeverity.Error, "ReactiveDeck", "Failed to add footer reaction remove of " + extension.GetType(), ex));
                }
            };
            AddedToFooter += async (s, e) =>
            {
                try
                {
                    await extension.AddReactionToFooter(s, e);
                }
                catch (Exception ex)
                {
                    await CommandHandler.Log(new LogMessage(LogSeverity.Error, "ReactiveDeck", "Failed to add footer reaction remove of " + extension.GetType(), ex));
                }
            };
            AddFooterMessageExtension(extension);
        }

        public void AddHeaderMessageExtension(IHeaderMessageExtension extension)
        {
            HeaderMessageExtensions.Add(extension);
            extension.Deck = this;
        }

        public async Task PostReactiveDeck(SocketUser deckToDisplay, List<UserCard> cardList, bool ascendingP = false)
        {
            DeckToDisplay = deckToDisplay ?? Initiator;
            CurrentPage = 0;
            DeckMessages = new List<MessageWithCard>();
            headerid = 0;
            footerid = 0;
            PaginationLimit = initialPaginationLimit;

            if (ascendingP)
            {
                CardList = new List<UserCard>(cardList.OrderBy(c => c.BaseCard.Type).ThenBy(c => c.Value).ThenBy(c => c.BaseCard.Rarity).ThenBy(c => c.Team.Name).ThenBy(c => c.Name));
            }
            else
            {
                CardList = new List<UserCard>(cardList.OrderByDescending(c => c.BaseCard.Type).ThenByDescending(c => c.Value).ThenByDescending(c => c.BaseCard.Rarity).ThenBy(c => c.Team.Name).ThenBy(c => c.Name));
            }

            await Execute(async (msgs) =>
            {
                await PostHeaderMessage(msgs);

                if (PaginationLimit > CardList.Count()) PaginationLimit = CardList.Count();

                await GeneratePage(msgs);

                string postMsg = "";

                foreach (var footerHandler in FooterMessageExtensions)
                {
                    postMsg += Environment.NewLine + footerHandler.FooterMessage;
                }

                var footer = (await Channel.SendMessageAsync(postMsg) as RestUserMessage);
                footerid = footer.Id;
                msgs.Add(footer);

                if (cardList.Count > PaginationLimit)
                {
                    if (Math.Ceiling(CardList.Count() / (double)PaginationLimit) > 2)
                    {
                        await AddReactions(DeckHeader, FirstPageEmote);
                    }
                    await AddReactions(DeckHeader, BackEmote, ForwardEmote);
                    if (Math.Ceiling(CardList.Count() / (double)PaginationLimit) > 2)
                    {
                        await AddReactions(DeckHeader, LastPageEmote);
                    }
                }

                await (AddedToFooter?.InvokeAsync(this, new ReactionAddedEventArgs { Message = DeckFooter }) ?? Task.CompletedTask);
            });
        }

        protected override async Task ReactAddImplementation(SocketReaction reaction)
        {
            if (DeckMessages.Any(d => d.Message.Id == reaction.MessageId))
            {
                await (AddReactedToDeck?.InvokeAsync(this, new ReactionEventArgs
                {
                    Reaction = reaction,
                    MessageWithCard = DeckMessages.First(d => d.Message.Id == reaction.MessageId)
                }) ?? Task.CompletedTask);
            }

            if (DeckFooter.Id == reaction.MessageId)
            {
                await (AddReactedToFooter?.InvokeAsync(this, new ReactionEventArgs
                {
                    Reaction = reaction,
                    MessageWithCard = new MessageWithCard(() => GetMessageById(footerid), null)
                }) ?? Task.CompletedTask);
            }

            await ChangePage(reaction);
        }

        protected override async Task ReactRemoveImplementation(SocketReaction reaction)
        {
            if (DeckFooter.Id == reaction.MessageId)
            {
                await (RemoveReactedToFooter?.InvokeAsync(this, new ReactionEventArgs
                {
                    Reaction = reaction,
                    MessageWithCard = new MessageWithCard(() => GetMessageById(footerid), null)
                }) ?? Task.CompletedTask);
            }
        }

        private async Task ChangePage(SocketReaction reaction)
        {
            int newPage = CurrentPage;

            if (reaction.Emote.Name == ForwardEmote.Name)
            {
                newPage = CurrentPage + 1;
                if (newPage >= Math.Ceiling(CardList.Count() / (double)PaginationLimit)) newPage = CurrentPage;
            }
            else if (reaction.Emote.Name == LastPageEmote.Name)
            {
                newPage = (int)Math.Ceiling(CardList.Count() / (double)PaginationLimit) - 1;
            }
            else if (reaction.Emote.Name == FirstPageEmote.Name)
            {
                newPage = 0;
            }
            else if (reaction.Emote.Name == BackEmote.Name)
            {
                newPage = CurrentPage - 1;
                if (newPage < 0) newPage = 0;
            }

            if (newPage != CurrentPage)
            {
                CurrentPage = newPage;
                await GeneratePage();
                await PostHeaderMessage();
            }

            await DeckHeader.RemoveReactionAsync(reaction.Emote, (SocketUser)reaction.User);
        }

        private async Task GeneratePage(List<IUserMessage> messages = null)
        {
            for (int i = 0; i < PaginationLimit; i++)
            {
                MessageWithCard msg = DeckMessages.Skip(i).FirstOrDefault();
                var card = CardList.Skip(i + CurrentPage * PaginationLimit).Take(PaginationLimit).FirstOrDefault();
                var content = card == null ? "-" : $"**{card}**";
                if (msg?.Message == null)
                {
                    var cardmsg = await Channel.SendMessageAsync(content) as RestUserMessage;
                    DeckMessages.Add(new MessageWithCard(() => GetMessageById(cardmsg.Id), card));
                    if (messages != null)
                    {
                        messages.Add(cardmsg);
                    }
                    msg = DeckMessages.Last();
                }
                else
                {
                    await msg.Message.ModifyAsync(e => { e.Content = content; e.Embed = null; });
                }

                msg.Card = card;
            }

            await (AddedToDeck?.InvokeAsync(this, new AddToDeckEventArgs { Messages = DeckMessages }) ?? Task.CompletedTask);
        }

        private async Task PostHeaderMessage(List<IUserMessage> messages = null)
        {
            string deckMsg = "";
            foreach (var headerExt in HeaderMessageExtensions)
            {
                deckMsg += headerExt.HeaderMessage + Environment.NewLine;
            }
            deckMsg += $"Deck of **{DeckToDisplay.Username}** ({CardList.Count()} cards) Page [{CurrentPage + 1}/{Math.Ceiling(CardList.Count() / (double)PaginationLimit)}]";
            if (DeckHeader == null)
            {
                var header = await Channel.SendMessageAsync($"{deckMsg}") as RestUserMessage;
                headerid = header.Id;
                if (messages != null)
                {
                    messages.Add(header);
                }
            }
            else
            {
                await DeckHeader.ModifyAsync(e => e.Content = deckMsg);
            }
        }
    }
}