﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using MarbleBot.Interfaces;
using MarbleBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MarbleBot.Utilities
{
    public class UserUpgradeReactable : ReactableBase
    {
        public readonly IEmote BalanceUpgrade = new Emoji("⚖️");
        public readonly IEmote ConfirmEmote = new Emoji("✅");
        public readonly IEmote DenyEmote = new Emoji("❌");
        public readonly IEmote EnduranceUpgrade = new Emoji("🏃");
        public readonly IEmote PowerUpgrade = new Emoji("⚡");
        public readonly IEmote StrengthUpgrade = new Emoji("💪");
        public readonly IEmote TeamworkUpgrade = new Emoji("🤼");
        public List<IEmote> SelectedUpgrade;
        public List<IEmote> UpgradeEmotes;
        private UserCard CardToUpgrade;
        private ulong displaymsgid;

        public UserUpgradeReactable(SocketUser user, UserTransaction transaction, CommandHandler handler) : base(user, transaction, handler)
        {
            UpgradeEmotes = new List<IEmote>();
        }

        public IUserMessage DisplayMessage => GetMessageById(displaymsgid);
        protected override bool AdditionalCompletion => false;
        protected override bool ConfirmResult { get; set; }

        public async Task PostConfirmationMessage(UserCard cardToUpgrade)
        {
            CardToUpgrade = cardToUpgrade;
            MessageReacted = false;
            SelectedUpgrade = new List<IEmote>();
            UpgradeEmotes = new List<IEmote>();

            string messageToSend = $"Select **{AvailableUpgrades()}** stat(s) you want to upgrade" + Environment.NewLine;

            var emotes = new List<IEmote>();
            if (cardToUpgrade.TotalStrength < UserCard.MaxCardStat)
            {
                emotes.Add(StrengthUpgrade);
                UpgradeEmotes.Add(StrengthUpgrade);
                messageToSend += $"> {StrengthUpgrade} = Strength ({cardToUpgrade.TotalStrength} ➡️ {cardToUpgrade.TotalStrength + 1})" + Environment.NewLine;
            }
            if (cardToUpgrade.TotalEndurance < UserCard.MaxCardStat)
            {
                emotes.Add(EnduranceUpgrade);
                UpgradeEmotes.Add(EnduranceUpgrade);
                messageToSend += $"> {EnduranceUpgrade} = Endurance ({cardToUpgrade.TotalEndurance} ➡️ {cardToUpgrade.TotalEndurance + 1})" + Environment.NewLine;
            }
            if (cardToUpgrade.TotalPower < UserCard.MaxCardStat)
            {
                emotes.Add(PowerUpgrade);
                UpgradeEmotes.Add(PowerUpgrade);
                messageToSend += $"> {PowerUpgrade} = Power ({cardToUpgrade.TotalPower} ➡️ {cardToUpgrade.TotalPower + 1})" + Environment.NewLine;
            }
            if (cardToUpgrade.TotalBalance < UserCard.MaxCardStat)
            {
                emotes.Add(BalanceUpgrade);
                UpgradeEmotes.Add(BalanceUpgrade);
                messageToSend += $"> {BalanceUpgrade} = Balance ({cardToUpgrade.TotalBalance} ➡️ {cardToUpgrade.TotalBalance + 1})" + Environment.NewLine;
            }
            if (cardToUpgrade.TotalTeamwork < UserCard.MaxCardStat)
            {
                emotes.Add(TeamworkUpgrade);
                UpgradeEmotes.Add(TeamworkUpgrade);
                messageToSend += $"> {TeamworkUpgrade} = Teamwork ({cardToUpgrade.TotalTeamwork} ➡️ {cardToUpgrade.TotalTeamwork + 1})" + Environment.NewLine;
            }

            messageToSend += $"Select {ConfirmEmote} to confirm, {DenyEmote} to cancel.";

            await Execute(async (msgs) =>
            {
                var msg = await Channel.SendMessageAsync(messageToSend) as RestUserMessage;
                displaymsgid = msg.Id;

                msgs.Add(msg);

                emotes.Add(DenyEmote);

                await AddReactions(DisplayMessage, emotes.ToArray());
            });
        }

        public Dictionary<PropertyInfo, PropertyInfo> UpgradeToCardProperty()
        {
            Dictionary<PropertyInfo, PropertyInfo> propertyInfo = new Dictionary<PropertyInfo, PropertyInfo>();
            foreach (var upgrade in SelectedUpgrade)
            {
                if (upgrade.Name == StrengthUpgrade.Name)
                {
                    propertyInfo.Add(typeof(UserCard).GetProperty(nameof(UserCard.TotalStrength)),
                        typeof(UserCard).GetProperty(nameof(UserCard.UpgradedStrength)));
                }
                else if (upgrade.Name == PowerUpgrade.Name)
                {
                    propertyInfo.Add(typeof(UserCard).GetProperty(nameof(UserCard.TotalPower)),
                        typeof(UserCard).GetProperty(nameof(UserCard.UpgradedPower)));
                }
                else if (upgrade.Name == EnduranceUpgrade.Name)
                {
                    propertyInfo.Add(typeof(UserCard).GetProperty(nameof(UserCard.TotalEndurance)),
                        typeof(UserCard).GetProperty(nameof(UserCard.UpgradedEndurance)));
                }
                else if (upgrade.Name == BalanceUpgrade.Name)
                {
                    propertyInfo.Add(typeof(UserCard).GetProperty(nameof(UserCard.TotalBalance)),
                        typeof(UserCard).GetProperty(nameof(UserCard.UpgradedBalance)));
                }
                else if (upgrade.Name == TeamworkUpgrade.Name)
                {
                    propertyInfo.Add(typeof(UserCard).GetProperty(nameof(UserCard.TotalTeamwork)),
                        typeof(UserCard).GetProperty(nameof(UserCard.UpgradedTeamwork)));
                }
            }

            return propertyInfo;
        }

        protected override Task ReactAddImplementation(SocketReaction reaction)
        {
            if ((reaction.Emote.Name == ConfirmEmote.Name || reaction.Emote.Name == DenyEmote.Name)
                && reaction.Message.Value.Reactions.Any(e => e.Key.Name == reaction.Emote.Name && e.Value.IsMe))
            {
                MessageReacted = true;
                ConfirmResult = (reaction.Emote.Name == ConfirmEmote.Name);
            }
            else if (UpgradeEmotes.Select(e => e.Name).Contains(reaction.Emote.Name))
            {
                _ = HandleUpgradeEmote(reaction.Emote, true);
            }

            return Task.CompletedTask;
        }

        protected override Task ReactRemoveImplementation(SocketReaction reaction)
        {
            if (reaction.Emote.Name == ConfirmEmote.Name || reaction.Emote.Name == DenyEmote.Name)
            {
                MessageReacted = false;
            }
            else if (UpgradeEmotes.Select(e => e.Name).Contains(reaction.Emote.Name))
            {
                _ = HandleUpgradeEmote(reaction.Emote, false);
            }

            return Task.CompletedTask;
        }

        private int AvailableUpgrades()
        {
            return (int)Math.Ceiling((CardToUpgrade.UpgradeCount + 1.0) / 2);
        }

        private async Task HandleUpgradeEmote(IEmote emote, bool wasAdded)
        {
            if (wasAdded == false)
            {
                if (SelectedUpgrade.Any(e => e.Name == emote.Name))
                {
                    SelectedUpgrade.RemoveAll(e => e.Name == emote.Name);
                }
            }
            else
            {
                SelectedUpgrade.Add(emote);

                if (SelectedUpgrade.Count > AvailableUpgrades())
                {
                    var upgrade = SelectedUpgrade.First();
                    SelectedUpgrade.Remove(SelectedUpgrade.First());
                    await RemoveReactions(DisplayMessage, Initiator, upgrade);
                }
            }

            if ((SelectedUpgrade.Count == AvailableUpgrades() || SelectedUpgrade.Count == UpgradeEmotes.Count) && !DisplayMessage.Reactions.Any(e => e.Key.Name == ConfirmEmote.Name))
            {
                await AddReactions(DisplayMessage, ConfirmEmote);
            }
            else if (DisplayMessage.Reactions.Any(e => e.Key.Name == ConfirmEmote.Name) && (SelectedUpgrade.Count < AvailableUpgrades() && SelectedUpgrade.Count < UpgradeEmotes.Count))
            {
                await RemoveReactions(DisplayMessage, Self, ConfirmEmote);
            }
        }
    }
}